package com.comeonnow.provider.app.ui.activity

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.interfaces.CancelAppointmentInterface
import com.comeonnow.provider.app.adapter.ScheduledAppointmentAdapter
import com.comeonnow.provider.app.model.CancelAppointmentModel
import com.comeonnow.provider.app.model.ScheduleAppointmentDataItem
import com.comeonnow.provider.app.model.ScheduleAppointmentModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_scheduled_appointment.*
import kotlinx.android.synthetic.main.activity_scheduled_appointment.txtNoDataFoundTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ScheduledAppointmentActivity : BaseActivity(){

    // - - Initialize Objects
    var mScheduledAppointmentAdapter: ScheduledAppointmentAdapter? = null
    var mScheduledAppointmentList: ArrayList<ScheduleAppointmentDataItem> = ArrayList<ScheduleAppointmentDataItem>()
    var clickPos:Int?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scheduled_appointment)
        ButterKnife.bind(this)
        executeScheduledAppointmentApi()
    }

    @OnClick(
        R.id.backRL)

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Execute Scheduled Appointment Listing Api
    private fun executeScheduledAppointmentApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.scheduledListingRequest(getAccessToken(), "1000", "1")
        call.enqueue(object : Callback<ScheduleAppointmentModel> {
            override fun onResponse(
                call: Call<ScheduleAppointmentModel>,
                response: Response<ScheduleAppointmentModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        if (response.body() != null && response.body()!!.data.size!! > 0) {
                            txtNoDataFoundTV.visibility = View.GONE
                            mScheduledAppointmentList = response.body()?.data as ArrayList<ScheduleAppointmentDataItem>
                            setAdapter(mActivity, mScheduledAppointmentList)
                            Log.i(TAG, "onResponse: $mScheduledAppointmentList")
                        }else{
                            txtNoDataFoundTV.visibility = View.VISIBLE
                            txtNoDataFoundTV.text = getString(R.string.no_scheduled_appointment)
                        }
                    }
                    404 -> {
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        txtNoDataFoundTV.text = getString(R.string.no_scheduled_appointment)
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        txtNoDataFoundTV.text = getString(R.string.no_scheduled_appointment)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        txtNoDataFoundTV.text = getString(R.string.no_scheduled_appointment)
                    }
                }
            }

            override fun onFailure(call: Call<ScheduleAppointmentModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    // - - Sets Adapter
    private fun setAdapter(activity: Activity?, mScheduledAppointmentList: ArrayList<ScheduleAppointmentDataItem>) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        scheduledAppointmentRV.layoutManager = layoutManager
        mScheduledAppointmentAdapter = ScheduledAppointmentAdapter(activity, mScheduledAppointmentList, mCancelAppointmentInterface)
        scheduledAppointmentRV.adapter = mScheduledAppointmentAdapter
    }


    // - - Implements Interface on cancel Appointment Click
    var mCancelAppointmentInterface: CancelAppointmentInterface = object : CancelAppointmentInterface {
        override fun cancelAppointment(position: Int, id: Int?) {
            super.cancelAppointment(position, id)
             clickPos = position
           showCancelAlertDialog(mActivity,getString(R.string.are_you_sure_to_cancel),id)
        }
    }

    // - - To Show Cancel Alert Dialog
    fun showCancelAlertDialog(mActivity: Activity?, strMessage: String?, id: Int?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_cancel_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnCancel = alertDialog.findViewById<TextView>(R.id.btnCancel)
        val btnOk = alertDialog.findViewById<TextView>(R.id.btnOk)
        txtMessageTV.text = strMessage
        btnCancel.setOnClickListener { alertDialog.dismiss() }
        btnOk.setOnClickListener { executeCancelAppointmentApi(id)
        alertDialog.dismiss()}
        alertDialog.show()
    }

    private fun executeCancelAppointmentApi(id: Int?) {
        Log.e(TAG, "id::$id")
        showProgressDialog(mActivity)
        val mGetProfileData = id?.let { ApiClient.apiInterface.cancelAppointment(getAccessToken(), it) }
        mGetProfileData?.enqueue(object : Callback<CancelAppointmentModel> {
            override fun onResponse(
                call: Call<CancelAppointmentModel>,
                response: Response<CancelAppointmentModel>
            ) {
                dismissProgressDialog()
                if (response.code() == 200) {
//                    mScheduledAppointmentList.removeAt(clickPos!!)
//                    mScheduledAppointmentAdapter?.notifyItemRemoved(clickPos!!)
                    executeScheduledAppointmentApi()
                 } else if (response.code() == 400) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                } else if (response.code() == 401) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showAuthFailedDialog(mActivity)
                } else if (response.code() == 500) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showToast(mActivity, getString(R.string.internal_server_error))
                }
            }
            override fun onFailure(call: Call<CancelAppointmentModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
}