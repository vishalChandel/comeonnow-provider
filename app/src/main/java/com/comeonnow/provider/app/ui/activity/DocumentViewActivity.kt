package com.comeonnow.provider.app.ui.activity
import android.annotation.SuppressLint
import android.content.ContentValues
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.os.StrictMode.ThreadPolicy
import android.util.DisplayMetrics
import android.util.Log
import android.view.MotionEvent
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonow.provider.app.R
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.activity_document_view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL



class DocumentViewActivity : BaseActivity() {
    var path:Uri?=null
    var docTitle:String?=null
    var documentUrl:String?=null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_view)
        ButterKnife.bind(this)
        val policy = ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
            documentUrl = intent.getStringExtra("docData")
            docTitle = intent.getStringExtra("docTitle")
            docTitleTV.text=docTitle
            showProgressDialog(mActivity)
            // launching a new coroutine
            GlobalScope.launch {
                initPdfView(documentUrl, docTitle.toString())
            }
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.signRL,

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()

        }
    }
    private fun initPdfView(docUrl: String?, docTitle: String) {
         path = Uri.fromFile(downloadFile(docUrl))
        pdfView.fromUri(path)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .pageFitPolicy(FitPolicy.BOTH)
            .fitEachPage(true)
            .autoSpacing(true)
            .pageSnap(true)
            .pageFling(true)
            .load()

        dismissProgressDialog()
        Log.i(TAG, "initPdfView: "+pdfView.width+":::"+ pdfView.height)
    }

    private fun downloadFile(docUrl: String?): File? {
        var file: File? = null
        try {
            val url = URL(docUrl)
            val urlConnection: HttpURLConnection = url
                .openConnection() as HttpURLConnection
            urlConnection.requestMethod = "GET"
            urlConnection.doOutput = true
            // connect
            urlConnection.connect()
            // create a new file, to save the downloaded file
            file = File("$cacheDir/sample.pdf")
            val fileOutput = FileOutputStream(file)

            // Stream used for reading the data from the internet
            val inputStream: InputStream = urlConnection.inputStream

            // create a buffer...
            val buffer = ByteArray(1024 * 1024)
            var bufferLength = 0
            while (inputStream.read(buffer).also { bufferLength = it } > 0) {
                fileOutput.write(buffer, 0, bufferLength)
                var downloadedSize: String? = null
                downloadedSize += bufferLength
            }
            fileOutput.close()
        } catch (e: MalformedURLException) {
            Log.e(TAG, "downloadFile: $e")
        } catch (e: IOException) {
            Log.e(TAG, "downloadFile: $e")
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "downloadFile: $e")
        }
        return file
    }
}