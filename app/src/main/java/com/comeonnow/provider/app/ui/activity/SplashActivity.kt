package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import com.comeonnow.provider.app.fcm.CustomNotificationModel
import com.comeonnow.provider.app.model.DefaultLangModel
import com.comeonnow.provider.app.model.LoginModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class SplashActivity : BaseActivity() {

    var  mNotificationModel : CustomNotificationModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        performChangeLanguageClick()
        setUpSplash()
    }



    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                if (isLogin()) {
                    setUpNotification()
                } else {
                    val i = Intent(mActivity, SignInActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }

    private fun setUpNotification() {
        if (intent.extras != null) {
            for (key in intent.extras!!.keySet()) {
                val value = intent.extras!![key]
                Log.d(TAG, "Key: $key Value: $value")
                if (key.equals("data", ignoreCase = true)) {
                    try {
                        Log.e(TAG,"Notifications:::::$key")
                        mNotificationModel = Gson().fromJson(value.toString(), CustomNotificationModel::class.java)
                    } catch (e: Exception) {
                        Log.e("FCM", "onMessageReceived: $e")
                    }
                }
            }
            if (!mNotificationModel?.type.equals("")){
                val intent = Intent(this, HomeActivity::class.java)
                intent.putExtra(NOTI_TYPE, mNotificationModel?.type)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                startActivity(intent)
                finish()
            }
        } else {
            val i = Intent(mActivity, HomeActivity::class.java)
            startActivity(i)
            finish()
        }
    }

    private fun performChangeLanguageClick() {
        if (Locale.getDefault().displayLanguage.subSequence(0, 2).toString() == "es") {
            executeDefaultLangRequest("es")
        }
        when {
            isEnglish() -> {
                setLocale("en")
            }

            !isEnglish() -> {
                setLocale("es")
            }
        }
    }

    private fun executeDefaultLangRequest(lang: String) {
        val mDefaultLang = ApiClient.apiInterface.defaultLangRequest(getAccessToken(), lang)
        mDefaultLang.enqueue(object : Callback<DefaultLangModel> {
            override fun onResponse(
                call: Call<DefaultLangModel>,
                response: Response<DefaultLangModel>
            ) {
                if (response.code() == 200) {
                    AppPreference().writeBoolean(mActivity, IS_ENGLISH, false)
                } else if (response.code() == 401) {
                    showAuthFailedDialog(mActivity)
                } else if (response.code() == 500) {
                    showToast(mActivity, getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<DefaultLangModel>, t: Throwable) {
            }
        })
    }
}