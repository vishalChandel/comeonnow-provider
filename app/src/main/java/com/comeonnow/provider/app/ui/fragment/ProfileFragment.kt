package com.comeonnow.provider.app.ui.fragment

import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.provider.app.model.DefaultLangModel
import com.comeonnow.provider.app.model.LogoutModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.ui.activity.*
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.fragment_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProfileFragment : BaseFragment() {

    // - - Initialize Widgets
    private lateinit var mUnbinder: Unbinder

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        return view
    }

    @OnClick(
        R.id.editRL,
        R.id.changePwdRL,
        R.id.historyRL,
        R.id.changeLanguageRL,
        R.id.aboutUsRL,
        R.id.privacyPolicyRL,
        R.id.termsRL,
        R.id.myClinicsRL,
        R.id.scheduledAppointmentRL,
        R.id.logoutRL,
        R.id.documentsRL,
        R.id.supportBotRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.editRL -> performEditProfileClick()
            R.id.changePwdRL -> performChangePwdClick()
            R.id.myClinicsRL -> performMyClinicsClick()
            R.id.historyRL -> performHistoryClick()
            R.id.scheduledAppointmentRL -> performScheduledAppointmentClick()
            R.id.changeLanguageRL -> performChangeLanguageClick()
            R.id.aboutUsRL -> performAboutUsClick()
            R.id.termsRL -> performTermsClick()
            R.id.privacyPolicyRL -> performPrivacyClick()
            R.id.logoutRL -> performLogoutClick()
            R.id.documentsRL -> performDocumentClick()
            R.id.supportBotRL -> performSupportBotClick()
        }
    }

    private fun performSupportBotClick() {
        val intent = Intent(activity, SupportBotActivity::class.java)
        startActivity(intent)
    }

    private fun performDocumentClick() {
        val intent = Intent(activity, DocumentActivity::class.java)
        startActivity(intent)
    }

    private fun performMyClinicsClick() {
        val intent = Intent(activity, MyClinicsActivity::class.java)
        startActivity(intent)
    }

    private fun performChangeLanguageClick() {
        when {
            isEnglish() -> {
                executeDefaultLangRequest("es")
            }
            !isEnglish() -> {
                executeDefaultLangRequest("en")
            }
        }
    }

    private fun performAboutUsClick() {
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_ABOUT)
        startActivity(i)
    }

    private fun performTermsClick() {
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_TERMS)
        startActivity(i)
    }

    private fun performPrivacyClick() {
        val i = Intent(activity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performScheduledAppointmentClick() {
        val intent = Intent(activity, ScheduledAppointmentActivity::class.java)
        startActivity(intent)
    }

    private fun performEditProfileClick() {
        val intent = Intent(activity, EditProfileActivity::class.java)
        startActivity(intent)
    }

    private fun performChangePwdClick() {
        val intent = Intent(activity, ChangePasswordActivity::class.java)
        startActivity(intent)
    }

    private fun performHistoryClick() {
        val intent = Intent(activity, HistoryActivity::class.java)
        startActivity(intent)
    }

    private fun performLogoutClick() {
        showLogoutConfirmAlertDialog()
    }

    // - - Logout Alert Dialog
    private fun showLogoutConfirmAlertDialog() {
        val alertDialog = activity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
            executeLogoutRequest()
        }
        alertDialog.show()
    }

    // - - To Clear Shared Preference Data
    private fun clearSharedPreferenceData() {
        val preferences: SharedPreferences = AppPreference().getPreferences(requireContext())
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
        editor.commit()
        val mIntent = Intent(activity, SignInActivity::class.java)
        // To clean up all activities
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        activity?.startActivity(mIntent)
        activity?.finish()
        activity?.finishAffinity()
    }

    // - - Execute Logout Api
    private fun executeLogoutRequest() {
        showProgressDialog(activity)
        val call = ApiClient.apiInterface.logoutRequest(
            getAccessToken(), "",
            USER_TYPE
        )
        call.enqueue(object : Callback<LogoutModel> {
            override fun onFailure(call: Call<LogoutModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<LogoutModel>, response: Response<LogoutModel>) {
                dismissProgressDialog()
                val mModel = response.body()
                when (response.code()) {
                    200 -> {
                        showToast(activity, mModel?.message)
                        clearSharedPreferenceData()
                    }
                    401 -> {
                        showAuthFailedDialog(activity)
                    }
                    500 -> {
                        showToast(activity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    private fun setDataOnWidgets() {
        profileNameTV.text = lastName() + "," + " " + firstName()
        profileEmailTV.text = userEmail()
        if (profilePicture() != null && !profilePicture().equals("")) {
            Picasso.get()
                .load(profilePicture())
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(profileIV)
        }

    }

    override fun onResume() {
        super.onResume()
        setDataOnWidgets()
        if (badgeCount() == 0) {
            badgeCountRL.visibility = View.GONE
        } else {
            badgeCountRL.visibility = View.VISIBLE
            unreadCountTV.text = "" + badgeCount()
        }
    }

    private fun executeDefaultLangRequest(lang: String) {
        showProgressDialog(activity)
        val mDefaultLang = ApiClient.apiInterface.defaultLangRequest(getAccessToken(), lang)
        mDefaultLang.enqueue(object : Callback<DefaultLangModel> {
            override fun onResponse(
                call: Call<DefaultLangModel>,
                response: Response<DefaultLangModel>
            ) {
                if (response.code() == 200) {
                    dismissProgressDialog()
                    when {
                        isEnglish() -> {
                            setLocale("es")
                            AppPreference().writeBoolean(requireActivity(), IS_ENGLISH, false)
                            val i = Intent(activity, HomeActivity::class.java)
                            startActivity(i)
                            requireActivity().finish()
                        }
                        !isEnglish() -> {
                            setLocale("en")
                            AppPreference().writeBoolean(requireActivity(), IS_ENGLISH, true)
                            val i = Intent(activity, HomeActivity::class.java)
                            startActivity(i)
                            requireActivity().finish()
                        }
                    }
                } else if (response.code() == 401) {
                    dismissProgressDialog()
                    showAuthFailedDialog(activity)
                } else if (response.code() == 500) {
                    dismissProgressDialog()
                    showToast(activity, getString(R.string.internal_server_error))

                }
            }

            override fun onFailure(call: Call<DefaultLangModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
}