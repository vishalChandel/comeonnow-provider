package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProfileErrorModel(

	@field:SerializedName("hasErrors")
	val hasErrors: Boolean? = null,

	@field:SerializedName("errors")
	val errors: ProfileErrors? = null
) : Parcelable

@Parcelize
data class ProfileErrors(

	@field:SerializedName("cellno")
	val cellno: List<String?>? = null
) : Parcelable
