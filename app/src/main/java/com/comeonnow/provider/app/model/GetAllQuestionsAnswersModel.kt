package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllQuestionsAnswersModel(

	@field:SerializedName("data")
	val data: ArrayList<GetAllQuestionsAnswersDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class GetAllQuestionsAnswersDataItem(

	@field:SerializedName("qlist")
	val qlist: ArrayList<QlistItem> = ArrayList(),

	@field:SerializedName("topic")
	val topic: String? = null,

	@field:SerializedName("topic_id")
	val topicId: Int? = null
) : Parcelable

@Parcelize
data class SubquestionItem(

	@field:SerializedName("questions")
	val questions: String? = null,

	@field:SerializedName("answers")
	val answers: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("subquestion")
	val subquestion: ArrayList<SubquestionItem> = ArrayList()
) : Parcelable

@Parcelize
data class QlistItem(

	@field:SerializedName("questions")
	val questions: String? = null,

	@field:SerializedName("answers")
	val answers: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("subquestion")
	val subquestion: ArrayList<SubquestionItem> = ArrayList()
) : Parcelable
