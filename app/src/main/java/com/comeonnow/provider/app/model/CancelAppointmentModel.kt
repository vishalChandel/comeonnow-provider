package com.comeonnow.provider.app.model

import com.google.gson.annotations.SerializedName

data class CancelAppointmentModel(

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
