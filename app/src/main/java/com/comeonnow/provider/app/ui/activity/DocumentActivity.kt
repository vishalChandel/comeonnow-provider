package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.adapter.DocumentsAdapter
import com.comeonnow.provider.app.model.DocDataItem
import com.comeonnow.provider.app.model.DocListModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.AppPreference
import com.comeonnow.provider.app.util.BADGE_COUNT
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_document.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DocumentActivity : BaseActivity() {

    private lateinit var mDocumentsAdapter: DocumentsAdapter
    var mArrayList : ArrayList<DocDataItem> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document)
        ButterKnife.bind(this)
    }

    private fun getDocData() {
        if (isNetworkAvailable(mActivity)){
            executeDocListRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeDocListRequest() {
        showProgressDialog(mActivity)
        val mGetProfileData = ApiClient.apiInterface.documentListRequest(getAccessToken(),"100","1")
        mGetProfileData.enqueue(object : Callback<DocListModel> {
            override fun onResponse(call: Call<DocListModel>, response: Response<DocListModel>) {
                dismissProgressDialog()
                mArrayList.clear()
                if (response.code() == 200) {
                    val mModel: DocListModel = response.body()!!
                    mModel.badgeCount?.let {
                        AppPreference().writeInteger(mActivity, BADGE_COUNT,
                            it
                        )
                    }
                    if (mModel.data?.size!! > 0) {
                        txtNoDataFoundTV.visibility = View.GONE
                        mArrayList.addAll(mModel.data)
                        setAdapter()
                    } else {
                        txtNoDataFoundTV.visibility = View.VISIBLE
                    }
                }else if (response.code() == 400){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                }else if (response.code() == 401){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<DocListModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
    

    private fun setAdapter() {
        mDocumentsAdapter = DocumentsAdapter(this,mArrayList)
        documentRV.adapter = mDocumentsAdapter
    }


    @OnClick(
        R.id.backRL,
        R.id.addDocumentLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()

            R.id.addDocumentLL -> performAddDocumentClick()
        }
    }

    private fun performAddDocumentClick() {
        val intent = Intent(mActivity, AddDocumentActivity::class.java)
        startActivity(intent)
    }

    override fun onResume() {
        super.onResume()
        getDocData()
    }
}