package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PatientModel(

	@field:SerializedName("data")
	val data: ArrayList<PatientDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = "",

	@field:SerializedName("status")
	val status: Int? = 0
) : Parcelable

@Parcelize
data class PatientDataItem(

	@field:SerializedName("last_name")
	val lastName: String? = "",

	@field:SerializedName("id")
	val id: Int? = 0,

	@field:SerializedName("first_name")
	val firstName: String? = ""
) : Parcelable
