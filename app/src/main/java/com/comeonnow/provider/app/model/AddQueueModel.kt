package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddQueueModel(

	@field:SerializedName("data")
	val data: ArrayList<QueueDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = "",

	@field:SerializedName("status")
	val status: Int? = 0
) : Parcelable

@Parcelize
data class QueueDataItem(

	@field:SerializedName("appoint_start_time")
	val appointStartTime: String? = "",

	@field:SerializedName("image")
	val image: String? = "",

	@field:SerializedName("gender")
	val gender: Int? = 0,

	@field:SerializedName("loginuser_name")
	val loginuserName: String? = "",

	@field:SerializedName("last_name")
	val lastName: String? = "",

	@field:SerializedName("type")
	val type: Int? = 0,

	@field:SerializedName("appoint_end_time")
	val appointEndTime: String? = "",

	@field:SerializedName("user_id")
	val userId: Int? = 0,

	@field:SerializedName("dob")
	val dob: String? = "",

	@field:SerializedName("id")
	val id: Int? = 0,

	@field:SerializedName("relationship")
	val relationship: Int? = null,

	@field:SerializedName("appoint_date")
	val appointDate: String? = "",

	@field:SerializedName("first_name")
	val firstName: String? = "",

	@field:SerializedName("age")
	val age: Int? = 0,

	@field:SerializedName("status")
	val status: Int? = 0
) : Parcelable
