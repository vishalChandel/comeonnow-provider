package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StateListModel(

	@field:SerializedName("data")
	val data: ArrayList<StateDataItem> = ArrayList<StateDataItem>(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class StateDataItem(

	@field:SerializedName("country_code")
	val countryCode: String? = null,

	@field:SerializedName("state_name")
	val stateName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("state_code")
	val stateCode: String? = null
) : Parcelable
