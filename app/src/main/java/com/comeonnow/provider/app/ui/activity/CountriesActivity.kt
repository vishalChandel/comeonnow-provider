package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.interfaces.ItemClickListner
import com.comeonnow.provider.app.adapter.CountriesAdapter
import com.comeonnow.provider.app.model.FlagModel
import com.comeonnow.provider.app.util.MODEL
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_countries.*

class CountriesActivity : BaseActivity() {
    // - - Initialize Objects
    lateinit var mAdapter: CountriesAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)
        ButterKnife.bind(this)
        setUpSearch()
        setCountriesAdapter()
    }

    @OnClick(
        R.id.backRL,
        R.id.imgCancelIV)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.imgCancelIV -> performCancelClick()
        }
    }

    private fun performCancelClick() {
        editSearchET.setText("")
        setCountriesAdapter()
    }

    // - - Search Functionality
    private fun setUpSearch() {
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty()) {
                    imgCancelIV.visibility = View.VISIBLE
                } else {
                    imgCancelIV.visibility = View.GONE
                }
                //after the change calling the method and passing the search input
                filter(s.toString());
            }
        })
    }

    // - - Filter Country list
    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdList = ArrayList<FlagModel>()
        //looping through existing elements
        for (s in getAllCountriesData()!!) {
            //if the existing elements contains the search input
            if (s!!.countryName.toLowerCase().contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdList.add(s)
            }
        }
        //calling a method of the adapter class and passing the filtered list
        mAdapter.filterList(filterdList)
    }

    // - - Sets Countries List Adapter
    private fun setCountriesAdapter() {
        mAdapter = CountriesAdapter(mActivity, getAllCountriesData(), mItemClickListner)
        countriesRV.layoutManager = LinearLayoutManager(this)
        countriesRV.setHasFixedSize(true)
        countriesRV.adapter = mAdapter
    }

    // - - Implements Interface on ItemClick
    var mItemClickListner: ItemClickListner = object : ItemClickListner {
        override fun onItemClickListner(mModel: FlagModel) {
            super.onItemClickListner(mModel)
            val intent = Intent()
            intent.putExtra(MODEL, mModel) //value should be your string from the edittext
            setResult(555, intent) //The data you want to send back
            finish()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}