package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.model.DocumentDetailModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import com.github.barteksc.pdfviewer.util.FitPolicy
import kotlinx.android.synthetic.main.activity_document.*
import kotlinx.android.synthetic.main.activity_document_detail.*
import kotlinx.android.synthetic.main.activity_document_view.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

class DocumentDetailActivity : BaseActivity() {
    var docId: String? = null
    var sendDoc: String? = null
    var receiveDoc: String? = null
    var path: Uri? = null
    var docTitle: String? = null
    private var mLastClickTab1: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_document_detail)
        ButterKnife.bind(this)
        getIntentdata()
    }

    private fun getIntentdata() {
        if (intent != null) {
            docId = intent.getStringExtra("DOC_ID")
            Log.e(TAG, "DOCID:$docId")
            executeDocDetailRequest()
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.docAttachedLL,
        R.id.docReceivedLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.docAttachedLL -> setSendDocClick()
            R.id.docReceivedLL -> setReceiveDocClick()
        }
    }

    private fun setReceiveDocClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        val i = Intent(mActivity, DocumentViewActivity::class.java)
        i.putExtra("docData", receiveDoc)
        i.putExtra("docTitle", docTitle)
        mActivity.startActivity(i)

    }

    private fun setSendDocClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTab1 < 2000) {
            return
        }
        mLastClickTab1 = SystemClock.elapsedRealtime()
        val i = Intent(mActivity, DocumentViewActivity::class.java)
        i.putExtra("docData", sendDoc)
        i.putExtra("docTitle", docTitle)
        mActivity.startActivity(i)

    }

    private fun executeDocDetailRequest() {
        showProgressDialog(mActivity)
        val mGetProfileData =
            docId?.let { ApiClient.apiInterface.documentDetailRequest(getAccessToken(), it) }
        mGetProfileData?.enqueue(object : Callback<DocumentDetailModel> {
            override fun onResponse(
                call: Call<DocumentDetailModel>,
                response: Response<DocumentDetailModel>
            ) {
                dismissProgressDialog()
                if (response.code() == 200) {
                    val mModel: DocumentDetailModel = response.body()!!
                    if (mModel.data?.size != null) {

                        for (i in 0 until mModel.data.size) {
                            headerNameTV.text = mModel.name
                            txtDocNameTV.text = mModel.docsName
                            txtDocDescpTV.text = mModel.docsDescription
                            docTitle = mModel.docsName
                            if (mModel.data[i]?.signStatus.equals("0")) //Send Doc
                            {
                                sendDateTV.text = mModel.data[i]?.createdAt
                                sendDoc = mModel.data[i]?.docs
                                attachedPdfTV.text = mModel.name
                            } else if (mModel.data[i]?.signStatus.equals("1")) //Received Doc
                            {
                                receiveDocLL.visibility = View.VISIBLE
                                receivedDateTV.text = mModel.data[i]?.createdAt
                                receiveDoc = mModel.data[i]?.docs
                                receivedPdfTV.text = mModel.name
                            }
                        }

                    }
                } else if (response.code() == 400) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                } else if (response.code() == 401) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showAuthFailedDialog(mActivity)
                } else if (response.code() == 500) {
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    showToast(mActivity, getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<DocumentDetailModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
}