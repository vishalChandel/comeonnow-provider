package com.comeonnow.provider.app

import android.app.Application
import com.comeonnow.provider.app.util.SOCKET_URL
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.engineio.client.transports.Polling
import okhttp3.OkHttpClient
import java.net.URISyntaxException
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

class ProviderApp : Application() {

    private var mInstance: ProviderApp? = null

    @Synchronized
    fun getInstance():ProviderApp?{
        return mInstance
    }

    var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()
        mInstance = this
        try {
            val myHostnameVerifier = HostnameVerifier { _, _ ->
                return@HostnameVerifier true
            }

            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager
            {
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}

                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            })

            val sslContext = SSLContext.getInstance("TLS")
            sslContext.init(null, trustAllCerts, null)

            val okHttpClient = OkHttpClient.Builder()
                .hostnameVerifier(myHostnameVerifier)
                .sslSocketFactory(sslContext.socketFactory, trustAllCerts[0] as X509TrustManager)
                .build()
            val options = IO.Options()
            options.transports = arrayOf(Polling.NAME)
            options.callFactory = okHttpClient
            options.webSocketFactory = okHttpClient
            mSocket = IO.socket(SOCKET_URL,options)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

    }

    fun getSocket(): Socket? {
        return mSocket
    }
}