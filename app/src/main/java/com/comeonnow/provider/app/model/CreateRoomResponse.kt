package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CreateRoomResponse(

    @field:SerializedName("status")
    val status: String? = "",

    @field:SerializedName("message")
    val message: String? = "",
    @field:SerializedName("room_id")
    val roomId: String? = ""
) : Parcelable