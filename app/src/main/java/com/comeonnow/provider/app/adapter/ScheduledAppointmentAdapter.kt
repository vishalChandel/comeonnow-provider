package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.interfaces.CancelAppointmentInterface
import com.comeonnow.provider.app.model.ScheduleAppointmentDataItem
import com.comeonow.provider.app.R
import java.text.SimpleDateFormat
import java.util.*

class ScheduledAppointmentAdapter(
    var mActivity: Activity?,
    var mScheduledAppointmentList: ArrayList<ScheduleAppointmentDataItem>,
    var mCancelAppointmentInterface: CancelAppointmentInterface
) :
    RecyclerView.Adapter<ScheduledAppointmentAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_scheduled_appointment, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel = mScheduledAppointmentList!![position]
        holder.txtApptTitleTV.text = mModel?.appointmentTitle
        holder.txtApptDate.text = mModel?.appointDate
        holder.txtApptTimeTV.text = mModel?.appointStartTime + " - " + mModel?.appointEndTime
        holder.txtDescriptionTV.text = mModel?.description
        holder.txtDiseaseTV.text = mModel?.disease
        holder.txtApptTypeTV.text = mModel?.appointmentType

        when (mModel?.patientType) {
            1 -> {
                holder.txtPatientTypeTV.text = mActivity!!.resources?.getString(R.string.adult)
            }
            2 -> {
                holder.txtPatientTypeTV.text = mActivity!!.resources?.getString(R.string.Child)
            }
        }


        //Time Date Comparison
        val c = Calendar.getInstance().time
        println("Current time => $c")
        val dateStr = SimpleDateFormat("MM-dd-yyyy h:mm a", Locale.getDefault())
        val formattedDate = dateStr.format(c)
        val strDate: Date = dateStr.parse(mModel.appointDate + " " + mModel.appointStartTime)
        val currentDate: Date = dateStr.parse(formattedDate)

         if (mModel.statusAppoint == 0) {
            holder.txtCancelAppointmentTV.setBackgroundResource(R.drawable.bg_accept_decline_unselected)
            holder.txtCancelAppointmentTV.text = "Cancelled"
        } else {
             if (strDate.before(currentDate)) {
                 holder.txtCancelAppointmentTV.text = mActivity?.resources?.getString(R.string.cancel_appointment)
                 holder.txtCancelAppointmentTV.setBackgroundResource(R.drawable.bg_accept_decline_unselected)
             }else {
                 holder.txtCancelAppointmentTV.setBackgroundResource(R.drawable.bg_accept_decline_selected)
                 holder.txtCancelAppointmentTV.setOnClickListener {
                     mCancelAppointmentInterface.cancelAppointment(position, mModel.id)
                 }
             }

        }

    }

    override fun getItemCount(): Int {
        return mScheduledAppointmentList!!.size
    }


    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var txtPatientTypeTV: TextView = itemView.findViewById(R.id.txtPatientTypeTV)
        var txtApptTitleTV: TextView = itemView.findViewById(R.id.txtApptTitleTV)
        var txtDiseaseTV: TextView = itemView.findViewById(R.id.txtDiseaseTV)
        var txtApptDate: TextView = itemView.findViewById(R.id.txtApptDate)
        var txtApptTimeTV: TextView = itemView.findViewById(R.id.txtApptTimeTV)
        var txtApptTypeTV: TextView = itemView.findViewById(R.id.txtApptTypeTV)
        var txtDescriptionTV: TextView = itemView.findViewById(R.id.txtDescriptionTV)
        var txtCancelAppointmentTV: TextView = itemView.findViewById(R.id.txtCancelAppointmentTV)
    }
}
