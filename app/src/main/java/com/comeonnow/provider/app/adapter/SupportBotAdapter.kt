package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.content.ContentValues.TAG
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.interfaces.QuestionSelectInterface
import com.comeonnow.provider.app.interfaces.SessionCompleteInterface
import com.comeonnow.provider.app.interfaces.SessionPausedInterface
import com.comeonnow.provider.app.model.GetAllQuestionsAnswersDataItem
import com.comeonnow.provider.app.model.QlistItem
import com.comeonnow.provider.app.model.SubquestionItem
import com.comeonnow.provider.app.ui.activity.SupportBotActivity.Companion.isPreviousMenuClicked
import com.comeonnow.provider.app.util.LEFT_VIEW_HOLDER
import com.comeonnow.provider.app.util.RIGHT_VIEW_HOLDER
import com.comeonow.provider.app.R
import com.eyalbira.loadingdots.LoadingDots

class SupportBotAdapter(
    val activity: Activity,
    private val dummyArrayList: ArrayList<String>,
    private val questionAnswersArrayList: ArrayList<GetAllQuestionsAnswersDataItem>,
    var questionSelectInterface: QuestionSelectInterface,
    var sessionCompleteInterface: SessionCompleteInterface,
    var sessionPausedInterface: SessionPausedInterface
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == RIGHT_VIEW_HOLDER) {
            val mView: View =
                LayoutInflater.from(activity).inflate(R.layout.item_right_viewholder, parent, false)
            return ItemRightViewHolder(mView)
        } else if (viewType == LEFT_VIEW_HOLDER) {
            val mView: View =
                LayoutInflater.from(activity).inflate(R.layout.item_left_viewholder, parent, false)
            return ItemLeftViewHolder(mView)
        }
        return null!!
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder.itemViewType == RIGHT_VIEW_HOLDER) {
            (holder as ItemRightViewHolder).bindData(
                activity,
                position,
                dummyArrayList,
                questionSelectInterface,
                sessionPausedInterface,
                questionAnswersArrayList
            )
        } else if (holder.itemViewType == LEFT_VIEW_HOLDER) {
            (holder as ItemLeftViewHolder).bindData(
                activity,
                position,
                questionSelectInterface,
                questionAnswersArrayList,
                dummyArrayList,
                sessionCompleteInterface,
                sessionPausedInterface
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position % 2 == 0) LEFT_VIEW_HOLDER
        else if (position % 2 == 1) RIGHT_VIEW_HOLDER
        else -1
    }

    override fun getItemCount(): Int {
        return dummyArrayList.size
    }

    class ItemRightViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val answerTV: TextView = itemView.findViewById(R.id.answerTV)

        fun bindData(
            activity: Activity,
            position: Int,
            dummyArrayList: ArrayList<String>,
            questionSelectInterface: QuestionSelectInterface,
            sessionPausedInterface: SessionPausedInterface,
            questionAnswersArrayList: ArrayList<GetAllQuestionsAnswersDataItem>
        ) {
            answerTV.visibility = View.VISIBLE
            answerTV.text = dummyArrayList[position]
            Handler().postDelayed({
                when (position) {
                    1 -> {
                        questionSelectInterface.questionSelect("question")
                        sessionPausedInterface.sessionPaused()
                    }
                    3 -> {
                        questionSelectInterface.questionSelect("subQuestion")
                        sessionPausedInterface.sessionPaused()
                    }
                    5 -> {
                        questionSelectInterface.questionSelect("subQuestion")
                        sessionPausedInterface.sessionPaused()
                    }
                    7 -> {
                        questionSelectInterface.questionSelect("answer")
                        sessionPausedInterface.sessionPaused()
                    }
                }
            }, 10)
        }
    }

    class ItemLeftViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var questionsAdpater: QuestionsAdpater
        private lateinit var topicsAdpater: TopicsAdpater
        private lateinit var subQuestionsAdpater: SubQuestionsAdpater
        private lateinit var subQuestionsAdpater2: SubQuestionsAdpater2
        private var questionsRV: RecyclerView = itemView.findViewById(R.id.questionsRV)
        private var typingDotsV: LoadingDots = itemView.findViewById(R.id.typingDotsV)
        private var mainLL: LinearLayout = itemView.findViewById(R.id.mainLL)
        private var optionTV: TextView = itemView.findViewById(R.id.optionTV)

        fun bindData(
            activity: Activity,
            position: Int,
            questionSelectInterface: QuestionSelectInterface,
            qlist: ArrayList<GetAllQuestionsAnswersDataItem>,
            dummyArrayList: ArrayList<String>,
            sessionCompleteInterface: SessionCompleteInterface,
            sessionPausedInterface: SessionPausedInterface
        ) {
            if (isPreviousMenuClicked) {
                mainLL.visibility = View.VISIBLE
                when (position) {
                    0 -> {
                        setTopicsAdapter(activity, qlist, questionSelectInterface)
                    }
                    2 -> {
                        when {
                            dummyArrayList[position] != "answer" -> {
                                var newPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist.size)
                                        if (dummyArrayList[i] == qlist[j].topic) {
                                            newPos = j
                                        }
                                }
                                setQuestionsAdapter(
                                    activity,
                                    qlist[newPos].qlist,
                                    questionSelectInterface
                                )
                                sessionPausedInterface.sessionPaused()
                            }
                            else -> {
                                var newPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist.size)
                                        if (dummyArrayList[i] == qlist[j].topic) {
                                            newPos = j
                                        }
                                }
                                var latestPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist[newPos].qlist.size) {
                                        if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                            latestPos = j
                                        }
                                    }

                                }
                                optionTV.text = qlist[newPos].qlist[latestPos].answers
                                sessionCompleteInterface.sessionComplete()
                                sessionPausedInterface.sessionPaused()
                            }
                        }
                    }
                    4 -> {
                        when {
                            dummyArrayList[position] != "answer" -> {
                                var newPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist.size)
                                        if (dummyArrayList[i] == qlist[j].topic) {
                                            newPos = j
                                        }
                                }
                                var latestPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist[newPos].qlist.size) {
                                        if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                            latestPos = j
                                        }
                                    }
                                }
                                setSubQuestionsAdapter(
                                    activity,
                                    qlist[newPos].qlist[latestPos].subquestion[0].subquestion,
                                    questionSelectInterface
                                )
                                sessionPausedInterface.sessionPaused()
                            }
                            else -> {
                                var newPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist.size)
                                        if (dummyArrayList[i] == qlist[j].topic) {
                                            newPos = j
                                        }
                                }
                                var latestPos = 0
                                for (i in 0 until dummyArrayList.size) {
                                    for (j in 0 until qlist[newPos].qlist.size) {
                                        if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                            latestPos = j
                                        }
                                    }
                                }
                                optionTV.text =
                                    qlist[newPos].qlist[latestPos].subquestion[0].answers
                                sessionCompleteInterface.sessionComplete()
                                sessionPausedInterface.sessionPaused()

                            }
                        }

                    }
                    else -> {
                        var newPos = 0
                        for (i in 0 until dummyArrayList.size) {
                            for (j in 0 until qlist.size)
                                if (dummyArrayList[i] == qlist[j].topic) {
                                    newPos = j
                                }
                        }
                        var latestPos = 0
                        for (i in 0 until dummyArrayList.size) {
                            for (j in 0 until qlist[newPos].qlist.size) {
                                if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                    latestPos = j
                                }
                            }
                        }
                        optionTV.text = qlist[newPos].qlist[latestPos].subquestion[0].answers
                        sessionCompleteInterface.sessionComplete()
                        sessionPausedInterface.sessionPaused()
                    }
                }
                isPreviousMenuClicked = false
            } else {
                Handler().postDelayed({
                    typingDotsV.visibility = View.GONE
                    mainLL.visibility = View.VISIBLE
                    val animLeftToRight =
                        AnimationUtils.loadAnimation(activity, R.anim.anim_left_to_right);
                    itemView.startAnimation(animLeftToRight)
                    when (position) {
                        0 -> {
                            setTopicsAdapter(activity, qlist, questionSelectInterface)
                        }
                        2 -> {
                            when {
                                dummyArrayList[position] != "answer" -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }

                                    setQuestionsAdapter(activity, qlist[newPos].qlist, questionSelectInterface)
                                    sessionPausedInterface.sessionPaused()

                                }


                                else -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }
                                    var latestPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                                latestPos = j
                                            }
                                        }
                                    }
                                    optionTV.text = qlist[newPos].qlist[latestPos].answers
                                    sessionCompleteInterface.sessionComplete()
                                    sessionPausedInterface.sessionPaused()
                                }
                            }
                        }
                        4 -> {
                            when {
                                dummyArrayList[position] != "answer" -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }
                                    var latestPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                                latestPos = j
                                            }
                                        }
                                    }

                                    if(qlist[newPos].qlist[latestPos].subquestion.isNullOrEmpty()){
                                        optionTV.text = qlist[newPos].qlist[latestPos].answers
                                        sessionCompleteInterface.sessionComplete()
                                        sessionPausedInterface.sessionPaused()
                                    }else {
                                        setSubQuestionsAdapter(activity, qlist[newPos].qlist[latestPos].subquestion, questionSelectInterface)
                                        sessionPausedInterface.sessionPaused()
                                    }
                                }
                                else -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }
                                    var latestPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                                latestPos = j
                                            }
                                        }
                                    }
                                    optionTV.text = qlist[newPos].qlist[latestPos].answers
                                    sessionCompleteInterface.sessionComplete()
                                    sessionPausedInterface.sessionPaused()

                                }
                            }

                        }
                        6 -> {
                            when {
                                dummyArrayList[position] != "answer" -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }
                                    var latestPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                                latestPos = j
                                            }
                                        }
                                    }

                                    var finalPos =0

                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist[latestPos].subquestion.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[latestPos].subquestion[j].questions) {
                                                finalPos = j
                                            }
                                        }
                                    }

                                    if(qlist[newPos].qlist[latestPos].subquestion[finalPos].subquestion.isNullOrEmpty()){
                                        optionTV.text = qlist[newPos].qlist[latestPos].subquestion[finalPos].answers
                                        sessionCompleteInterface.sessionComplete()
                                        sessionPausedInterface.sessionPaused()
                                    }else {
                                        setSubQuestions2Adapter(activity, qlist[newPos].qlist[latestPos].subquestion[finalPos].subquestion, questionSelectInterface)
                                        sessionPausedInterface.sessionPaused()
                                    }
                                }
                                else -> {
                                    var newPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist.size)
                                            if (dummyArrayList[i] == qlist[j].topic) {
                                                newPos = j
                                            }
                                    }
                                    var latestPos = 0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                                latestPos = j
                                            }
                                        }
                                    }

                                    var finalPos =0
                                    for (i in 0 until dummyArrayList.size) {
                                        for (j in 0 until qlist[newPos].qlist[latestPos].subquestion.size) {
                                            if (dummyArrayList[i] == qlist[newPos].qlist[latestPos].subquestion[j].questions) {
                                                finalPos = j
                                            }
                                        }
                                    }
                                    optionTV.text = qlist[newPos].qlist[latestPos].subquestion[finalPos].answers
                                    sessionCompleteInterface.sessionComplete()
                                    sessionPausedInterface.sessionPaused()

                                }
                            }

                        }

                        else -> {
                            var newPos = 0
                            for (i in 0 until dummyArrayList.size) {
                                for (j in 0 until qlist.size)
                                    if (dummyArrayList[i] == qlist[j].topic) {
                                        newPos = j
                                    }
                            }
                            var latestPos = 0
                            for (i in 0 until dummyArrayList.size) {
                                for (j in 0 until qlist[newPos].qlist.size) {
                                    if (dummyArrayList[i] == qlist[newPos].qlist[j].questions) {
                                        latestPos = j
                                    }
                                }
                            }

                            var finalPos =0

                            for (i in 0 until dummyArrayList.size) {
                                for (j in 0 until qlist[newPos].qlist[latestPos].subquestion.size) {
                                    if (dummyArrayList[i] == qlist[newPos].qlist[latestPos].subquestion[j].questions) {
                                        finalPos = j
                                    }
                                }
                            }

                            var ultimatePos =0

                            for (i in 0 until dummyArrayList.size) {
                                for (j in 0 until qlist[newPos].qlist[latestPos].subquestion[finalPos].subquestion.size) {
                                    if (dummyArrayList[i] == qlist[newPos].qlist[latestPos].subquestion[finalPos].subquestion[j].questions) {
                                        ultimatePos = j
                                    }
                                }
                            }

                            optionTV.text = qlist[newPos].qlist[latestPos].subquestion[finalPos].subquestion[ultimatePos].answers
                            sessionCompleteInterface.sessionComplete()
                            sessionPausedInterface.sessionPaused()
                        }
                    }
                }, 2000)
            }
        }

        private fun setQuestionsAdapter(
            activity: Activity,
            qlist: ArrayList<QlistItem>,
            questionSelectInterface: QuestionSelectInterface
        ) {
            questionsAdpater = QuestionsAdpater(activity, qlist, questionSelectInterface)
            questionsRV.adapter = questionsAdpater
        }

        private fun setTopicsAdapter(
            activity: Activity,
            qlist: ArrayList<GetAllQuestionsAnswersDataItem>,
            questionSelectInterface: QuestionSelectInterface
        ) {
            topicsAdpater = TopicsAdpater(activity, qlist, questionSelectInterface)
            questionsRV.adapter = topicsAdpater
        }

        private fun setSubQuestionsAdapter(
            activity: Activity,
            subQuestionList: ArrayList<SubquestionItem>,
            questionSelectInterface: QuestionSelectInterface
        ) {
            subQuestionsAdpater =
                SubQuestionsAdpater(activity, subQuestionList, questionSelectInterface)
            questionsRV.adapter = subQuestionsAdpater
        }

        private fun setSubQuestions2Adapter(
            activity: Activity,
            subQuestionList: ArrayList<SubquestionItem>,
            questionSelectInterface: QuestionSelectInterface
        ) {
            subQuestionsAdpater2 = SubQuestionsAdpater2(activity, subQuestionList, questionSelectInterface)
            questionsRV.adapter = subQuestionsAdpater2
        }
    }
}