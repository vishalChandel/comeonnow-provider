package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.model.DocListModel
import com.comeonnow.provider.app.model.GetProfileModel
import com.comeonnow.provider.app.model.LoginModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.android.synthetic.main.activity_sign_in.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignInActivity : BaseActivity() {

    // - - Initialize Objects
    var mDeviceToken: String = ""
    var count: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        ButterKnife.bind(this)
        setEditTextFocused(editPasswordET)
        getDeviceToken()
    }

    @OnClick(
        R.id.txtForgotPasswordTV,
        R.id.btnLogInTV,
        R.id.showHideRL,
        R.id.rememberMeTV,
        R.id.txtSignUpTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtForgotPasswordTV -> performForgotPasswordClick()
            R.id.btnLogInTV -> performLogInClick()
            R.id.showHideRL -> setPasswordHideShow(editPasswordET, imgShowHidePwdIV)
            R.id.rememberMeTV -> performRememberMeClick()
            R.id.txtSignUpTV -> performSignUpClick()
        }
    }


    // - - To Get Device Token
    private fun getDeviceToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result!!
            Log.e("DeviceToken:", "DeviceToken:$mDeviceToken")
        })
    }


    private fun performForgotPasswordClick() {
        startActivity(Intent(mActivity, ForgotPasswordActivity::class.java))
    }

    private fun performRememberMeClick() {
        when {
            count -> {
                rememberMeTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0)
                count = false
                RememberMeAppPreference().writeBoolean(mActivity, IS_REMEMBER_ME, false)
                RememberMeAppPreference().writeString(mActivity, RM_EMAIL, "")
                RememberMeAppPreference().writeString(mActivity, RM_PASSWORD, "")
            }
            !count -> {
                rememberMeTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0)
                count = true
                RememberMeAppPreference().writeBoolean(mActivity, IS_REMEMBER_ME, true)
                RememberMeAppPreference().writeString(
                    mActivity,
                    RM_EMAIL,
                    editEmailET.text.toString().trim()
                )
                RememberMeAppPreference().writeString(
                    mActivity,
                    RM_PASSWORD,
                    editPasswordET.text.toString().trim()
                )
            }
        }
    }

    private fun performLogInClick() {
        if (isRememberMe()) {
            RememberMeAppPreference().writeString(mActivity, RM_EMAIL, editEmailET.text.toString().trim())
            RememberMeAppPreference().writeString(mActivity, RM_PASSWORD, editPasswordET.text.toString().trim())
        } else if (!isRememberMe()) {
            RememberMeAppPreference().writeString(mActivity, RM_EMAIL, "")
            RememberMeAppPreference().writeString(mActivity, RM_PASSWORD, "")
        }
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeLoginApi()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun performSignUpClick() {
        startActivity(Intent(mActivity, SignUpActivity::class.java))
        finish()
    }

    // - - Execute Login Api
    private fun executeLoginApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.loginRequest(
            editEmailET.text.toString(),
            editPasswordET.text.toString(),
            DEVICE_TYPE,
            mDeviceToken,
            USER_TYPE
        )
        call.enqueue(object : Callback<LoginModel> {
            override fun onFailure(call: Call<LoginModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<LoginModel>,
                response: Response<LoginModel>
            ) {
                dismissProgressDialog()
                when {
                    response.code() == 200 -> {
                        if (response.body()?.status == 401) {
                            showAlertDialog(mActivity, response.body()!!.message.toString())
                        } else if (response.body()?.status == 200) {
                            val mModel: LoginModel = response.body()!!
                            AppPreference().writeString(mActivity, ACCESS_TOKEN, "Bearer" + " " + response.body()?.accessToken)
                            Log.e(TAG, "TOKEN::" + response.body()?.accessToken)
                            mModel.badgeCount?.let {
                                AppPreference().writeInteger(mActivity, BADGE_COUNT, it)
                            }
                            executeGetProfileApi()
                        }

                    }
                    response.code() == 409 -> {
                        showAlertDialog(mActivity, getString(R.string.wrong_email_password))
                    }
                    response.code() == 401 -> {
                        showAlertDialog(mActivity, getString(R.string.wrong_email_password))
                    }
                    response.code() == 500 -> {
                        showAlertDialog(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - Execute Get Profile Api
    private fun executeGetProfileApi() {
        val call = ApiClient.apiInterface.getProfileRequest(getAccessToken())
        Log.e("ProfileParams:", "AccessToken:${getAccessToken()}")
        call.enqueue(object : Callback<GetProfileModel> {
            override fun onResponse(
                call: Call<GetProfileModel>,
                response: Response<GetProfileModel>
            ) {
                when (response.code()) {
                    200 -> {
                        val mModel: GetProfileModel = response.body()!!
                        AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                        AppPreference().writeString(mActivity, RM_EMAIL, editEmailET.text.toString().trim())
                        AppPreference().writeString(mActivity, RM_PASSWORD, editPasswordET.text.toString().trim())
                        AppPreference().writeString(mActivity, USER_ID, mModel.data?.id)
                        AppPreference().writeString(mActivity, FIRST_NAME, mModel.data?.firstName)
                        AppPreference().writeString(mActivity, LAST_NAME, mModel.data?.lastName)
                        AppPreference().writeString(mActivity, CELL_NO, mModel.data?.cellno)
                        AppPreference().writeString(mActivity, EMAIL, mModel.data?.email)
                        AppPreference().writeString(mActivity, COUNTRY_CODE, mModel.data?.countrycode)
                        AppPreference().writeString(mActivity, PROFILE_IMAGE, mModel.data?.image)
                        startActivity(Intent(mActivity, HomeActivity::class.java))
                        showToast(mActivity, getString(R.string.signin_successfully))
                        finish()
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<GetProfileModel>, t: Throwable) {
                dismissProgressDialog()

            }
        })
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
        }
        return flag
    }

    override fun onResume() {
        super.onResume()
        when {
            isRememberMe() -> {
                rememberMeTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_check, 0, 0, 0)
                editEmailET.setText(rememberMeEmail())
                editPasswordET.setText(rememberMePassword())
                count = true
            }
            !isRememberMe() -> {
                rememberMeTV.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_uncheck, 0, 0, 0)
                editEmailET.setText("")
                editPasswordET.setText("")
                count = false
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finishAffinity()
    }
}
