package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AddAppointmentModel(

	@field:SerializedName("data")
	val data: List<AddAppointmentDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AddAppointmentDataItem(

	@field:SerializedName("doctor_id")
	val doctorId: Int? = null,

	@field:SerializedName("appoint_start_time")
	val appointStartTime: String? = null,

	@field:SerializedName("disease")
	val disease: String? = null,

	@field:SerializedName("patient_type")
	val patientType: Int? = null,

	@field:SerializedName("appointment_type")
	val appointmentType: String? = "",

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("appointment_title")
	val appointmentTitle: String? = null,

	@field:SerializedName("doctor_name")
	val doctorName: String? = null,

	@field:SerializedName("appoint_end_time")
	val appointEndTime: String? = null,

	@field:SerializedName("appoint_date")
	val appointDate: String? = null
) : Parcelable
