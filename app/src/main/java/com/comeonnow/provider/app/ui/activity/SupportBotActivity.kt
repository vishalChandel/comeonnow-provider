package com.comeonnow.provider.app.ui.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.AnimationUtils
import android.widget.ScrollView
import android.widget.TextView
import com.comeonnow.provider.app.adapter.SupportBotAdapter
import com.comeonnow.provider.app.interfaces.QuestionSelectInterface
import com.comeonnow.provider.app.interfaces.SessionCompleteInterface
import com.comeonnow.provider.app.interfaces.SessionPausedInterface
import com.comeonnow.provider.app.model.GetAllQuestionsAnswersDataItem
import com.comeonnow.provider.app.model.GetAllQuestionsAnswersModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_support_bot.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SupportBotActivity : BaseActivity() {

    lateinit var supportBotAdapter: SupportBotAdapter
    private var questionAnswersArrayList: ArrayList<GetAllQuestionsAnswersDataItem> = ArrayList()
    private var dummyArrayList: ArrayList<String> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support_bot)
        onViewClicked()
    }

    private fun initView() {
        val animLeftToRight =
            AnimationUtils.loadAnimation(applicationContext, R.anim.anim_left_to_right)
        mainRL.startAnimation(animLeftToRight)
        isSubQuestion2Clickable = true
        isSubQuestionClickable = true
        isQuestionClickable = true
        isTopicClickable = true
        isFaqClickable = true
        isPreviousMenuClicked = false
        setDataOnWidgets()
        executeGetAllQuestionAnswersApi()
    }

    private fun setDataOnWidgets() {
        welcomeTV.text =
            "Hi " + lastName() + "," + " " + firstName() + " welcome to Come On Now! Support Bot"
    }

    private fun onViewClicked() {
        backRL.setOnClickListener {
            onBackPressed()
        }

        faqTV.setOnClickListener {
            if (isFaqClickable) {
                dummyArrayList.add(0, "topic")
                faqTV1.visibility = View.VISIBLE
                setAdapter()
                supportBotRV.visibility = View.VISIBLE
                isFaqClickable = false
            }
        }

        chatSupportTV.setOnClickListener {
            val i = Intent(this@SupportBotActivity, SupportChatActivity::class.java)
            startActivity(i)
        }

        supportTV.setOnClickListener {
            val i = Intent(this@SupportBotActivity, SupportChatActivity::class.java)
            startActivity(i)
        }

        previousMenuTV.setOnClickListener {
            isQuestionClickable = true
            isTopicClickable = true
            isSubQuestionClickable = true
            isSubQuestion2Clickable = true
            val obj = dummyArrayList[0]
            dummyArrayList.clear()
            supportBotAdapter.notifyDataSetChanged()
            dummyArrayList.add(obj)
            setAdapter()
            menuLL.visibility = View.GONE
            isPreviousMenuClicked = true
        }

        mainMenuTV.setOnClickListener {
            isQuestionClickable = true
            isTopicClickable = true
            isFaqClickable = true
            isSubQuestionClickable = true
            isSubQuestion2Clickable = true
            dummyArrayList.clear()
            supportBotAdapter.notifyDataSetChanged()
            faqTV1.visibility = View.GONE
            menuLL.visibility = View.GONE
        }
    }


    private fun executeGetAllQuestionAnswersApi() {
        showProgressDialog(mActivity)
        val mMyClinicsData = ApiClient.apiInterface.getAllQuestionAnswersRequest(getAccessToken())
        mMyClinicsData.enqueue(object : Callback<GetAllQuestionsAnswersModel> {
            override fun onResponse(
                call: Call<GetAllQuestionsAnswersModel>,
                response: Response<GetAllQuestionsAnswersModel>
            ) {
                dismissProgressDialog()
                questionAnswersArrayList.clear()
                when {
                    response.code() == 200 -> {
                        val mModel: GetAllQuestionsAnswersModel = response.body()!!
                        if (mModel.data.size > 0) {
                            questionAnswersArrayList.addAll(mModel.data)
                            if (dummyArrayList.isNotEmpty()) {
                                dummyArrayList.clear()
                            }
                        }
                    }

                    response.code() == 404 -> {
                    }

                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }

                    response.code() == 500 -> {

                    }
                }
            }

            override fun onFailure(call: Call<GetAllQuestionsAnswersModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    private fun setAdapter() {
        supportBotAdapter = SupportBotAdapter(
            mActivity,
            dummyArrayList,
            questionAnswersArrayList,
            questionSelectInterface,
            sessionCompleteInterface,
            sessionPausedInterface
        )
        supportBotRV.adapter = supportBotAdapter
    }

    // - - Implements Interface on Question Click
    private var questionSelectInterface: QuestionSelectInterface = object :
        QuestionSelectInterface {
        override fun questionSelect(selectionString: String) {
            super.questionSelect(selectionString)
            val insertIndex = dummyArrayList.size
            dummyArrayList.add(insertIndex, selectionString)
            supportBotAdapter.notifyItemInserted(insertIndex)
            supportBotAdapter.notifyItemInserted(dummyArrayList.size - 1)
        }
    }

    // - - Implements Interface on Session Complete
    private var sessionCompleteInterface: SessionCompleteInterface = object :
        SessionCompleteInterface {
        override fun sessionComplete() {
            super.sessionComplete()
            menuLL.visibility = View.VISIBLE

        }
    }

    // - - Implements Interface on Session Complete
    private var sessionPausedInterface: SessionPausedInterface = object :
        SessionPausedInterface {
        override fun sessionPaused() {
            super.sessionPaused()
            scrollToBottom()
        }
    }

    private fun scrollToBottom() {
        Handler().postDelayed({
            scrollViewSV.fullScroll(ScrollView.FOCUS_DOWN)
        }, 100)
    }

    private fun showContactSupportDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_contact_support)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val contactTV = alertDialog.findViewById<TextView>(R.id.contactTV)
        val signoutTV = alertDialog.findViewById<TextView>(R.id.signoutTV)

        contactTV.setOnClickListener {
            alertDialog.dismiss()
            val intent = Intent(Intent.ACTION_SEND)
            val recipients = arrayOf("info@comeonnow.io")
            intent.putExtra(Intent.EXTRA_EMAIL, recipients)
            intent.type = "text/html"
            intent.setPackage("com.google.android.gm")
            startActivity(Intent.createChooser(intent, "Send mail"))
        }

        signoutTV.setOnClickListener {
            alertDialog.dismiss()
        }

        alertDialog.show()
    }

    override fun onResume() {
        super.onResume()
        Log.i(TAG, "onResume: " + dummyArrayList.size)
        faqTV1.visibility = View.GONE
        menuLL.visibility = View.GONE
        dummyArrayList.clear()
        setAdapter()
        initView()

    }

    companion object {
        var isPreviousMenuClicked = false
        var isQuestionClickable = true
        var isTopicClickable = true
        var isFaqClickable = true
        var isSubQuestionClickable = true
        var isSubQuestion2Clickable = true
    }
}