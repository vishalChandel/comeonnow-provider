package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.interfaces.UserSelectClickListner
import com.comeonnow.provider.app.adapter.SelectUsersAdapter
import com.comeonnow.provider.app.model.SelectUserModel
import com.comeonnow.provider.app.model.UserDataItem
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.LIST
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_select_users.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SelectUsersActivity : BaseActivity() {
    var mArrayList: ArrayList<UserDataItem> = ArrayList()
    var mSelectedArrayList: ArrayList<UserDataItem> = ArrayList()
    var mSelectUsersAdapter : SelectUsersAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_users)
        ButterKnife.bind(this)
        getIntentData()
        getUserListing()
        setUpSearch()
    }

    private fun getIntentData() {
        if (intent != null && intent.getParcelableArrayListExtra<UserDataItem>(LIST)!! != null && intent.getParcelableArrayListExtra<UserDataItem>(LIST)!!.isNotEmpty()){
            mSelectedArrayList = intent.getParcelableArrayListExtra<UserDataItem>(LIST)!!
        }
    }

    // - - Search Functionality
    private fun setUpSearch() {
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isNotEmpty()) {
                    imgCancelIV.visibility = View.VISIBLE
                } else {
                    imgCancelIV.visibility = View.GONE
                }
                //after the change calling the method and passing the search input
                filter(s.toString());
            }
        })
    }

    // - - Filter Country list
    private fun filter(text: String) {
        //new array list that will hold the filtered data
        val filterdList = ArrayList<UserDataItem>()
        //looping through existing elements
        for (s in mArrayList) {
            //if the existing elements contains the search input
            if (s!!.lastName?.toLowerCase()!!.contains(text.toLowerCase())) {
                //adding the element to filtered list
                filterdList.add(s)
            }
        }
        //calling a method of the adapter class and passing the filtered list
        mSelectUsersAdapter?.filterList(filterdList)
    }

    private fun getUserListing() {
        if (isNetworkAvailable(mActivity)){
            executeUserListingApi()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeUserListingApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.selectedUserListRequest(getAccessToken())
        call.enqueue(object : Callback<SelectUserModel> {
            override fun onResponse(call: Call<SelectUserModel>, response: Response<SelectUserModel>) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        mArrayList = response.body()?.data!!
                        if (mArrayList.size > 0){
                            txtNoDataFoundTV.visibility = View.GONE
                            setAdapter()
                        }else{
                            txtNoDataFoundTV.visibility = View.VISIBLE
                        }
                    }
                    401 -> {
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        txtNoDataFoundTV.visibility = View.VISIBLE
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
            override fun onFailure(call: Call<SelectUserModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    @OnClick(
        R.id.backRL,
        R.id.btnSelectUsersTV,
        R.id.imgCancelIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.btnSelectUsersTV -> performSelectUserClick()
            R.id.imgCancelIV -> performCancelClick()
        }
    }

    private fun performCancelClick() {
        editSearchET.setText("")
        setAdapter()
    }

    private fun performSelectUserClick() {
        if (mSelectedArrayList.isEmpty()){
            showAlertDialog(mActivity,getString(R.string.please_select_users))
        }else{
            val intent = Intent()
            intent.putExtra(LIST, mSelectedArrayList)
            setResult(555, intent)
            finish()
        }
    }


    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }


    private fun setAdapter() {
        for (i in 0 until mSelectedArrayList.size){
            for (j in 0 until mArrayList.size){
                if (mSelectedArrayList.get(i).patientId == mArrayList.get(j).patientId){
                    mArrayList.get(j).isSelected = true
                }
            }
        }
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        usersRV.layoutManager = layoutManager
        mSelectUsersAdapter = SelectUsersAdapter(mActivity, mArrayList,mUserSelectClickListner)
        usersRV.adapter = mSelectUsersAdapter
    }

    var mUserSelectClickListner : UserSelectClickListner = object : UserSelectClickListner{
        override fun onItemClickListner(mList : ArrayList<UserDataItem>) {
            super.onItemClickListner(mList)
            mSelectedArrayList = mList
            Log.e(TAG,"**SIZE**"+mSelectedArrayList.size)
        }
    }


}