package com.comeonnow.provider.app.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.model.ChangePasswordModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.RM_PASSWORD
import com.comeonnow.provider.app.util.RememberMeAppPreference
import com.comeonnow.provider.app.util.USER_TYPE
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_change_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(this)
        setEditTextFocused(editConfirmNewPasswordET)
    }

    @OnClick(
        R.id.backRL,
        R.id.btnSaveTV,
        R.id.showHide1RL,
        R.id.showHide2RL,
        R.id.showHide3RL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.btnSaveTV -> performSaveClick()
            R.id.showHide1RL -> setPasswordHideShow(editOldPasswordET, imgShowHidePwd1IV)
            R.id.showHide2RL -> setPasswordHideShow(editNewPasswordET, imgShowHidePwd2IV)
            R.id.showHide3RL -> setPasswordHideShow(editConfirmNewPasswordET, imgShowHidePwd3IV)
        }
    }

    private fun performSaveClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeChangePasswordApi()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }


    // - - Execute Change Password Api
    private fun executeChangePasswordApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.changePasswordRequest(
            getAccessToken(),
            editOldPasswordET.text.toString(),
            editNewPasswordET.text.toString(),
            USER_TYPE
        )
        Log.e("ChangePasswordParams:","AccessToken:${getAccessToken()},OldPassword:${editOldPasswordET.text},NewPassword:${editNewPasswordET.text},UserType:$USER_TYPE")
        call.enqueue(object : Callback<ChangePasswordModel> {
            override fun onFailure(call: Call<ChangePasswordModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<ChangePasswordModel>,
                response: Response<ChangePasswordModel>
            ) {
                dismissProgressDialog()
                val mModel = response.body()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity, mModel?.message)
                        RememberMeAppPreference().writeString(mActivity, RM_PASSWORD, editConfirmNewPasswordET.text.toString())
                        onBackPressed()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    403 -> {
                        showAlertDialog(mActivity, "Your old password is incorrect.")
                        editOldPasswordET.setText("")
                        editNewPasswordET.setText("")
                        editConfirmNewPasswordET.setText("")
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - Validations for Change password fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editOldPasswordET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_old_password))
                flag = false
            }
            editNewPasswordET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
                flag = false
            }
            editNewPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            editConfirmNewPasswordET.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
                flag = false
            }
            editNewPasswordET!!.text.toString().trim() != editConfirmNewPasswordET.text.toString()
                .trim() -> {
                showAlertDialog(mActivity, getString(R.string.the_new_re_typed))
                flag = false
            }
        }
        return flag
    }

}