package com.comeonnow.provider.app.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.provider.app.adapter.QueueAdapter
import com.comeonnow.provider.app.model.AppointmentListingModel
import com.comeonnow.provider.app.model.AptDataItem
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.ui.activity.AddQueueActivity
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.fragment_queue.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class QueueFragment : BaseFragment() {

    // - - Initialize Widgets
    @BindView(R.id.queueListRV)
    lateinit var queueListRV: RecyclerView

    // - - Initialize Objects
    lateinit var mUnbinder: Unbinder
    var mQueueAdapter: QueueAdapter? = null
    var mQueueList: ArrayList<AptDataItem?>? = ArrayList<AptDataItem?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_queue, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        return view
    }

    override fun onResume() {
        super.onResume()
        executeQueueListingApi()
    }

    @OnClick(
        R.id.addQueueLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.addQueueLL -> performAddQueueClick()
        }
    }

    private fun performAddQueueClick() {
        val intent = Intent(activity, AddQueueActivity::class.java)
        startActivity(intent)
    }

    // - - Execute Queue Listing Api
    private fun executeQueueListingApi() {
        showProgressDialog(activity)
        val call = ApiClient.apiInterface.appointmentListingRequest(getAccessToken(),"500","1","1")
        call.enqueue(object : Callback<AppointmentListingModel> {
            override fun onResponse(
                call: Call<AppointmentListingModel>,
                response: Response<AppointmentListingModel>
            ) {
                dismissProgressDialog()
                Log.i(TAG, "Response: "+"Success")
                when (response.code()) {

                    200 -> {
                        if (response.body()?.data != null) {
                            mQueueList = response.body()?.data as ArrayList<AptDataItem?>?
                            if (mQueueList?.size!! > 0) {
                                setAdapter(activity, mQueueList)
                                txtNoDataFoundTV.visibility = View.GONE
                            } else {
                                txtNoDataFoundTV.visibility = View.VISIBLE
                            }
                        }else{
                            txtNoDataFoundTV.visibility = View.VISIBLE
                        }
                    }
                    401 -> {
                        showAuthFailedDialog(activity)
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                    404 -> {

                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                    500 -> {
                        showToast(activity, getString(R.string.internal_server_error))
                        txtNoDataFoundTV.visibility=View.VISIBLE
                    }
                }
            }

            override fun onFailure(call: Call<AppointmentListingModel>, t: Throwable) {
                Log.i(TAG, "Response: "+"Failure")
                dismissProgressDialog()
            }
        })
    }

    private fun setAdapter(
        activity: FragmentActivity?,
        mQueueList: ArrayList<AptDataItem?>?
    ) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        queueListRV.layoutManager = layoutManager
        mQueueAdapter = QueueAdapter(activity,mQueueList)
        queueListRV.adapter = mQueueAdapter
    }
}
