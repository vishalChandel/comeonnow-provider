package com.comeonnow.provider.app.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.*
import android.net.Uri
import android.os.Bundle
import android.os.StrictMode
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.widget.TextView
import androidx.core.net.toUri
import androidx.exifinterface.media.ExifInterface
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.util.PDFTools
import com.comeonow.provider.app.R
import com.github.barteksc.pdfviewer.util.FitPolicy
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.rm.freedrawview.FreeDrawView
import kotlinx.android.synthetic.main.activity_sign_document.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.*


class SignDocumentActivity : BaseActivity() {
    private var xDown = 0f
    private var yDown = 0f
    private var dialogSignaturePad: BottomSheetDialog? = null
    var mSignBitmap: Bitmap? = null
    var signRotatedBitmap: Bitmap? = null
    private var strByteArray: ByteArray? = null
    var path: String? = null
    var docTitle = ""
    var docUri: Uri? = null
    var docUpdatedUri:Uri?=null

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_document)
        ButterKnife.bind(this)
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)
        getIntentData()
        val listener = View.OnTouchListener(function = { _, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    xDown = motionEvent.x
                    yDown = motionEvent.y
                }
                MotionEvent.ACTION_UP -> {
                }
                MotionEvent.ACTION_MOVE -> {
                    val xMove: Float = motionEvent.x
                    val yMove: Float = motionEvent.y
                    val distX: Float = xMove - xDown
                    val distY: Float = yMove - yDown
                    signIV.x = signIV.x + distX
                    signIV.y = signIV.y + distY
                }
            }
            true
        })
        signIV.setOnTouchListener(listener)
    }

    private fun getIntentData() {
        if (intent != null) {
            docTitle = intent.getStringExtra("selDocumentName").toString()
            docUri = intent.getStringExtra("selDocumentUri")!!.toUri()
            docTitleTV.text = docTitle
            showProgressDialog(mActivity)
            GlobalScope.launch {
                initPdfView(docUri)
            }

        }
    }

    @OnClick(
        R.id.backRL,
        R.id.signRL,
        R.id.saveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.signRL -> performSignClick()
            R.id.saveTV -> performSaveClick()
        }
    }

    private fun performSaveClick() {

        savePdf()

    }

    private fun savePdf() {
        val pdf = PDFTools(applicationContext)
        setImageRotation(mSignBitmap!!)
        val outputSignature: File = saveSignature(signRotatedBitmap!!)!!
        val tmpLocalFile = File("$cacheDir/sample.pdf")
        pdf.open(tmpLocalFile)
        val location = IntArray(2)
        signIV.getLocationOnScreen(location)
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels


        val scaleX = 540 / width.toFloat() * signIV.x
        val scaleY = 820 / height.toFloat() * signIV.y
        val signImagePosX = signIV.x
        val signImagePosY = signIV.y

        if (signIV.y > height / 2) {
            pdf.insertImage(
                outputSignature.path,
                scaleX.toInt() + 25,
                545 - scaleY.toInt(),
                240,
                100,
                pdfView.currentPage
            )
        } else {
            pdf.insertImage(
                outputSignature.path,
                scaleX.toInt() + 25,
                735 - scaleY.toInt(),
                240,
                100,
                pdfView.currentPage
            )
        }
        val returnIntent = Intent()
        returnIntent.putExtra("updatedFilePath", "$cacheDir/sample.pdf")
        setResult(RESULT_OK, returnIntent)
        finish()
    }


    private fun performSignClick() {
        signRL.visibility = View.GONE
        signIV.visibility = View.GONE
        saveTV.visibility = View.VISIBLE
        showSignaturePad()
    }

    private fun initPdfView(docUri: Uri?) {
        pdfView.fromUri(docUri)
            .defaultPage(0)
            .enableSwipe(true)
            .swipeHorizontal(false)
            .enableDoubletap(true)
            .pageFitPolicy(FitPolicy.BOTH)
            .pageSnap(true)
            .pageFling(true)
            .autoSpacing(true)
            .load()

        saveToFile(mActivity,docUri,"$cacheDir/sample.pdf")
        dismissProgressDialog()
    }

    private fun saveToFile(context: Context, uri: Uri?, filePath: String?) {
        var bis: BufferedInputStream? = null
        var bos: BufferedOutputStream? = null
        try {
            bis = BufferedInputStream(
                context.contentResolver
                    .openInputStream(uri!!)
            )
            bos = BufferedOutputStream(
                FileOutputStream(
                    filePath,
                    false
                )
            )
            val buffer = ByteArray(1024)
            bis.read(buffer)
            do {
                bos.write(buffer)
            } while (bis.read(buffer) !== -1)
        } catch (ioe: IOException) {
        } finally {
            try {
                bis?.close()
                bos?.close()
            } catch (e: IOException) {
            }
        }
    }

    private fun showSignaturePad() {
        dialogSignaturePad = BottomSheetDialog(mActivity)
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.bottom_sheet_signature_pad, null)
        dialogSignaturePad!!.setContentView(view)
        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)
        val signatureView =
            dialogSignaturePad!!.findViewById<com.rm.freedrawview.FreeDrawView>(R.id.signatureView)
        val cancelSignatureTV: TextView? =
            dialogSignaturePad!!.findViewById<TextView>(R.id.cancelSignatureTV)
        val doneSignatureTV: TextView? =
            dialogSignaturePad!!.findViewById<TextView>(R.id.doneSignatureTV)
        val eraseSignId: TextView? = dialogSignaturePad!!.findViewById<TextView>(R.id.eraseSignId)

        signatureView?.paintColor = resources.getColor(R.color.colorBlack)

        eraseSignId?.setOnClickListener {
            signatureView!!.undoLast()
        }

        cancelSignatureTV?.setOnClickListener {
            signRL.visibility=View.VISIBLE
            saveTV.visibility=View.GONE
            dialogSignaturePad?.dismiss()
        }

        doneSignatureTV?.setOnClickListener {
            signatureView!!.getDrawScreenshot(object : FreeDrawView.DrawCreatorListener {
                override fun onDrawCreated(draw: Bitmap) {
                    mSignBitmap = draw
                    signIV.visibility = View.VISIBLE
                    signIV?.setImageBitmap(draw)
                }

                override fun onDrawCreationError() {
                }
            })
            Log.e("Bitmap", "")
            dialogSignaturePad?.dismiss()


        }

        dialogSignaturePad!!.setCanceledOnTouchOutside(false)
        dialogSignaturePad!!.setCancelable(true)
        dialogSignaturePad!!.show()
    }

    private fun saveSignature(signature: Bitmap): File? {
        try {
            val output = File.createTempFile("signer", ".png", applicationContext.cacheDir)
            saveBitmapToPNG(signature, output)
            return output
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }

    @Throws(IOException::class)
    private fun saveBitmapToPNG(bitmap: Bitmap, photo: File) {
        val newBitmap = Bitmap.createBitmap(bitmap.width, bitmap.height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(newBitmap)
        canvas.drawColor(Color.TRANSPARENT)
        canvas.drawBitmap(bitmap, 0f, 0f, null)
        val stream: OutputStream = FileOutputStream(photo)
        newBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        stream.close()
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray? {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 512
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

    private fun setImageRotation(mSignBitmap: Bitmap) {
        val signFile: File = saveSignature(mSignBitmap)!!
        val ei = ExifInterface(signFile.path)
        signRotatedBitmap =
            when (ei.getAttributeInt(
                ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED
            )) {
                ExifInterface.ORIENTATION_ROTATE_90 -> rotateImage(mSignBitmap, 90F)
                ExifInterface.ORIENTATION_ROTATE_180 -> rotateImage(mSignBitmap, 180F)
                ExifInterface.ORIENTATION_ROTATE_270 -> rotateImage(mSignBitmap, 270F)
                ExifInterface.ORIENTATION_NORMAL -> mSignBitmap
                else -> mSignBitmap
            }


    }

    private fun rotateImage(source: Bitmap, angle: Float): Bitmap? {
        val matrix = Matrix()
        matrix.postRotate(angle)
        return Bitmap.createBitmap(
            source, 0, 0, source.width, source.height,
            matrix, true
        )
    }
}