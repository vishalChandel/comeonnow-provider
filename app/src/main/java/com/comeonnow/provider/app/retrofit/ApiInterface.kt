package com.comeonnow.provider.app.retrofit

import com.comeonnow.provider.app.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {


    @FormUrlEncoded
    @POST("api/v1/user/login")
    fun loginRequest(
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("device_type") device_type: String,
        @Field("device_token") device_token: String,
        @Field("usertype") userType: Int
    ): Call<LoginModel>

    @FormUrlEncoded
    @POST("api/v1/user/forgotpassword")
    fun forgotPasswordRequest(@Field("username") username: String, @Field("usertype") userType: Int
    ): Call<ForgotPasswordModel>

    @FormUrlEncoded
    @POST("api/v1/user/changepassword")
    fun changePasswordRequest(
        @Header("Authorization") Authorization: String,
        @Field("old_password") old_password: String,
        @Field("password") password: String,
        @Field("usertype") userType: Int
    ): Call<ChangePasswordModel>

    @FormUrlEncoded
    @POST("api/v1/user/logout")
    fun logoutRequest(
        @Header("Authorization") Authorization: String,
        @Field("") old_password: String,
        @Field("usertype") userType: Int
    ): Call<LogoutModel>

    @GET("api/v1/user/profile")
    fun getProfileRequest(
        @Header("Authorization") Authorization: String
    ): Call<GetProfileModel>

    @Multipart
    @POST("api/v1/user/profile")
    fun editProfileRequest(
        @HeaderMap Authorization: Map<String, String>?,
        @Part image: MultipartBody.Part?,
        @Part("first_name") first_name: RequestBody?,
        @Part("last_name") last_name: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part("cellno") cellno: RequestBody?,
        @Part("countrycode") countrycode: RequestBody?,
        @Part("usertype") usertype: RequestBody?
    ): Call<GetProfileModel>

    @GET("api/v1/user/get-notification-list")
    fun notificationListingRequest(
        @Header("Authorization") Authorization: String,
        @Query("per-page") perPage: String,
        @Query("page") page: String
    ): Call<NotificationModel>

    @GET("api/v1/providerappointment/appointment")
    fun appointmentListingRequest(
        @Header("Authorization") Authorization: String,
        @Query("per-page") perPage: String,
        @Query("page") page: String,
        @Query("status") status: String
    ): Call<AppointmentListingModel>

    @FormUrlEncoded
    @POST("api/v1/providerappointment/schedule-appointment")
    fun addAppointmentRequest(
        @Header("Authorization") Authorization: String,
        @Field("appointment_title") appointment_title: String,
        @Field("patient_type") patient_type: Int,
        @Field("disease") disease: String,
        @Field("appoint_date") appoint_date: String,
        @Field("appoint_start_time") appoint_start_time: String,
        @Field("appoint_end_time") appoint_end_time: String,
        @Field("appointment_type") appointment_type: String,
        @Field("description") description: String
    ): Call<AddAppointmentModel>

    @FormUrlEncoded
    @POST("api/v1/providerappointment/schedule-appointment-user")
    fun addAppointmentSelectUserRequest(
        @Header("Authorization") Authorization: String,
        @Field("appointment_title") appointment_title: String,
        @Field("patient_type") patient_type: Int,
        @Field("disease") disease: String,
        @Field("appoint_date") appoint_date: String,
        @Field("appoint_start_time") appoint_start_time: String,
        @Field("appoint_end_time") appoint_end_time: String,
        @Field("appointment_type") appointment_type: String,
        @Field("description") description: String,
        @Field("patient_id") patient_id: String
    ): Call<AddAppointmentModel>

    @GET("api/v1/queue/disease-list")
    fun diseaseListRequest(
        @Header("Authorization") Authorization: String
    ): Call<DiseaseModel>

    @GET("api/v1/providerappointment/appointmenttype-list")
    fun appointmentsTypeListRequest(
        @Header("Authorization") Authorization: String
    ): Call<AppointmentTypeModel>

    @GET("api/v1/providerappointment/schedule-appointment")
    fun scheduledListingRequest(
        @Header("Authorization") Authorization: String,
        @Query("per-page") perPage: String,
        @Query("page") page: String,
    ): Call<ScheduleAppointmentModel>

    @GET("api/v1/providerappointment/appointment-history")
    fun appointmentHistoryRequest(
        @Header("Authorization") Authorization: String,
        @Query("per-page") perPage: String,
        @Query("page") page: String
    ): Call<AppointmentHistoryModel>

    @GET("api/v1/providerappointment/patient-list")
    fun getQueuePatientListRequest(
        @Header("Authorization") Authorization: String
    ): Call<PatientModel>

    @GET("api/v1/queue/clinic-list")
    fun getHospitalsRequest(
        @Header("Authorization") Authorization: String
    ): Call<HospitalModel>?

    @GET("api/v1/queue/branch-list")
    fun getBranchesRequest(
        @Header("Authorization") Authorization: String,
        @Query("clinic_id") clinic_id: String
    ): Call<BranchModel>?

    @GET("api/v1/queue/provider-list")
    fun getProvidersRequest(
        @Header("Authorization") Authorization: String,
        @Query("clinic_id") clinic_id: String,
        @Query("branch_id") branch_id: String
    ): Call<ProviderModel>?

    @GET("api/v1/queue/disease-list")
    fun getDiseaseListRequest(
        @Header("Authorization") Authorization: String
    ): Call<DiseaseModel>?

    @FormUrlEncoded
    @POST("api/v1/providerappointment/queue")
    fun addQueueRequest(
        @HeaderMap  Authorization : Map<String, String>?,
        @Field("clinic_id") clinic_id: String,
        @Field("branch_id") branch_id: String,
        @Field("disease_id") disease_id: String,
        @Field("add_user_id") provider_id: String,
    ): Call<AddQueueModel>?



    @GET("api/v1/queue/queue-list")
    fun selectedUserListRequest(
        @Header("Authorization") Authorization: String
    ): Call<SelectUserModel>

    @GET("api/v1/user/defaultlang")
    fun defaultLangRequest(
        @Header("Authorization") Authorization: String,
        @Query("lang") lang: String
    ): Call<DefaultLangModel>

    @GET("api/v1/user/provider-clinic")
    fun providerClinicRequest(
        @Header("Authorization") Authorization: String,
    ): Call<ProviderClinicModel>

    @GET("api/v1/user/hospital-patient-list")
    fun hospitalPatientListRequest(
        @Header("Authorization") Authorization: String,
    ): Call<PatientModel>

    @Multipart
    @POST("api/v1/docs/upload-docs")
    fun uploadDocRequest(
        @HeaderMap Authorization: Map<String, String>?,
        @Part("title") title: RequestBody,
        @Part("description") description: RequestBody,
        @Part("patient_id") patient_id: RequestBody,
        @Part("receiver_id") receiver_id: RequestBody,
        @Part docs: MultipartBody.Part?,
    ): Call<UploadDocModel>

    @GET("api/v1/docs/docs-list")
    fun documentListRequest(
        @Header("Authorization") Authorization: String,
        @Query("per-page") perpage: String,
        @Query("page") page: String
    ): Call<DocListModel>


    @GET("api/v1/docs/docs-detail")
    fun documentDetailRequest(
        @Header("Authorization") Authorization: String,
        @Query("docs_id") docIdd: String,
    ): Call<DocumentDetailModel>


    @GET("api/v1/providerappointment/cancel")
    fun cancelAppointment(
        @Header("Authorization") Authorization: String,
        @Query("appointment_id") appointId: Int
    ): Call<CancelAppointmentModel>

    @GET("api/v1/queue/state-list")
    fun stateListRequest(): Call<StateListModel>?

    @GET("api/v1/queue/clinic-list")
    fun clinicListRequest(
        @Query("state_code") state_code: String,
    ): Call<ClinicListModel>

    @FormUrlEncoded
    @POST("api/v1/user/signup")
    fun signUpRequest(
        @Field("username") username: String,
        @Field("first_name") first_name: String,
        @Field("last_name") last_name: String,
        @Field("password") password: String,
        @Field("specialization") specialization: String,
        @Field("degree") degree: String,
        @Field("usertype") usertype: String,
        @Field("device_type") device_type: String,
        @Field("user_clinics") user_clinics:String,
        @Field("countrycode") countrycode: String,
        @Field("cellno") cellno: String,
    ): Call<SignUpModel>?

    @GET("api/v1/queue/provider-clinic")
    fun myClinicsListRequest(
        @Header("Authorization") Authorization: String,
    ): Call<MyClinicsModel>

    @GET("api/v1/queue/delete-provider-clinic")
    fun deleteMyClinicRequest(
        @Header("Authorization") Authorization: String,
        @Query("user_clinic") user_clinic: String
    ): Call<MyClinicsModel>

    @GET("api/v1/queue/add-provider-clinic")
    fun addMyClinicRequest(
        @Header("Authorization") Authorization: String,
        @Query("user_clinic") user_clinic: String
    ): Call<MyClinicsModel>

    @GET("api/v1/queue/edit-provider-clinic")
    fun editMyClinicRequest(
        @Header("Authorization") Authorization: String,
        @Query("user_clinic") user_clinic: String,
        @Query("remove_clinic") remove_clinic: String
    ): Call<MyClinicsModel>

    @GET("api/v1/queue/branch-listing")
    fun branchListRequest(
        @Query("clinic_id") clinic_id: String
    ): Call<BranchModel>?

    @FormUrlEncoded
    @POST("api/v1/user/clinicrequest")
    fun acceptRejectClinicRequest(
        @Header("Authorization") Authorization: String,
        @Field("clinic_id") clinic_id: String,
        @Field("type") type: String,
        @Field("notification_id") notification_id: String,
        ): Call<StatusMessageModel>

    @GET("api/v1/chatbot/chatbot-listing")
    fun getAllQuestionAnswersRequest(
        @Header("Authorization") Authorization: String,
    ): Call<GetAllQuestionsAnswersModel>

    @FormUrlEncoded
    @POST("api/v1/chatroom/createroom")
    fun createRoomRequest(
        @Header("Authorization") Authorization: String,
        @Field("other_id") otherId: String,
    ): Call<CreateRoomResponse>

    @FormUrlEncoded
    @POST("api/v1/chatroom/sendmsg")
    fun sendMessageRequest(
        @Header("Authorization") Authorization: String,
        @Field("message") message: String,
        @Field("room_id") roomId: String,
    ): Call<SendMessageResponse>

    @GET("api/v1/chatroom/allchat")
    fun getAllMessagesRequest(
        @Header("Authorization") Authorization: String,
        @Query("room_id") roomId: String,
        @Query("page") pageNo: String,
        @Query("per_page") perPage: String,
    ): Call<GetAllMessagesResponse>
}