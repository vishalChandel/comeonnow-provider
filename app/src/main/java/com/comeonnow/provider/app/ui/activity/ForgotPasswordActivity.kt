package com.comeonnow.provider.app.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.model.ChangePwdModel
import com.comeonnow.provider.app.model.ForgotPasswordModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.USER_TYPE
import com.comeonow.provider.app.R
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        ButterKnife.bind(this)
        setEditTextFocused(editEmailET)
    }

    @OnClick(
        R.id.backRL,
        R.id.btnSubmitTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.btnSubmitTV -> performSubmitClick()
        }
    }

    private fun performSubmitClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeForgotPasswordApi()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    // - - Execute Forgot Password Api
    private fun executeForgotPasswordApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.  forgotPasswordRequest(editEmailET.text.toString(), USER_TYPE)
        Log.e("ForgotPasswordParams:","Email:${editEmailET.text},UserType:$USER_TYPE")
        call.enqueue(object : Callback<ForgotPasswordModel> {
            override fun onFailure(call: Call<ForgotPasswordModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<ForgotPasswordModel>,
                response: Response<ForgotPasswordModel>
            ) {
                dismissProgressDialog()
                val mModel = response.body()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity, mModel?.message)
                        onBackPressed()
                    }
                    404 -> {
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        val mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - Validations for Forgot Password fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
        }
        return flag
    }
}