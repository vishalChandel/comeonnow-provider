package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
data class SignUpModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class SignUpData(

	
	@field:SerializedName("plan_type")
	val planType: @RawValue  Any? = null,

	@field:SerializedName("payment_details")
	val paymentDetails: @RawValue  Any? = null,

	@field:SerializedName("allow_without_payment")
	val allowWithoutPayment: Int? = null,

	@field:SerializedName("countrycode")
	val countrycode: String? = null,

	@field:SerializedName("channel")
	val channel:@RawValue  Any? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("device_type")
	val deviceType: @RawValue Any? = null,

	@field:SerializedName("password_reset_token")
	val passwordResetToken: @RawValue Any? = null,

	@field:SerializedName("lastvisit")
	val lastvisit: @RawValue Any? = null,

	@field:SerializedName("last_message")
	val lastMessage: @RawValue Any? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("cellno")
	val cellno: String? = null,

	@field:SerializedName("stripe_cus_id")
	val stripeCusId: @RawValue Any? = null,

	@field:SerializedName("updated_at")
	val updatedAt: @RawValue Any? = null,

	@field:SerializedName("branch_id")
	val branchId: @RawValue Any? = null,

	@field:SerializedName("default_lang")
	val defaultLang: @RawValue Any? = null,

	@field:SerializedName("branch_name")
	val branchName: @RawValue Any? = null,

	@field:SerializedName("subscription_status")
	val subscriptionStatus: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("verification_token")
	val verificationToken: String? = null,

	@field:SerializedName("clinic_id")
	val clinicId: @RawValue Any? = null,

	@field:SerializedName("sms_status")
	val smsStatus: Int? = null,

	@field:SerializedName("image")
	val image: @RawValue Any? = null,

	@field:SerializedName("push_status")
	val pushStatus: @RawValue Any? = null,

	@field:SerializedName("address")
	val address: @RawValue Any? = null,

	@field:SerializedName("degree")
	val degree: @RawValue Any? = null,

	@field:SerializedName("user_clinics")
	val userClinics: @RawValue Any? = null,

	@field:SerializedName("usertype")
	val usertype: String? = null,

	@field:SerializedName("auth_key")
	val authKey: String? = null,

	@field:SerializedName("access_token")
	val accessToken: @RawValue Any? = null,

	@field:SerializedName("dob")
	val dob: @RawValue Any? = null,

	@field:SerializedName("device_token")
	val deviceToken: @RawValue Any? = null,

	@field:SerializedName("referral_code")
	val referralCode: @RawValue Any? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mail_status")
	val mailStatus: Int? = null,

	@field:SerializedName("last_message_time")
	val lastMessageTime: @RawValue Any? = null,

	@field:SerializedName("specialization")
	val specialization: @RawValue Any? = null,

	@field:SerializedName("user_request_clinic")
	val userRequestClinic: String? = null,

	@field:SerializedName("expire_at")
	val expireAt: @RawValue Any? = null,

	@field:SerializedName("user_location")
	val userLocation: @RawValue Any? = null,

	@field:SerializedName("trial_period")
	val trialPeriod: Int? = null,

	@field:SerializedName("state_code")
	val stateCode: String? = null,

	@field:SerializedName("age")
	val age: @RawValue Any? = null,

	@field:SerializedName("superuser")
	val superuser: Int? = null,

	@field:SerializedName("username")
	val username: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable
