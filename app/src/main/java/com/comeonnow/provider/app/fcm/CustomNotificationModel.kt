package com.comeonnow.provider.app.fcm

import java.io.Serializable

class CustomNotificationModel(
    val usertype: String,
    val message: String,
    val type: String,
    val title: String,
    val badgeCount: Int
) : Serializable
