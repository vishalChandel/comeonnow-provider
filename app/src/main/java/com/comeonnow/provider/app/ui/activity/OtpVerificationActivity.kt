package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.comeonnow.provider.app.util.AppPreference
import com.comeonnow.provider.app.util.CELL_NO
import com.comeonnow.provider.app.util.COUNTRY_CODE
import com.comeonow.provider.app.R
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.*
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import kotlinx.android.synthetic.main.activity_otp_verification.*
import java.util.concurrent.TimeUnit


class OtpVerificationActivity : BaseActivity() {

    private var auth: FirebaseAuth? = null
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    private var resendToken: ForceResendingToken? = null
    private var storedVerificationId = ""
    private var mEmail = ""
    private var mFirstName = ""
    private var mLastName = ""
    private var mPassword = ""
    private var mDegree = ""
    private var mSpecialization = ""
    private var mCountryName = ""
    private var mCountryCode = ""
    private var mPhoneNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)
        getIntentData()
        onViewClicked()
    }

    private fun onViewClicked() {
        verifyET.setOnClickListener {
            showProgressDialog(mActivity)
            verifyVerificationCode(codeET.text.trim().toString())
        }

        backRL.setOnClickListener {
            finish()
        }

        resend_code.setOnClickListener {
            PhoneAuthProvider.getInstance().verifyPhoneNumber(
                mPhoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                callbacks, resendToken
            );
        }
    }


    private fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra("tag") == "signinActivity") {
                mEmail = intent.getStringExtra("email").toString()
                mFirstName = intent.getStringExtra("firstName").toString()
                mLastName = intent.getStringExtra("lastName").toString()
                mPassword = intent.getStringExtra("password").toString()
                mDegree = intent.getStringExtra("degree").toString()
                mSpecialization = intent.getStringExtra("specialization").toString()
                mCountryName = intent.getStringExtra("countryName").toString()
                mPhoneNumber = intent.getStringExtra("phoneNumber").toString()
                mCountryCode = intent.getStringExtra("countryCode").toString()
                numberTV.text = mCountryCode + mPhoneNumber.toString()
                verifyPhoneNumber(mCountryCode.trim() + mPhoneNumber.trim())
            } else if (intent.getStringExtra("tag") == "editProfileActivity") {
                mPhoneNumber = intent.getStringExtra("phoneNumber").toString()
                mCountryCode = intent.getStringExtra("countryCode").toString()
                numberTV.text = mCountryCode + mPhoneNumber.toString()
                verifyPhoneNumber(mCountryCode.trim() + mPhoneNumber.trim())
            }

        }
    }

    private fun verifyPhoneNumber(phoneNumber: String) {
        showProgressDialog(mActivity)
        auth = FirebaseAuth.getInstance()
        otpCallbacks()
        val options = PhoneAuthOptions.newBuilder(auth!!)
            .setPhoneNumber(phoneNumber)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(this)                 // Activity (for callback binding)
            .setCallbacks(callbacks)          // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun otpCallbacks() {
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:$credential")
            }

            override fun onVerificationFailed(e: FirebaseException) {
                dismissProgressDialog()
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                if (e is FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    Log.w(TAG, "onVerificationFailed", e)
                } else if (e is FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    Log.w(TAG, "onVerificationFailed", e)
                }
                // Show a message and update the UI
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
            ) {
                dismissProgressDialog()
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:$verificationId")

                // Save verification ID and resending token so we can use them later
                storedVerificationId = verificationId
                resendToken = token
            }
        }
    }


    private fun verifyVerificationCode(code: String) {
        val credential = PhoneAuthProvider.getCredential(storedVerificationId, code)
        signInWithPhoneAuthCredential(credential)
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth!!.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")
                    val user = task.result?.user
                    dismissProgressDialog()
                    if (intent.getStringExtra("tag") == "signinActivity") {
                        val i = Intent(this, NewClinicSelectionActivity::class.java)
                        i.putExtra("email", mEmail)
                        i.putExtra("firstName", mFirstName)
                        i.putExtra("lastName", mLastName)
                        i.putExtra("password", mPassword)
                        i.putExtra("degree", mDegree)
                        i.putExtra("specialization", mSpecialization)
                        i.putExtra("countryName", mCountryName)
                        i.putExtra("phoneNumber", mPhoneNumber)
                        startActivity(i)
                        finish()
                    } else if (intent.getStringExtra("tag") == "editProfileActivity") {
                        AppPreference().writeString(mActivity, CELL_NO, mPhoneNumber)
                        AppPreference().writeString(mActivity, COUNTRY_CODE, mCountryCode.substring(1))
                        finish()
                    }
                } else {
                    // Sign in failed, display a message and update the UI
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        // The verification code entered was invalid
                        dismissProgressDialog()
                        showAlertDialog(mActivity, getString(R.string.you_have_entered_wrong_otp))
                    }
                    // Update UI
                }
            }
    }


}