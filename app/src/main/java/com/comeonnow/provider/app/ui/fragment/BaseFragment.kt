package com.comeonnow.provider.app.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.util.DisplayMetrics
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.comeonnow.provider.app.ui.activity.SignInActivity
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import java.security.SecureRandom
import java.util.*
import javax.net.ssl.*

open class BaseFragment : Fragment() {

    // - - Get Class Name
    var TAG = this@BaseFragment.javaClass.simpleName

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    fun isEnglish(): Boolean {
        return activity?.let { AppPreference().readBoolean(it, IS_ENGLISH, true) }!!
    }

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return activity?.let { AppPreference().readBoolean(it, IS_LOGIN, false) }!!
    }


    // - - GET BASE URL
    fun getBaseUrl(): String {
        return activity?.let { AppPreference().readString(it, BASE_URL_PREF, "") }!!
    }

    // - - To Get Auth_Token
    fun getAccessToken(): String {
        return activity?.let { AppPreference().readString(it, ACCESS_TOKEN, "") }!!
    }

    // - - To Get First Name
    fun firstName(): String {
        return activity?.let { AppPreference().readString(it, FIRST_NAME, "") }!!
    }

    // - - To Get Last Name
    fun lastName(): String {
        return activity?.let { AppPreference().readString(it, LAST_NAME, "") }!!
    }

    // - - To Get User Email
    fun userEmail(): String {
        return activity?.let { AppPreference().readString(it, EMAIL, "") }!!
    }

    // - - To Get Profile Picture
    fun profilePicture(): String {
        return activity?.let { AppPreference().readString(it, PROFILE_IMAGE, "") }!!
    }
    fun badgeCount(): Int {
        return activity?.let{AppPreference().readInteger(it, BADGE_COUNT, 0) }!!
    }


    // - - To Check Internet Connection
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    // - - Authentication Failed Dialog
    fun showAuthFailedDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_auth_failed)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences = mActivity?.let { AppPreference().getPreferences(it)}!!
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, SignInActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }


    @SuppressLint("TrulyRandom")
    fun handleSSLHandshake() {
        try {
            val trustAllCerts: Array<TrustManager> =
                arrayOf<TrustManager>(object : X509TrustManager {
                    val acceptedIssuers: Array<Any?>?
                        get() = arrayOfNulls(0)

                    override fun checkClientTrusted(
                        p0: Array<out java.security.cert.X509Certificate>?,
                        p1: String?
                    ) {
                    }

                    override fun checkServerTrusted(
                        p0: Array<out java.security.cert.X509Certificate>?,
                        p1: String?
                    ) {
                    }

                    override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                        TODO("Not yet implemented")
                    }
                })
            val sc: SSLContext = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
            HttpsURLConnection.setDefaultHostnameVerifier(object : HostnameVerifier {
                override fun verify(arg0: String?, arg1: SSLSession?): Boolean {
                    return true
                }
            })
        } catch (ignored: java.lang.Exception) {
        }
    }

    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res: Resources = requireActivity().resources
        val dm: DisplayMetrics = res.displayMetrics
        val conf: Configuration = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
    }

}