package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.content.Intent
import android.provider.SyncStateContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.model.DocDataItem
import com.comeonnow.provider.app.ui.activity.DocumentDetailActivity
import com.comeonow.provider.app.R

class DocumentsAdapter(
    var mActivity: Activity?,
    var mArrayList: ArrayList<DocDataItem>
) :
    RecyclerView.Adapter<DocumentsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_documents, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.docTitle.text=mArrayList!![position].title
        holder.docDescription.text=mArrayList!![position].patientName
        holder.statusTV.text=mArrayList!![position].type
        if(mArrayList[position].type.equals("Sent"))
        {
            holder.circleRL.visibility= View.GONE
        }
        else {
            if(mArrayList[position].status.equals("1")) //Status 1 means Document Seen
            {
                holder.circleRL.visibility= View.GONE
            }
            else{
                holder.circleRL.visibility= View.VISIBLE
            }
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(mActivity, DocumentDetailActivity::class.java)
            intent.putExtra("DOC_ID",mArrayList[position].docsId)
            mActivity!!.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return if(mArrayList?.size==0){
            0
        }else {
            mArrayList!!.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var docTitle: TextView = itemView.findViewById(R.id.documentTV)
        var docDescription: TextView = itemView.findViewById(R.id.documentNameTV)
        var statusTV: TextView = itemView.findViewById(R.id.statusTV)
        var circleRL: RelativeLayout = itemView.findViewById(R.id.circleRL)
    }
}