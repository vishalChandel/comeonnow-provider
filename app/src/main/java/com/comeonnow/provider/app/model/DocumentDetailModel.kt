package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DocumentDetailModel(

	@field:SerializedName("docsName")
	val docsName: String? = null,

	@field:SerializedName("data")
	val data: List<DataItemX?>? = null,

	@field:SerializedName("docsDescription")
	val docsDescription: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class DataItemX(

	@field:SerializedName("docs")
	val docs: String? = null,

	@field:SerializedName("patient_id")
	val patientId: String? = null,

	@field:SerializedName("sign_status")
	val signStatus: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("reciever_id")
	val recieverId: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("sender_id")
	val senderId: String? = null,

	@field:SerializedName("docs_id")
	val docsId: String? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable
