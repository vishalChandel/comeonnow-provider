package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import com.comeonnow.provider.app.model.FlagModel
import com.comeonnow.provider.app.util.LINK_PP
import com.comeonnow.provider.app.util.LINK_TYPE
import com.comeonnow.provider.app.util.MODEL
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : BaseActivity() {
    // - - Initialize Objects
    var mCountryImage = "United States"
    private var isPriPolicyAgree = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        setEditTextFocused(editConfirmPasswordET)
        setPrivacyPolicyAgreeClick()
        onViewClicked()
    }

    fun onViewClicked() {
        txtLoginTV.setOnClickListener {
            performLoginClick()
        }
        showHide1RL.setOnClickListener {
            setPasswordHideShow(editPasswordET, imgShowHidePwd1IV)
        }
        showHide2RL.setOnClickListener {
            setPasswordHideShow(editConfirmPasswordET, imgShowHidePwd2IV)
        }
        txtPrivacyPolicyTV.setOnClickListener {
            performPricacyPolicyClick()
        }
        btnSignUpTV.setOnClickListener {
            performSignUpClick()
        }

        txtCountryCodeTV.setOnClickListener {
            performCountryCodeClick()
        }
    }

    private fun performCountryCodeClick() {
        val intent = Intent(mActivity, CountriesActivity::class.java)
        startActivityForResult(intent, 555)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, mIntent: Intent?) {
        super.onActivityResult(requestCode, resultCode, mIntent)

        if (requestCode == 555) {
            if (mIntent != null) {
                val mModel: FlagModel? = mIntent.getParcelableExtra(MODEL)
                txtCountryCodeTV.text = mModel!!.countryPhoneCode
                mCountryImage = mModel.countryImageName
            }
        }
    }

    private fun setPrivacyPolicyAgreeClick() {
        privacyPolicyMeCB.setOnCheckedChangeListener { _, isChecked ->
            isPriPolicyAgree = isChecked
        }
    }

    private fun performPricacyPolicyClick() {
        val i = Intent(mActivity, WebViewActivity::class.java)
        i.putExtra(LINK_TYPE, LINK_PP)
        startActivity(i)
    }

    private fun performSignUpClick() {
        if (isValidate()) {
            val i = Intent(this, OtpVerificationActivity::class.java)
            i.putExtra("tag","signinActivity")
            i.putExtra("email", editEmailET.text.toString().trim())
            i.putExtra("firstName", editFirstNameET.text.toString().trim())
            i.putExtra("lastName", editLastNameET.text.toString().trim())
            i.putExtra("password", editPasswordET.text.toString().trim())
            i.putExtra("degree", degreeET.text.toString())
            i.putExtra("specialization", specializationET.text.toString())
            i.putExtra("countryName", mCountryImage)
            i.putExtra("phoneNumber", editPhoneNoET.text.toString().trim())
            i.putExtra("countryCode", txtCountryCodeTV.text.toString().trim())
            startActivity(i)
        }
    }

    private fun performLoginClick() {
        startActivity(Intent(mActivity, SignInActivity::class.java))
        finish()
    }

    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editFirstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_first_name))
                flag = false
            }
            editLastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_last_name))
                flag = false
            }
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmailId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            degreeET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_degree))
                flag = false
            }
            specializationET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_specialization))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
            editPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            editConfirmPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_retype_password))
                flag = false
            }
            editPasswordET.text.toString().trim() != editConfirmPasswordET.text.toString()
                .trim() -> {
                showAlertDialog(mActivity, getString(R.string.the_pwd_re_typed))
                flag = false
            }
            !isPriPolicyAgree -> {
                showAlertDialog(mActivity, getString(R.string.please_agree))
                flag = false
            }
        }
        return flag
    }
}