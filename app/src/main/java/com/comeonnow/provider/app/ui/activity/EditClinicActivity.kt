package com.comeonnow.provider.app.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.comeonnow.provider.app.model.*
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_edit_clinic.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class EditClinicActivity : BaseActivity() {

    var mStateArrayList = ArrayList<StateDataItem>()
    var mClinicArrayList = ArrayList<ClinicDataItem>()
    private var mLastClickTime: Long = 0
    private var mStateCode = ""
    private var mClinicId = ""
    private var mRemoveClinicId = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_clinic)
        ButterKnife.bind(this)
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
            stateTV.text = intent.getStringExtra("mStateName")
            clinicTV.text = intent.getStringExtra("mClinicName")
            mRemoveClinicId= intent.getStringExtra("mClinicId").toString()
            mClinicId=intent.getStringExtra("mClinicId").toString()
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.clinicTV,
        R.id.stateTV,
        R.id.saveTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> finish()
            R.id.clinicTV -> performClinicClick()
            R.id.stateTV -> performStateClick()
            R.id.saveTV -> performSaveClick()
        }
    }


    private fun performStateClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (isNetworkAvailable(mActivity)) {
            executeGetStatesRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetStatesRequest() {
        stateSKV.visibility = View.VISIBLE
        stateTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetStateList = ApiClient.apiInterface.stateListRequest()
        mGetStateList!!.enqueue(object : Callback<StateListModel> {
            override fun onResponse(
                call: Call<StateListModel>,
                response: Response<StateListModel>
            ) {
                stateSKV.visibility = View.GONE
                stateTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: StateListModel = response.body()!!
                        if (!mStateArrayList.isNullOrEmpty()) {
                            mStateArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mStateArrayList.addAll(mModel.data)
                        }
                        createStateListingDialog(mActivity)
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<StateListModel>, t: Throwable) {
                Log.i(TAG, "onFailure: ")
                stateSKV.visibility = View.GONE
                stateTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }


    private fun performClinicClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mStateCode.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeGetClinicsRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_state_first))
        }
    }

    private fun executeGetClinicsRequest() {
        clinicSKV.visibility = View.VISIBLE
        clinicTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetClinicList = ApiClient.apiInterface.clinicListRequest(mStateCode)
        mGetClinicList.enqueue(object : Callback<ClinicListModel> {
            override fun onResponse(
                call: Call<ClinicListModel>,
                response: Response<ClinicListModel>
            ) {
                clinicSKV.visibility = View.GONE
                clinicTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: ClinicListModel = response.body()!!
                        if (!mClinicArrayList.isNullOrEmpty()) {
                            mClinicArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mClinicArrayList.addAll(mModel.data)
                        }
                        createClinicListingDialog(mActivity)
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createClinicListingDialog(mActivity)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<ClinicListModel>, t: Throwable) {
                clinicSKV.visibility = View.GONE
                clinicTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun createClinicListingDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList()

        if (mClinicArrayList.size > 0) {
            for (i in 0 until mClinicArrayList.size) {
                mDataList.add(mClinicArrayList[i].clinicName!!)
            }
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_clinic_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.text = getString(R.string.clinic)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)
        txtNoBranchFoundTV.text = getString(R.string.no_clinics_found)
        if (mDataList.size > 0) {
            txtNoBranchFoundTV.visibility = View.GONE
            txtSaveTV.visibility = View.VISIBLE
        } else {
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility = View.GONE
        }
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener {
            mBottomSheetDialog.dismiss()
            if (!mClinicArrayList.isNullOrEmpty()) {
                mClinicArrayList.clear()
            }
        }
        txtSaveTV.setOnClickListener {
            mClinicId = "" + mClinicArrayList[mWheelPickerWP.currentItemPosition].clinicId
            clinicTV.text = mDataList[mWheelPickerWP.currentItemPosition]
            mBottomSheetDialog.dismiss()
        }
    }

    private fun performSaveClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()

        if(stateTV.text.trim().isEmpty()){
            showAlertDialog(mActivity, getString(R.string.please_select_state))
        }else {
            if (clinicTV.text.trim().isNotEmpty()) {
                if (isNetworkAvailable(mActivity)) {
                    executeEditClinicRequest()
                } else {
                    showToast(mActivity, getString(R.string.internet_connection_error))
                }
            } else {
                showAlertDialog(mActivity, getString(R.string.please_select_clinic))
            }
        }
    }

    private fun executeEditClinicRequest() {
        showProgressDialog(mActivity)
        val mEditMyClinicRequest = ApiClient.apiInterface.editMyClinicRequest(getAccessToken(), mClinicId,mRemoveClinicId)
        mEditMyClinicRequest.enqueue(object : Callback<MyClinicsModel> {
            override fun onResponse(
                call: Call<MyClinicsModel>,
                response: Response<MyClinicsModel>
            ) {
                dismissProgressDialog()
                when {
                    response.code() == 200 -> {
                        showToast(mActivity, response.body()?.message)
                        finish()
                    }

                    response.code() == 401 -> {
                        dismissProgressDialog()
                        showAuthFailedDialog(mActivity)
                    }

                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }

                    else -> {
                        val gson = GsonBuilder().create()
                        val mError: SignUpErrorModel
                        try {
                            mError = gson.fromJson(
                                response.errorBody()!!.string(),
                                SignUpErrorModel::class.java
                            )
                            showAlertDialog(mActivity, mError.message)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                }
            }

            override fun onFailure(call: Call<MyClinicsModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    private fun createStateListingDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList()

        if (mStateArrayList.size > 0) {
            for (i in 0 until mStateArrayList.size) {
                mDataList.add(mStateArrayList[i].stateName!!)
            }
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_clinic_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.text = getString(R.string.search_state)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)
        txtNoBranchFoundTV.text = getString(R.string.no_clinics_found)
        if (mDataList.size > 0) {
            txtNoBranchFoundTV.visibility = View.GONE
            txtSaveTV.visibility = View.VISIBLE
        } else {
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility = View.GONE
        }
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }
        txtSaveTV.setOnClickListener {
            mStateCode = "" + mStateArrayList[mWheelPickerWP.currentItemPosition].stateCode
            stateTV.text = mDataList[mWheelPickerWP.currentItemPosition]
            clinicTV.text=""
            mBottomSheetDialog.dismiss()
        }
    }
}