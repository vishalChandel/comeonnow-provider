package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ScheduleAppointmentModel(

	@field:SerializedName("data")
	val data: ArrayList<ScheduleAppointmentDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ScheduleAppointmentDataItem(

	@field:SerializedName("doctor_id")
	val doctorId: Int? = null,

	@field:SerializedName("status")
	val statusAppoint: Int? = null,

	@field:SerializedName("appoint_start_time")
	val appointStartTime: String? = null,
	@field:SerializedName("first_name")
	val first_name: String? = null,
	@field:SerializedName("last_name")
	val last_name: String? = null,
	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("disease")
	val disease: String? = null,

	@field:SerializedName("patient_type")
	val patientType: Int? = null,

	@field:SerializedName("appointment_type")
	val appointmentType: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("appointment_title")
	val appointmentTitle: String? = null,

	@field:SerializedName("doctor_name")
	val doctorName: String? = null,

	@field:SerializedName("appoint_end_time")
	val appointEndTime: String? = null,

	@field:SerializedName("appoint_date")
	val appointDate: String? = null
) : Parcelable
