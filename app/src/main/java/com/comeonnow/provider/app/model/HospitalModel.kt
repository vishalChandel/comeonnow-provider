package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class HospitalModel(

	@field:SerializedName("data")
	val data: ArrayList<HospitalDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class HospitalDataItem(

	@field:SerializedName("created_on")
	val createdOn: String? = null,

	@field:SerializedName("clinic_name")
	val clinicName: String? = null,

	@field:SerializedName("clinic_id")
	val clinicId: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable
