package com.comeonnow.provider.app.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BaseActivity() {

    // - - Initialize Objects
    var linkType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        ButterKnife.bind(this)
        getIntentData()
    }

    @OnClick(R.id.backRL)
    fun onClick(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Get Intent Data from Another Activity
    private fun getIntentData() {
        if (intent != null) {
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            when (linkType) {
                LINK_ABOUT -> {
                    txtHeadingTV.text = getString(R.string.about_us)
                    setUpWebView(ABOUT_WEB_LINK)
                }
                LINK_TERMS -> {
                    txtHeadingTV.text = getString(R.string.term_of_service)
                    setUpWebView(TERMS_WEB_LINK)
                }
                LINK_PP -> {
                    txtHeadingTV.text = getString(R.string.privacy_policy)
                    setUpWebView(PP_WEB_LINK)
                }
                else -> {
                    txtHeadingTV.text = getString(R.string.document)
                    Log.e(TAG, "***Url***$linkType")
                    if (linkType.endsWith(".pdf")){
                        setUpWebView("https://drive.google.com/viewerng/viewer?embedded=true&url=$linkType")
                    }else{
                        setUpWebView(linkType)
                    }
                }
            }
        }
    }

    // - - Opens WebView in Application
    private fun setUpWebView(mUrl: String) {
        // this will set up webView client
        linkWV.webViewClient = WebViewClient()
        // this will load the url of the website
        linkWV.loadUrl(mUrl)
        // this will enable the javascript settings
        linkWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        linkWV.settings.setSupportZoom(true)
        showProgressDialog(mActivity)
        linkWV.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                view.loadUrl(url)
                return true
            }

            override fun onPageFinished(view: WebView, url: String) {
                if (view.title.equals("")){
                    view.reload()
                }
                dismissProgressDialog()
            }

            override fun onReceivedError(
                view: WebView,
                errorCode: Int,
                description: String,
                failingUrl: String
            ) {
                Toast.makeText(mActivity, "Error:$description", Toast.LENGTH_SHORT).show()
            }
        }
    }

}