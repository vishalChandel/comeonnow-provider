package com.comeonnow.provider.app.ui.activity

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.adapter.AppointmentHistoryAdapter
import com.comeonnow.provider.app.model.AppointmentHistoryDataItem
import com.comeonnow.provider.app.model.AppointmentHistoryModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_history.*
import kotlinx.android.synthetic.main.activity_history.txtNoDataFoundTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class HistoryActivity : BaseActivity() {

    // - - Initialize Objects
    var appointmentHistoryAdapter: AppointmentHistoryAdapter? = null
    var mAppointmentHistoryList: ArrayList<AppointmentHistoryDataItem?>? = ArrayList<AppointmentHistoryDataItem?>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history)
        ButterKnife.bind(this)
        executeAppointmentHistoryApi()
    }

    @OnClick(
        R.id.backRL
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Execute Appointment History Api
    private fun executeAppointmentHistoryApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.appointmentHistoryRequest(getAccessToken(),"500","1")
        call.enqueue(object : Callback<AppointmentHistoryModel> {
            override fun onResponse(
                call: Call<AppointmentHistoryModel>,
                response: Response<AppointmentHistoryModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        mAppointmentHistoryList = response.body()?.data as ArrayList<AppointmentHistoryDataItem?>?
                        setAdapter(mActivity, mAppointmentHistoryList)
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    404 -> {
                        txtNoDataFoundTV.visibility=View.VISIBLE
                        txtNoDataFoundTV.text=getString(R.string.no_history_found)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<AppointmentHistoryModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    // - - Sets Adapter
    private fun setAdapter(
        activity: Activity?,
        mAppointmentHistoryList: ArrayList<AppointmentHistoryDataItem?>?
    ) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(applicationContext, LinearLayoutManager.VERTICAL, false)
        historyParentRV.layoutManager = layoutManager
        appointmentHistoryAdapter = AppointmentHistoryAdapter(activity,mAppointmentHistoryList)
        historyParentRV.adapter = appointmentHistoryAdapter
    }
}