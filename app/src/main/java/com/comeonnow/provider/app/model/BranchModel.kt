package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BranchModel(

	@field:SerializedName("data")
	val data: ArrayList<BranchDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class BranchDataItem(

	@field:SerializedName("branch_name")
	val branchName: String? = null,

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("clinic_id")
	val clinicId: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable
