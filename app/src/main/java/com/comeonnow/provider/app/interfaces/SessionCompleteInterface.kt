package com.comeonnow.provider.app.interfaces

interface SessionCompleteInterface {
    fun sessionComplete() {}
}