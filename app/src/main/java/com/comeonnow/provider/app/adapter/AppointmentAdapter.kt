package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.model.AptDataItem
import com.comeonnow.provider.app.ui.activity.AppointmentDetailActivity
import com.comeonow.provider.app.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_appointment_detail.*
import java.util.*

class AppointmentAdapter(
    var mActivity: Activity?,
    var mAppointmentList: ArrayList<AptDataItem?>?
) :
    RecyclerView.Adapter<AppointmentAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_appointment, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel = mAppointmentList!![position]

        Picasso.get()
            .load(mModel?.image)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.allListImage)

        if (mModel != null) {
            holder.txtDiseaseTV.text = mModel.disease_name
            holder.allListNameTV.text = mModel.lastName + "," + " "+mModel.firstName
            holder.allListAgeTV.text = mModel.age.toString() + " "+mActivity!!.resources?.getString(R.string.years_old)
            holder.allListDateTV.text = mModel.appointDate.toString()
            holder.allListTimeTV.text =
                mModel.appointStartTime.toString() + " - " + mModel.appointEndTime
            when (mModel.gender) {
                1 -> {
                    holder.allListGenderTV.text = mActivity?.resources?.getString(R.string.male)
                }
                2 -> {
                    holder.allListGenderTV.text = mActivity?.resources?.getString(R.string.female)
                }
                3 -> {
                    holder.allListGenderTV.text = mActivity?.resources?.getString(R.string.others)
                }
            }
        }
        holder.itemView.setOnClickListener {
            val intent = Intent(mActivity, AppointmentDetailActivity::class.java)
            intent.putExtra("mFirstName", mModel?.firstName.toString())
            intent.putExtra("mLastName", mModel?.lastName.toString())
            intent.putExtra("mAge", mModel?.age.toString())
            intent.putExtra("mImage", mModel?.image.toString())
            intent.putExtra("mGender", mModel?.gender.toString())
            intent.putExtra("mDate", mModel?.appointDate.toString())
            intent.putExtra("mRelationship", mModel?.loginuserName.toString())
            intent.putExtra("mStartTime", mModel?.appointStartTime.toString())
            intent.putExtra("mEndTime", mModel?.appointEndTime.toString())
            intent.putExtra("mDescription", mModel?.description.toString())
            intent.putExtra("mAppointmentType", mModel?.appointment_type.toString())
            mActivity?.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mAppointmentList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var allListImage: ImageView = itemView.findViewById(R.id.allListIV)
        var allListNameTV: TextView = itemView.findViewById(R.id.allListNameTV)
        var allListAgeTV: TextView = itemView.findViewById(R.id.allListAgeTV)
        var allListGenderTV: TextView = itemView.findViewById(R.id.allListGenderTV)
        var allListDateTV: TextView = itemView.findViewById(R.id.allListDateTV)
        var allListTimeTV: TextView = itemView.findViewById(R.id.allListTimeTV)
        var txtAppointmentStatusTV: TextView = itemView.findViewById(R.id.txtAppointmentStatusTV)
        var txtDiseaseTV: TextView = itemView.findViewById(R.id.txtDiseaseTV)
    }
}
