package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationModel(

	@field:SerializedName("data")
	val data: List<DataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class DataItem(

	@field:SerializedName("notify_type")
	val notifyType: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("notify_title")
	val notifyTitle: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = 0,

	@field:SerializedName("read_by")
	val readBy: String? = "",

	@field:SerializedName("id")
	val id: Int? = 0,

	@field:SerializedName("creation_date")
	val creationDate: String? = "",

	@field:SerializedName("add_user_id")
	val addUserId: String? = "",

	@field:SerializedName("notify_message")
	val notifyMessage: String? = "",

	@field:SerializedName("push_image")
	val push_image: String? = "",

	@field:SerializedName("status")
	val status: Int? = 0,

	@field:SerializedName("branch_id")
	val branch_id: Int? = 0
) : Parcelable
