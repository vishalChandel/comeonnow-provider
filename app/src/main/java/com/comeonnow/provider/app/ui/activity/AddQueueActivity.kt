package com.comeonnow.provider.app.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.comeonnow.provider.app.model.*
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.GENDER_TYPES
import com.comeonnow.provider.app.util.GENDER_TYPES_IDS
import com.comeonow.provider.app.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_add_queue.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class AddQueueActivity : BaseActivity() {
    var mPatientArrayList: ArrayList<PatientDataItem> = ArrayList()
    var mPatientId = ""
    var mGender: String? = ""
    var mHospitalsArrayList: ArrayList<ProviderClinicData> = ArrayList()
    var mHostpitalId = ""
    var mBranchsArrayList = ArrayList<BranchDataItem>()
    var mBranchId : String = ""
    var mProvidersArrayList = ArrayList<ProviderDataItem>()
    var mProviderId : String = ""
    var mDiseaseArrayList = ArrayList<DiseaseDataItem>()
    var mDiseaseId : String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_queue)
        ButterKnife.bind(this)
        genderSpinner()
        getPatientNameListing()
        getDiseaseData()
    }

    @OnClick(
        R.id.backRL,
        R.id.txtPatientNameTV,
        R.id.txtHospitalNameTV,
        R.id.txtBranchTV,
        R.id.txtProviderTV,
        R.id.txtDiseaseTV,
        R.id.btnSaveTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.txtPatientNameTV -> performPatientNamesClick()
            R.id.txtHospitalNameTV -> performHospitalNamesClick()
            R.id.txtBranchTV -> performBranchClick()
            R.id.txtProviderTV -> performProviderClick()
            R.id.txtDiseaseTV -> performDiseaseClick()
            R.id.btnSaveTV -> performSaveClick()
        }
    }




    /*
    * Patient Names Listing & Popup & Api
    * Executions
    * */
    private fun getPatientNameListing() {
        if (isNetworkAvailable(mActivity)){
            executeGetPatientRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetPatientRequest() {
        patientPB.visibility = View.VISIBLE
        txtPatientNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.hospitalPatientListRequest(getAccessToken())
        mGetProfileData.enqueue(object : Callback<PatientModel> {
            override fun onResponse(call: Call<PatientModel>, response: Response<PatientModel>) {
                patientPB.visibility = View.GONE
                txtPatientNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    val mModel : PatientModel = response.body()!!
                    mPatientArrayList?.addAll(mModel.data!!)
                }else if (response.code() == 401){
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<PatientModel>, t: Throwable) {
                patientPB.visibility = View.GONE
                txtPatientNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performPatientNamesClick() {
        createPatientListingDialog(mActivity)
    }

    private fun createPatientListingDialog(mActivity: Activity) {
        val mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0 until mPatientArrayList.size){
            mDataList.add(mPatientArrayList?.get(i)!!.lastName!! + ", " + mPatientArrayList?.get(i)?.firstName!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_patient_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)

        if(mDataList.size>0){
            txtSaveTV.visibility=View.VISIBLE
            txtNoBranchFoundTV.visibility = View.GONE
        }else{
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility=View.GONE
        }

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mPatientId = ""+ mPatientArrayList!![mWheelPickerWP.currentItemPosition]?.id
            txtPatientNameTV.text = mDataList.get(mWheelPickerWP.currentItemPosition)
            mBottomSheetDialog.dismiss()
        }
    }

    /*************************************************************************************************************/


    /*
    * User Gender Spinner With
    * Id's & Gender Text
    * */
    @SuppressLint("ClickableViewAccessibility")
    private fun genderSpinner() {
        // - - Set Adapter to spinner
        val adapter = ArrayAdapter(this, R.layout.spinner_item_selected, GENDER_TYPES)
        genderSpinner.adapter = adapter
        genderSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                mGender = GENDER_TYPES_IDS[position]
            }
            override fun onNothingSelected(parent: AdapterView<*>) {
            }
        }
    }

    /*************************************************************************************************************/


       /*
       * Hospital Names Listing & Popup & Api
       * Executions
       * */
    private fun getHospitalNameListing() {
        if (isNetworkAvailable(mActivity)){
            executeGetHospitalRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetHospitalRequest() {
        hospitalPB.visibility = View.VISIBLE
        txtHospitalNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.providerClinicRequest(getAccessToken())
        mGetProfileData.enqueue(object : Callback<ProviderClinicModel> {
            override fun onResponse(call: Call<ProviderClinicModel>, response: Response<ProviderClinicModel>) {
                hospitalPB.visibility = View.GONE
                txtHospitalNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    val mModel : ProviderClinicModel = response.body()!!
                    mHospitalsArrayList.clear()
                    if (mModel.data !=null) {
                        mHospitalsArrayList?.addAll(mModel.data!!)
                    }
                    createHospitalListingDialog(mActivity)
                    txtBranchTV.text = ""
                }else if (response.code() == 401){
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<ProviderClinicModel>, t: Throwable) {
                hospitalPB.visibility = View.GONE
                txtHospitalNameTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performHospitalNamesClick() {
        getHospitalNameListing()
    }

    fun createHospitalListingDialog(mActivity: Activity) {
        val mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0 until mHospitalsArrayList.size!!){
            mDataList.add(mHospitalsArrayList?.get(i)?.name!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_hospital_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mHostpitalId = ""+ mHospitalsArrayList!!.get(mWheelPickerWP.currentItemPosition)?.id
            txtHospitalNameTV.setText(mDataList.get(mWheelPickerWP.currentItemPosition))
            mBottomSheetDialog.dismiss()
            getBranchNameListing(mHostpitalId)
        }
    }
    /*************************************************************************************************/


       /*
       * Branch Names Listing & Popup & Api
       * Executions
       * */
    private fun getBranchNameListing(mHostpitalId: String) {
        if (isNetworkAvailable(mActivity)){
            executeGetBranchRequest(mHostpitalId)
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetBranchRequest(mHostpitalId: String) {
        branchPB.visibility = View.VISIBLE
        txtBranchTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.getBranchesRequest(getAccessToken(),mHostpitalId)
        mGetProfileData?.enqueue(object : Callback<BranchModel> {
            override fun onResponse(call: Call<BranchModel>, response: Response<BranchModel>) {
                branchPB.visibility = View.GONE
                txtBranchTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    var mModel : BranchModel = response.body()!!
                    mBranchsArrayList.clear()
                    if (mModel.data?.size!! > 0) {
                        mBranchsArrayList?.addAll(mModel.data!!)
                    }
                }else if (response.code() == 401){
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<BranchModel>, t: Throwable) {
                branchPB.visibility = View.GONE
                txtBranchTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performBranchClick() {
        if (mBranchsArrayList.size > 0){
            createBranchListingDialog(mActivity)
        }else{
            showAlertDialog(mActivity,getString(R.string.no_branches_found))
        }
    }

    fun createBranchListingDialog(mActivity: Activity) {
        var mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0..mBranchsArrayList?.size!! - 1){
            mDataList.add(mBranchsArrayList?.get(i)?.branchName!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_patient_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.setText(getString(R.string.branch))
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mBranchId = ""+ mBranchsArrayList!!.get(mWheelPickerWP.currentItemPosition)?.id
            txtBranchTV.setText(mDataList.get(mWheelPickerWP.currentItemPosition))
            mBottomSheetDialog.dismiss()
            getProviderData(mBranchId)
        }
    }
    /*************************************************************************************************/

    /*
    * Provider Names Listing & Popup & Api
    * Executions
    * */
    private fun getProviderData(mBranchId: String) {
        if (isNetworkAvailable(mActivity)){
            executeGetProviderRequest(mBranchId)
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetProviderRequest(mBranchId: String) {
        providerPB.visibility = View.VISIBLE
        txtProviderTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.getProvidersRequest(getAccessToken(),mHostpitalId,mBranchId)
        mGetProfileData?.enqueue(object : Callback<ProviderModel> {
            override fun onResponse(call: Call<ProviderModel>, response: Response<ProviderModel>) {
                providerPB.visibility = View.GONE
                txtProviderTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    var mModel : ProviderModel = response.body()!!
                    if (mModel.data?.size!! > 0) {
                        mProvidersArrayList?.addAll(mModel.data!!)
                    }
                }else if (response.code() == 401){
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<ProviderModel>, t: Throwable) {
                providerPB.visibility = View.GONE
                txtProviderTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    fun performProviderClick(){
        if (mProvidersArrayList.size > 0){
            createProviderListingDialog(mActivity)
        }else{
            showAlertDialog(mActivity,getString(R.string.no_provider_found))
        }
    }

    fun createProviderListingDialog(mActivity: Activity) {
        var mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0..mProvidersArrayList?.size!! - 1){
            mDataList.add(mProvidersArrayList?.get(i)?.name!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_patient_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mProviderId = ""+ mProvidersArrayList!!.get(mWheelPickerWP.currentItemPosition)?.id
            txtProviderTV.setText(mDataList.get(mWheelPickerWP.currentItemPosition))
            mBottomSheetDialog.dismiss()
        }
    }

    /*************************************************************************************************/

    /*
    * Disease Names Listing & Popup & Api
    * Executions
    * */
    private fun performDiseaseClick() {
        if (mDiseaseArrayList.size > 0){
            createDiseaseListingDialog(mActivity)
        }else{
            showAlertDialog(mActivity,getString(R.string.no_disease_found))
        }
    }
    /*
   * Disease Listing Data
   * */
    private fun getDiseaseData() {
        if (isNetworkAvailable(mActivity)){
            executeGetDiseaseRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }
    private fun executeGetDiseaseRequest() {
        showProgressDialog(mActivity)
        diseasePB.visibility = View.VISIBLE
        txtDiseaseTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.getDiseaseListRequest(getAccessToken())
        mGetProfileData?.enqueue(object : Callback<DiseaseModel> {
            override fun onResponse(call: Call<DiseaseModel>, response: Response<DiseaseModel>) {
                diseasePB.visibility = View.GONE
                dismissProgressDialog()
                txtDiseaseTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    var mModel : DiseaseModel = response.body()!!
                    if (mModel.data?.size!! > 0) {
                        mDiseaseArrayList?.addAll(mModel.data!!)
                    }
                }else if (response.code() == 401){
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    showToast(mActivity,getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<DiseaseModel>, t: Throwable) {
                dismissProgressDialog()
                diseasePB.visibility = View.GONE
                txtDiseaseTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    fun createDiseaseListingDialog(mActivity: Activity) {
        var mDataList : ArrayList<String> = ArrayList<String>()
        for (i in 0..mDiseaseArrayList?.size!! - 1){
            mDataList.add(mDiseaseArrayList?.get(i)?.diseaseName!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_disease, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mDiseaseId = ""+ mDiseaseArrayList!!.get(mWheelPickerWP.currentItemPosition)?.id
            txtDiseaseTV.setText(mDataList.get(mWheelPickerWP.currentItemPosition))
            mBottomSheetDialog.dismiss()
        }
    }



    /*
    * Add Queue Api Executions
    * To Save All Details on Server End
    * */
    private fun isValidate(): Boolean {
        var flag = true
        when {
            txtPatientNameTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_patient))
                flag = false
            }
            txtHospitalNameTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_hospital))
                flag = false
            }
//            txtBranchTV.text.toString().trim { it <= ' ' } == "" -> {
//                showAlertDialog(mActivity, getString(R.string.please_select_your_branch))
//                flag = false
//            }
            txtDiseaseTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_disease))
                flag = false
            }
        }
        return flag
    }

    private fun performSaveClick() {
        if (isValidate()){
            if (isNetworkAvailable(mActivity)){
                executeSubmitQueueRequest()
            }else{
                showToast(mActivity,getString(R.string.internet_connection_error))
            }
        }
    }


    private fun executeSubmitQueueRequest() {
        showProgressDialog(mActivity)
        val headers: MutableMap<String, String> = HashMap()
        headers["Authorization"] = getAccessToken()
        val mGetProfileData = ApiClient.apiInterface.addQueueRequest(headers, mHostpitalId,mBranchId,mDiseaseId,mPatientId)
        Log.i(TAG, "executeSubmitQueueRequest: $headers::$mHostpitalId::$mBranchId::$mDiseaseId::$mPatientId")
        mGetProfileData?.enqueue(object : Callback<AddQueueModel> {
            override fun onResponse(call: Call<AddQueueModel>, response: Response<AddQueueModel>) {
                dismissProgressDialog()
                when {
                    response.code() == 200 -> {
                        txtPatientNameTV.text = ""
                        txtHospitalNameTV.text = ""
                        txtBranchTV.text = ""
                        txtProviderTV.text = ""
                        txtDiseaseTV.text = ""
                        showFinishAlertDialog(mActivity,getString(R.string.queue_added_successfully))
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 405 -> {
                        dismissProgressDialog()
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        val mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity,getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<AddQueueModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }


}