package com.comeonnow.provider.app.ui.activity

import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.comeonow.provider.app.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_appointment_detail.*

class AppointmentDetailActivity : BaseActivity() {

    // - - Initialize Objects
    private var mFirstName: String = ""
    private var mLastName: String = ""
    private var mAge: String = ""
    private var mImage: String = ""
    private var mGender: String = ""
    private var mDate: String = ""
    private var mStartTime: String = ""
    private var mEndTime: String = ""
    private var mRelationship: String = ""
    private var mDescription: String = ""
    private var mAppointmentType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_appointment_detail)
        ButterKnife.bind(this)
        getIntentData()
    }

    @OnClick(
        R.id.backRL,
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }

    // - - Get Intent Data from Another Activity
    private fun getIntentData() {
        if (intent != null) {
            mFirstName = intent.getStringExtra("mFirstName").toString()
            mLastName = intent.getStringExtra("mLastName").toString()
            mAge = intent.getStringExtra("mAge").toString()
            mImage = intent.getStringExtra("mImage").toString()
            mGender = intent.getStringExtra("mGender").toString()
            mDate = intent.getStringExtra("mDate").toString()
            mRelationship = intent.getStringExtra("mRelationship").toString()
            mStartTime = intent.getStringExtra("mStartTime").toString()
            mEndTime = intent.getStringExtra("mEndTime").toString()
            mDescription = intent.getStringExtra("mDescription").toString()
            mAppointmentType = intent.getStringExtra("mAppointmentType").toString()


            setDataOnWidgets()
        }
    }

    // - - Set Data on Widgets
    private fun setDataOnWidgets() {
        txtNameTV.text = "$mLastName, $mFirstName"
        txtAppointmentDateTV.text = mDate
        txtRelationshipTV.text = mRelationship
        txtAppointmentTimeTV.text = "$mStartTime-$mEndTime"
        txtAppointmentTypeTV.text = mAppointmentType
        txtDescriptionTV.text = mDescription


        txtAgeTV.text= "$mAge years old"
        when (mGender) {
            "1" -> {
                txtGenderTV.text = "Male"
            }
            "2" -> {
                txtGenderTV.text = "Female"
            }
            "3" -> {
                txtGenderTV.text = "Others"
            }
        }

        Picasso.get()
            .load(mImage)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(imgPatientIV)
    }
}