package com.comeonnow.provider.app.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FlagModel (
    var countryName: String,
    var countryCode: String,
    var countryPhoneCode: String,
    var countryImage: Int,
    var countryImageName: String
) : Parcelable