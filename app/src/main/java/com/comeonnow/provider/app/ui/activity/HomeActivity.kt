package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.ui.fragment.*
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(this)
        setUpHomeScreen()
    }

    private fun setUpHomeScreen() {
        if (intent != null && intent.getStringExtra(NOTI_TYPE) != null){
            if (intent.getStringExtra(NOTI_TYPE).equals("add_queue")) {
                performQueueClick()
            }else if (intent.getStringExtra(NOTI_TYPE).equals("appointment_confirm")){
                performAppointmentClick()
            }else if (intent.getStringExtra(NOTI_TYPE).equals("request_accept")){
                performNotificationClick()
            }
            else if (intent.getStringExtra(NOTI_TYPE).equals("send_document")) {
                tabSelection(3)
                val i = Intent(applicationContext, DocumentActivity::class.java)
                startActivity(i)
                finish()
            }
            else{
                setUpFirstFragment()
            }
        }else{
            setUpFirstFragment()
        }
    }

    @OnClick(
        R.id.actionAppointment,
        R.id.actionQueue,
        R.id.actionNotification,
        R.id.actionProfile

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.actionAppointment -> performAppointmentClick()
            R.id.actionQueue -> performQueueClick()
            R.id.actionNotification -> performNotificationClick()
            R.id.actionProfile -> performProfileClick()
        }
    }

    // - - SetUp HomeFragment on HomeScreen
    private fun setUpFirstFragment() {
        performAppointmentClick()
    }

    // - - Opens AppointmentFragment
    private fun performAppointmentClick() {
        tabSelection(0)
        switchFragment(AppointmentFragment(), APPOINTMENT_TAG, false, null)
    }

    // - - Opens QueueFragment
    private fun performQueueClick() {
        tabSelection(1)
        switchFragment(QueueFragment(), QUEUE_TAG, false, null)
    }

    // - - Opens NotificationFragment
    private fun performNotificationClick() {
        tabSelection(2)
        switchFragment(NotificationFragment(), NOTIFICATION_TAG, false, null)
    }

    // - - Opens ProfileFragment
    private fun performProfileClick() {
        tabSelection(3)
        switchFragment(fragment = ProfileFragment(), PROFILE_TAG, false, null)
    }

    // In your activity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }

    // - - To Change the color of Icon and Text When Tab is selected
    private fun tabSelection(mPos: Int) {
        imgAppointmentIV.setImageResource(R.drawable.ic_appointment)
        imgQueueIV.setImageResource(R.drawable.ic_queue)
        imgNotificationIV.setImageResource(R.drawable.ic_notification)
        imgProfileIV.setImageResource(R.drawable.ic_profile)
        txtAppointmentTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack))
        txtQueueTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack))
        txtNotificationTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack))
        txtProfileTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlack))
        when (mPos) {
            0 -> {
                imgAppointmentIV.setImageResource(R.drawable.ic_appointment_sel)
                txtAppointmentTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            1 -> {
                imgQueueIV.setImageResource(R.drawable.ic_queue_sel)
                txtQueueTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            2 -> {
                imgNotificationIV.setImageResource(R.drawable.ic_notification_sel)
                txtNotificationTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
            3 -> {
                imgProfileIV.setImageResource(R.drawable.ic_profile_sel)
                txtProfileTV.setTextColor(ContextCompat.getColor(mActivity, R.color.colorBlue))
            }
        }
    }
}