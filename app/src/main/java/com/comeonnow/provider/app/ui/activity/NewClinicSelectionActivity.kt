package com.comeonnow.provider.app.ui.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.os.SystemClock
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.core.view.isVisible
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.comeonnow.provider.app.model.*
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.DEVICE_TYPE
import com.comeonnow.provider.app.util.user_type
import com.comeonow.provider.app.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_new_clinic_selection.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException

class NewClinicSelectionActivity : BaseActivity() {
    var mStateArrayList = ArrayList<StateDataItem>()
    var mClinicArrayList = ArrayList<ClinicDataItem>()
    var mBranchArrayList = ArrayList<BranchDataItem>()
    private var mNoOfClinics = 1
    private var mLastClickTime: Long = 0
    private var mBranchId1 = ""
    private var mBranchId2 = ""
    private var mBranchId3 = ""
    private var mClinicId1 = ""
    private var mClinicId2 = ""
    private var mClinicId3 = ""
    private var mStateCode1 = ""
    private var mStateCode2 = ""
    private var mStateCode3 = ""
    private var mEmail = ""
    private var mFirstName = ""
    private var mLastName = ""
    private var mPassword = ""
    private var mDegree = ""
    private var mSpecialization = ""
    private var mClinicIds = ""
    private var mCountryName = ""
    private var mPhoneNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_clinic_selection)
        ButterKnife.bind(this)
        getIntentData()
    }

    private fun getIntentData() {
        if (intent != null) {
            mEmail = intent.getStringExtra("email").toString()
            mFirstName = intent.getStringExtra("firstName").toString()
            mLastName = intent.getStringExtra("lastName").toString()
            mPassword = intent.getStringExtra("password").toString()
            mDegree = intent.getStringExtra("degree").toString()
            mSpecialization = intent.getStringExtra("specialization").toString()
            mCountryName = intent.getStringExtra("countryName").toString()
            mPhoneNumber = intent.getStringExtra("phoneNumber").toString()
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.addClinicLL,
        R.id.removeTV2,
        R.id.removeTV3,
        R.id.clinicTV1,
        R.id.clinicTV2,
        R.id.clinicTV3,
        R.id.stateTV1,
        R.id.stateTV2,
        R.id.stateTV3,
        R.id.branchTV1,
        R.id.branchTV2,
        R.id.branchTV3,
        R.id.saveTV
    )

    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> performBackClick()
            R.id.addClinicLL -> performAddNewClinicClick()
            R.id.removeTV2 -> performRemoveClick2()
            R.id.removeTV3 -> performRemoveClick3()
            R.id.clinicTV1 -> performClinicClick1()
            R.id.clinicTV2 -> performClinicClick2()
            R.id.clinicTV3 -> performClinicClick3()
            R.id.stateTV1 -> performStateClick1()
            R.id.stateTV2 -> performStateClick2()
            R.id.stateTV3 -> performStateClick3()
            R.id.branchTV1 -> performBranchClick1()
            R.id.branchTV2 -> performBranchClick2()
            R.id.branchTV3 -> performBranchClick3()
            R.id.saveTV -> performSaveClick()
        }
    }

    private fun performBackClick() {
        val i =Intent(mActivity,SignUpActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun performBranchClick3() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mClinicId3.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeBranchRequest3()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_clinic_first))
        }
    }

    private fun executeBranchRequest3() {
        branchSKV3.visibility = View.VISIBLE
        branchTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetBranchList = ApiClient.apiInterface.branchListRequest(mClinicId3)
        mGetBranchList!!.enqueue(object : Callback<BranchModel> {
            override fun onResponse(
                call: Call<BranchModel>,
                response: Response<BranchModel>
            ) {
                branchSKV3.visibility = View.GONE
                branchTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: BranchModel = response.body()!!
                        if (mBranchArrayList.isNotEmpty()) {
                            mBranchArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mBranchArrayList.addAll(mModel.data)
                        }
                        createBranchListingDialog(mActivity, branchTV3, "branchTV3")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createBranchListingDialog(mActivity, branchTV3, "branchTV3")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<BranchModel>, t: Throwable) {
                branchSKV3.visibility = View.GONE
                branchTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performBranchClick2() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mClinicId2.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeBranchRequest2()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_clinic_first))
        }
    }

    private fun executeBranchRequest2() {
        branchSKV2.visibility = View.VISIBLE
        branchTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetBranchList = ApiClient.apiInterface.branchListRequest(mClinicId2)
        mGetBranchList!!.enqueue(object : Callback<BranchModel> {
            override fun onResponse(
                call: Call<BranchModel>,
                response: Response<BranchModel>
            ) {
                branchSKV2.visibility = View.GONE
                branchTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: BranchModel = response.body()!!
                        if (mBranchArrayList.isNotEmpty()) {
                            mBranchArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mBranchArrayList.addAll(mModel.data)
                        }
                        createBranchListingDialog(mActivity, branchTV2, "branchTV2")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createBranchListingDialog(mActivity, branchTV2, "branchTV2")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<BranchModel>, t: Throwable) {
                branchSKV2.visibility = View.GONE
                branchTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performBranchClick1() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mClinicId1.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeBranchRequest1()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_clinic_first))
        }
    }

    private fun executeBranchRequest1() {
        branchSKV1.visibility = View.VISIBLE
        branchTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetBranchList = ApiClient.apiInterface.branchListRequest(mClinicId1)
        mGetBranchList!!.enqueue(object : Callback<BranchModel> {
            override fun onResponse(
                call: Call<BranchModel>,
                response: Response<BranchModel>
            ) {
                branchSKV1.visibility = View.GONE
                branchTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: BranchModel = response.body()!!
                        if (mBranchArrayList.isNotEmpty()) {
                            mBranchArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mBranchArrayList.addAll(mModel.data)
                        }
                        createBranchListingDialog(mActivity, branchTV1, "branchTV1")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createBranchListingDialog(mActivity, branchTV1, "branchTV1")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<BranchModel>, t: Throwable) {
                branchSKV1.visibility = View.GONE
                branchTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performSaveClick() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        when {
            mBranchId1.isNotEmpty() && mBranchId2.isNotEmpty() && mBranchId3.isNotEmpty() -> {
                mClinicIds = "$mBranchId1,$mBranchId2,$mBranchId3"
            }
            mClinicId1.isNotEmpty() && mBranchId2.isNotEmpty() && mBranchId3.isEmpty() -> {
                mClinicIds = "$mBranchId1,$mBranchId2"
            }
            mClinicId1.isNotEmpty() && mBranchId2.isEmpty() && mBranchId3.isEmpty() -> {
                mClinicIds = mBranchId1
            }
        }
        if (mClinicIds.isNotEmpty()) {
            when {
                mBranchId2.isEmpty() && clinicLL2.isVisible -> {
                    showAlertDialog(mActivity, getString(R.string.please_fill_empty_branch))
                }
                mBranchId3.isEmpty() && clinicLL3.isVisible -> {
                    showAlertDialog(mActivity, getString(R.string.please_fill_empty_branch))
                }
                mBranchId2.isEmpty() && clinicLL2.isVisible && mBranchId3.isEmpty() && clinicLL3.isVisible -> {
                    showAlertDialog(mActivity, getString(R.string.please_fill_empty_branch))
                }
                else -> {
                    if (isNetworkAvailable(mActivity)) {
                        executeSignUpRequest()
                    } else {
                        showToast(mActivity, getString(R.string.internet_connection_error))
                    }
                }
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_atleast_one_clinic_details))
        }
    }

    private fun performStateClick3() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (isNetworkAvailable(mActivity)) {
            executeGetStatesRequest3()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }

    private fun performStateClick2() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (isNetworkAvailable(mActivity)) {
            executeGetStatesRequest2()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }


    }

    private fun performStateClick1() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (isNetworkAvailable(mActivity)) {
            executeGetStatesRequest1()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }

    private fun performClinicClick3() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()

        if (mStateCode3.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeGetClinicsRequest3()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_state_first))
        }
    }

    private fun performClinicClick2() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mStateCode2.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeGetClinicsRequest2()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_state_first))
        }
    }

    private fun performClinicClick1() {
        if (SystemClock.elapsedRealtime() - mLastClickTime < 1500) {
            return
        }
        mLastClickTime = SystemClock.elapsedRealtime()
        if (mStateCode1.isNotEmpty()) {
            if (isNetworkAvailable(mActivity)) {
                executeGetClinicsRequest1()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        } else {
            showAlertDialog(mActivity, getString(R.string.please_select_state_first))
        }
    }

    private fun executeGetStatesRequest1() {
        stateSKV1.visibility = View.VISIBLE
        stateTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetStateList = ApiClient.apiInterface.stateListRequest()
        mGetStateList!!.enqueue(object : Callback<StateListModel> {
            override fun onResponse(
                call: Call<StateListModel>,
                response: Response<StateListModel>
            ) {
                stateSKV1.visibility = View.GONE
                stateTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: StateListModel = response.body()!!
                        if (mStateArrayList.isNotEmpty()) {
                            mStateArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mStateArrayList.addAll(mModel.data)
                        }
                        createStateListingDialog(mActivity, stateTV1, "stateTV1")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<StateListModel>, t: Throwable) {
                Log.i(TAG, "onFailure: ")
                stateSKV1.visibility = View.GONE
                stateTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun executeGetStatesRequest2() {
        stateSKV2.visibility = View.VISIBLE
        stateTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetStateList = ApiClient.apiInterface.stateListRequest()
        mGetStateList!!.enqueue(object : Callback<StateListModel> {
            override fun onResponse(
                call: Call<StateListModel>,
                response: Response<StateListModel>
            ) {
                stateSKV2.visibility = View.GONE
                stateTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: StateListModel = response.body()!!
                        if (mStateArrayList.isNotEmpty()) {
                            mStateArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mStateArrayList.addAll(mModel.data)
                        }
                        createStateListingDialog(mActivity, stateTV2, "stateTV2")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<StateListModel>, t: Throwable) {
                Log.i(TAG, "onFailure: ")
                stateSKV2.visibility = View.GONE
                stateTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun executeGetStatesRequest3() {
        stateSKV3.visibility = View.VISIBLE
        stateTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetStateList = ApiClient.apiInterface.stateListRequest()
        mGetStateList!!.enqueue(object : Callback<StateListModel> {
            override fun onResponse(
                call: Call<StateListModel>,
                response: Response<StateListModel>
            ) {
                stateSKV3.visibility = View.GONE
                stateTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: StateListModel = response.body()!!
                        if (mStateArrayList.isNotEmpty()) {
                            mStateArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mStateArrayList.addAll(mModel.data)
                        }
                        createStateListingDialog(mActivity, stateTV3, "stateTV3")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<StateListModel>, t: Throwable) {
                Log.i(TAG, "onFailure: ")
                stateSKV3.visibility = View.GONE
                stateTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performAddNewClinicClick() {
        when (mNoOfClinics) {
            1 -> {
                removeTV2.visibility = View.VISIBLE
                removeTV3.visibility = View.GONE
                clinicLL1.visibility = View.VISIBLE
                clinicLL2.visibility = View.VISIBLE
                clinicLL3.visibility = View.GONE
                mNoOfClinics = 2
            }
            2 -> {
                clinicLL1.visibility = View.VISIBLE
                clinicLL2.visibility = View.VISIBLE
                clinicLL3.visibility = View.VISIBLE
                removeTV2.visibility = View.VISIBLE
                removeTV3.visibility = View.VISIBLE
                mNoOfClinics = 3
            }
            3 -> {
                showToast(
                    mActivity,
                    "You have added max number of clinics. To add more clinics go to profile section."
                )
            }
        }
    }

    private fun performRemoveClick2() {
        removeTV2.visibility = View.GONE
        clinicLL2.visibility = View.GONE
        mNoOfClinics -= 1
        stateTV2.text = ""
        clinicTV2.text = ""
        mStateCode2 = ""
        mClinicId2 = ""
    }

    private fun performRemoveClick3() {
        removeTV3.visibility = View.GONE
        clinicLL3.visibility = View.GONE
        mNoOfClinics -= 1
        stateTV3.text = ""
        clinicTV3.text = ""
        mStateCode3 = ""
        mClinicId3 = ""
    }

    private fun executeGetClinicsRequest1() {
        clinicSKV1.visibility = View.VISIBLE
        clinicTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetClinicList = ApiClient.apiInterface.clinicListRequest(mStateCode1)
        mGetClinicList.enqueue(object : Callback<ClinicListModel> {
            override fun onResponse(
                call: Call<ClinicListModel>,
                response: Response<ClinicListModel>
            ) {
                clinicSKV1.visibility = View.GONE
                clinicTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: ClinicListModel = response.body()!!
                        if (mClinicArrayList.isNotEmpty()) {
                            mClinicArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mClinicArrayList.addAll(mModel.data)
                        }
                        createClinicListingDialog(mActivity, clinicTV1, "clinicTV1")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createClinicListingDialog(mActivity, clinicTV1, "clinicTV1")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<ClinicListModel>, t: Throwable) {
                clinicSKV1.visibility = View.GONE
                clinicTV1.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun executeGetClinicsRequest2() {
        clinicSKV2.visibility = View.VISIBLE
        clinicTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetClinicList = ApiClient.apiInterface.clinicListRequest(mStateCode2)
        mGetClinicList.enqueue(object : Callback<ClinicListModel> {
            override fun onResponse(
                call: Call<ClinicListModel>,
                response: Response<ClinicListModel>
            ) {
                clinicSKV2.visibility = View.GONE
                clinicTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: ClinicListModel = response.body()!!
                        if (mClinicArrayList.isNotEmpty()) {
                            mClinicArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mClinicArrayList.addAll(mModel.data)
                        }
                        createClinicListingDialog(mActivity, clinicTV2, "clinicTV2")
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createClinicListingDialog(mActivity, clinicTV2, "clinicTV2")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<ClinicListModel>, t: Throwable) {
                clinicSKV2.visibility = View.GONE
                clinicTV2.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun executeGetClinicsRequest3() {
        clinicSKV3.visibility = View.VISIBLE
        clinicTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetClinicList = ApiClient.apiInterface.clinicListRequest(mStateCode3)
        mGetClinicList.enqueue(object : Callback<ClinicListModel> {
            override fun onResponse(
                call: Call<ClinicListModel>,
                response: Response<ClinicListModel>
            ) {
                clinicSKV3.visibility = View.GONE
                clinicTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                when {
                    response.code() == 200 -> {
                        val mModel: ClinicListModel = response.body()!!
                        if (mClinicArrayList.isNotEmpty()) {
                            mClinicArrayList.clear()
                        }
                        if (mModel.data.size > 0) {
                            mClinicArrayList.addAll(mModel.data)
                        }
                        createClinicListingDialog(mActivity, clinicTV3, "clinicTV3")
                    }

                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 404 -> {
                        createClinicListingDialog(mActivity, clinicTV3, "clinicTV3")
                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<ClinicListModel>, t: Throwable) {
                clinicSKV3.visibility = View.GONE
                clinicTV3.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun createClinicListingDialog(mActivity: Activity, mTextView: TextView, s: String) {
        val mDataList: ArrayList<String> = ArrayList()

        if (mClinicArrayList.size > 0) {
            for (i in 0 until mClinicArrayList.size) {
                mDataList.add(mClinicArrayList[i].clinicName!!)
            }
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_clinic_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.text = getString(R.string.clinic)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)
        txtNoBranchFoundTV.text = getString(R.string.no_clinics_found)
        if (mDataList.size > 0) {
            txtNoBranchFoundTV.visibility = View.GONE
            txtSaveTV.visibility = View.VISIBLE
        } else {
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility = View.GONE
        }
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener {
            mBottomSheetDialog.dismiss()
            if (mClinicArrayList.isNotEmpty()) {
                mClinicArrayList.clear()
            }
        }
        txtSaveTV.setOnClickListener {
            when (s) {
                "clinicTV1" -> {
                    mClinicId1 = "" + mClinicArrayList[mWheelPickerWP.currentItemPosition].clinicId
                }
                "clinicTV2" -> {
                    mClinicId2 = "" + mClinicArrayList[mWheelPickerWP.currentItemPosition].clinicId
                }
                "clinicTV3" -> {
                    mClinicId3 = "" + mClinicArrayList[mWheelPickerWP.currentItemPosition].clinicId
                }
            }
            when {
                mClinicId2.isNotEmpty() && mClinicId3.isNotEmpty() -> {
                    if (mClinicId1 == mClinicId2 || mClinicId1 == mClinicId3 || mClinicId2 == mClinicId3) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this clinic. Please select other clinic."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                mClinicId2.isNotEmpty() && mClinicId3.isEmpty() -> {
                    if (mClinicId1 == mClinicId2) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this clinic. Please select other clinic."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                mClinicId2.isEmpty() && mClinicId3.isNotEmpty() -> {
                    if (mClinicId1 == mClinicId3) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this clinic. Please select other clinic."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                else -> {
                    mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                    mBottomSheetDialog.dismiss()
                }
            }

        }
    }

    private fun createBranchListingDialog(mActivity: Activity, mTextView: TextView, s: String) {
        val mDataList: ArrayList<String> = ArrayList()

        if (mBranchArrayList.size > 0) {
            for (i in 0 until mBranchArrayList.size) {
                mDataList.add(mBranchArrayList[i].branchName!!)
            }
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_branch_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.text = getString(R.string.branch)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)
        txtNoBranchFoundTV.text = getString(R.string.no_branches_found)
        if (mDataList.size > 0) {
            txtNoBranchFoundTV.visibility = View.GONE
            txtSaveTV.visibility = View.VISIBLE
        } else {
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility = View.GONE
        }
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener {
            mBottomSheetDialog.dismiss()
            if (mBranchArrayList.isNotEmpty()) {
                mBranchArrayList.clear()
            }
        }
        txtSaveTV.setOnClickListener {
            when (s) {
                "branchTV1" -> {
                    mBranchId1 = "" + mBranchArrayList[mWheelPickerWP.currentItemPosition].id
                }
                "branchTV2" -> {
                    mBranchId2 = "" + mBranchArrayList[mWheelPickerWP.currentItemPosition].id
                }
                "branchTV3" -> {
                    mBranchId3 = "" + mBranchArrayList[mWheelPickerWP.currentItemPosition].id
                }
            }
            when {
                mBranchId2.isNotEmpty() && mBranchId3.isNotEmpty() -> {
                    if (mBranchId1 == mBranchId2 || mBranchId1 == mBranchId3 || mBranchId2 == mBranchId3) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this branch. Please select other branch."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                mBranchId2.isNotEmpty() && mBranchId3.isEmpty() -> {
                    if (mBranchId1 == mBranchId2) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this branch. Please select other branch."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                mBranchId2.isEmpty() && mBranchId3.isNotEmpty() -> {
                    if (mBranchId1 == mBranchId3) {
                        showAlertDialog(
                            mActivity,
                            "You have already selected this branch. Please select other branch."
                        )
                    } else {
                        mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                        mBottomSheetDialog.dismiss()
                    }
                }
                else -> {
                    mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
                    mBottomSheetDialog.dismiss()
                }
            }

        }
    }


    private fun createStateListingDialog(mActivity: Activity, mTextView: TextView, s: String) {
        val mDataList: ArrayList<String> = ArrayList()

        if (mStateArrayList.size > 0) {
            for (i in 0 until mStateArrayList.size) {
                mDataList.add(mStateArrayList[i].stateName!!)
            }
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_clinic_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        val txtHeaderTV = sheetView.findViewById<TextView>(R.id.txtHeaderTV)
        txtHeaderTV.text = getString(R.string.search_state)
        val txtNoBranchFoundTV = sheetView.findViewById<TextView>(R.id.txtNoBranchFoundTV)
        txtNoBranchFoundTV.text = getString(R.string.no_clinics_found)
        if (mDataList.size > 0) {
            txtNoBranchFoundTV.visibility = View.GONE
            txtSaveTV.visibility = View.VISIBLE
        } else {
            txtNoBranchFoundTV.visibility = View.VISIBLE
            txtSaveTV.visibility = View.GONE
        }
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener {
            mBottomSheetDialog.dismiss()
        }
        txtSaveTV.setOnClickListener {
            when (s) {
                "stateTV1" -> {
                    mStateCode1 = "" + mStateArrayList[mWheelPickerWP.currentItemPosition].stateCode
                }
                "stateTV2" -> {
                    mStateCode2 = "" + mStateArrayList[mWheelPickerWP.currentItemPosition].stateCode
                }
                "stateTV3" -> {
                    mStateCode3 = "" + mStateArrayList[mWheelPickerWP.currentItemPosition].stateCode
                }
            }
            mTextView.text = mDataList[mWheelPickerWP.currentItemPosition]
            mBottomSheetDialog.dismiss()
        }
    }

    private fun executeSignUpRequest() {
        showProgressDialog(mActivity)
        val mSignUpRequest = ApiClient.apiInterface.signUpRequest(
            mEmail,
            mFirstName,
            mLastName,
            mPassword,
            mSpecialization,
            mDegree,
            user_type,
            DEVICE_TYPE,
            mClinicIds,
            mCountryName,
            mPhoneNumber,
        )

        mSignUpRequest?.enqueue(object : Callback<SignUpModel> {
            override fun onResponse(call: Call<SignUpModel>, response: Response<SignUpModel>) {
                dismissProgressDialog()
                when {
                    response.code() == 200 -> {
                        showToast(mActivity, response.body()?.message)
                        startActivity(Intent(mActivity, SignInActivity::class.java))
                        finish()
                    }
                    response.code() == 409 -> {
                        val gson = GsonBuilder().create()
                        val mError: SignUpErrorModel
                        try {
                            mError = gson.fromJson(
                                response.errorBody()!!.string(),
                                SignUpErrorModel::class.java
                            )
                            showAlertDialog(mActivity, mError.message)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                    response.code() == 500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<SignUpModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
}