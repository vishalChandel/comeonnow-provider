package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.interfaces.ClinicRequestAcceptRejectInterface
import com.comeonnow.provider.app.model.DataItem
import com.comeonnow.provider.app.ui.activity.WebViewActivity
import com.comeonnow.provider.app.util.LINK_TYPE
import com.comeonow.provider.app.R
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NotificationAdapter(
    var mActivity: Activity?,
    var mNotificationList: ArrayList<DataItem?>?,
    var langStatus: String,
    var clinicRequestAcceptRejectInterface: ClinicRequestAcceptRejectInterface
) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_notification_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel = mNotificationList!![position]
        mActivity?.let {
            Glide.with(it).load(mModel?.image)
                .placeholder(R.drawable.ic_provider)
                .error(R.drawable.ic_provider)
                .into(holder.notificationListIV)
        }
        holder.notificationListTV.text = mModel?.notifyTitle
        holder.txtDescriptionTV.text = mModel?.notifyMessage
        holder.txtTimeTV.text = getDateTime(mModel?.creationDate!!)

        if (mModel.notifyType == "request_accept") {
            if (mModel.status == 1) {
                holder.acceptRejectLL.visibility = View.VISIBLE
            } else {
                holder.acceptRejectLL.visibility = View.GONE
            }
        } else {
            holder.acceptRejectLL.visibility = View.GONE
        }

        holder.acceptTV.setOnClickListener {
            clinicRequestAcceptRejectInterface.clinicRequestAcceptReject(
                "1",
                mModel.branch_id.toString(),
                mModel.id.toString()
            )
        }
        holder.rejectTV.setOnClickListener {
            clinicRequestAcceptRejectInterface.clinicRequestAcceptReject(
                "2",
                mModel.branch_id.toString(),
                mModel.id.toString()
            )

        }
        holder.itemView.setOnClickListener {
            if (mModel.notifyType == "mass_push" && mModel?.push_image != null && mModel?.push_image!!.contains(
                    "http"
                )
            ) {
                val mIntent = Intent(mActivity, WebViewActivity::class.java)
                mIntent.putExtra(LINK_TYPE, mModel.push_image)
                mActivity!!.startActivity(mIntent)
            }
        }

    }

    override fun getItemCount(): Int {
        return mNotificationList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var notificationListIV: ImageView = itemView.findViewById(R.id.notificationListIV)
        var notificationListTV: TextView = itemView.findViewById(R.id.notificationListTV)
        var txtDescriptionTV: TextView = itemView.findViewById(R.id.txtDescriptionTV)
        var txtTimeTV: TextView = itemView.findViewById(R.id.txtTimeTV)
        var acceptRejectLL: LinearLayout = itemView.findViewById(R.id.acceptRejectLL)
        var acceptTV: TextView = itemView.findViewById(R.id.acceptTV)
        var rejectTV: TextView = itemView.findViewById(R.id.rejectTV)
    }


    private fun getDateTime(s: String): String? {
        return try {
            val locale: Locale? = if (langStatus == "es") {
                Locale("es", "es")
            } else {
                Locale.ENGLISH
            }
            val sdf = SimpleDateFormat("MMMM dd, yyyy 'at' hh:mm a", locale)
            val netDate = Date(s.toLong() * 1000)
            sdf.format(netDate)
        } catch (e: Exception) {
            e.toString()
        }
    }
}
