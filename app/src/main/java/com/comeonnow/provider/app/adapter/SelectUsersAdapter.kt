package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.interfaces.UserSelectClickListner
import com.comeonnow.provider.app.model.UserDataItem
import com.comeonow.provider.app.R
import de.hdodenhof.circleimageview.CircleImageView


class SelectUsersAdapter(var mActivity: Activity?, var mArrayList: ArrayList<UserDataItem>, var mUserSelectClickListner : UserSelectClickListner) :
    RecyclerView.Adapter<SelectUsersAdapter.MyViewHolder>() {


    var mSelectedArrayList: ArrayList<UserDataItem> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_select_users, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        var mModel = mArrayList!!.get(position)

        holder.txtUserNameTV.text = mModel!!.lastName + ", " + mModel!!.firstName

        mActivity?.let {
            Glide.with(it).load(mModel!!.image)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(holder.imgUserImageIV)
        }

        holder.selectCB.isChecked = mModel.isSelected!!

        holder.selectCB.setOnClickListener {
            mModel.isSelected = !mModel.isSelected!!
            if (mModel.isSelected!!){
                mSelectedArrayList.add(mModel)
            }else{
                mSelectedArrayList.remove(mModel)
            }
            mUserSelectClickListner.onItemClickListner(mSelectedArrayList)

        }


    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var imgUserImageIV: CircleImageView
        var txtUserNameTV: TextView
        var selectCB: CheckBox

        init {
            imgUserImageIV = itemView.findViewById(R.id.imgUserImageIV)
            txtUserNameTV = itemView.findViewById(R.id.txtUserNameTV)
            selectCB = itemView.findViewById(R.id.selectCB)
        }
    }

    //This method will filter the list
    //here we are passing the filtered data
    //and assigning it to the list with notifydatasetchanged method
    fun filterList(filterdList: ArrayList<UserDataItem>) {
        this.mArrayList = filterdList
        notifyDataSetChanged()
    }

}
