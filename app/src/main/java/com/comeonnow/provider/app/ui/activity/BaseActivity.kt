package com.comeonnow.provider.app.ui.activity

import android.annotation.SuppressLint
import android.app.*
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.comeonnow.provider.app.model.FlagModel
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.json.JSONArray
import org.json.JSONException
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*
import java.util.regex.Pattern
import android.widget.Toast

import android.widget.TimePicker




open class BaseActivity : AppCompatActivity() {

    // - - Get Class Name
    var TAG = this@BaseActivity.javaClass.simpleName

    // - - Initialize Activity
    var mActivity: Activity = this@BaseActivity

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    //  Finishes Activity
    override fun finish() {
        super.finish()
        overridePendingTransitionSlideDownExit()
    }

    //  Start Activity
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransitionSlideUPEnter()
    }

    //  Overrides the pending Activity transition by performing the "Bottom Up" animation.
    private fun overridePendingTransitionSlideUPEnter() {
        overridePendingTransition(R.anim.bottom_up, 0)
    }

    //  Overrides the pending Activity transition by performing the "Bottom Down" animation.
    private fun overridePendingTransitionSlideDownExit() {
        overridePendingTransition(0, R.anim.bottom_down)
    }




    fun isEnglish(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_ENGLISH, true)
    }

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_LOGIN, false)
    }

    // - - To Get Access Token
    fun getAccessToken(): String {
        return AppPreference().readString(mActivity, ACCESS_TOKEN, "")!!
    }

    // - - To Get Access Token
    fun getUserId(): String {
        return AppPreference().readString(mActivity, USER_ID, "")!!
    }

    // - - GET BASE URL
    fun getBaseUrl(): String {
        return AppPreference().readString(mActivity, BASE_URL_PREF, "")!!
    }

    // - - To Check Whether User has opted remember me or not
    fun isRememberMe(): Boolean {
        return RememberMeAppPreference().readBoolean(mActivity, IS_REMEMBER_ME, false)
    }

    // - - To Get Remember Me Email
    fun rememberMeEmail(): String {
        return RememberMeAppPreference().readString(mActivity, RM_EMAIL, "")!!
    }

    // - - To Get Remember Me Password
    fun rememberMePassword(): String {
        return RememberMeAppPreference().readString(mActivity, RM_PASSWORD, "")!!
    }

    // - - To Get First Name
    fun firstName(): String {
        return AppPreference().readString(mActivity, FIRST_NAME, "")!!
    }

    // - - To Get Last Name
    fun lastName(): String {
        return AppPreference().readString(mActivity, LAST_NAME, "")!!
    }

    // - - To Get User Email
    fun userEmail(): String {
        return AppPreference().readString(mActivity, EMAIL, "")!!
    }

    // - - To Get Phone Number
    fun cellNo(): String {
        return AppPreference().readString(mActivity, CELL_NO, "")!!
    }

    // - - To Get Country Code
    fun countryCode(): String {
        return AppPreference().readString(mActivity, COUNTRY_CODE, "+91")!!
    }

    // - - To Get Profile Picture
    fun profilePicture(): String {
        return AppPreference().readString(mActivity, PROFILE_IMAGE, "")!!
    }

    // - - To Check Internet Connection
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager = mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Validate Email
    fun isValidEmailId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }



    // - - To Clear Focus From EditText
    fun setEditTextFocused(mEditText: EditText) {
        mEditText.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                mEditText.clearFocus()
                val imm = v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(v.windowToken, 0)
                return@OnEditorActionListener true
            }
            false
        })
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    // - - Switch Between Fragments
    fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction =
                fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.container, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()
        }
    }

    // - - To Show or Hide Password
    open fun setPasswordHideShow(editText: EditText, imageView: ImageView) {
        if (editText.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
            imageView.setImageResource(R.drawable.ic_show_pwd)
            //Show Password
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        } else {
            imageView.setImageResource(R.drawable.ic_hide_pwd);
            //Hide Password
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        }
    }

    // - - Authentication Failed Dialog
    fun showAuthFailedDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_auth_failed)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences =
                mActivity?.let { AppPreference().getPreferences(it) }!!
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, SignInActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }

    // - - Convert Bitmap to Byte Array
    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }

    // - - To Get Alpha Numeric String
    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {

            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }

    // - -  Get All Countries Data
    open fun getAllCountriesData(): ArrayList<FlagModel> {
        val mCountriesArrayList: ArrayList<FlagModel> = ArrayList()
        try {
            val countryValue = readJSONFromAsset()
            val jsonArray = JSONArray(countryValue)
            for (idx in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(idx)
                val firstValue = jsonObject.getString("nameImage").replace(" ", "_").toLowerCase()
                val secondValue = firstValue.replace("-", "_").toLowerCase()
                val resources: Resources = resources
                val resourceId: Int = resources.getIdentifier(secondValue, "drawable", packageName)
                val mModel: FlagModel = FlagModel(
                    jsonObject.getString("name"),
                    jsonObject.getString("code"),
                    jsonObject.getString("dial_code"),
                    resourceId,
                    jsonObject.getString("nameImage")
                )
                mCountriesArrayList!!.add(mModel)
            }
            Log.e(TAG, "CountryData: " + mCountriesArrayList!!.size)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return mCountriesArrayList;
    }

    // - - Get Country Dial Code
    open fun getCountryDialCode(mCountryName: String): String {
        var mCountryCode: String = ""
        val mCountriesArrayList: ArrayList<FlagModel> = ArrayList()
        try {
            val countryValue = readJSONFromAsset()
            val jsonArray = JSONArray(countryValue)
            for (idx in 0 until jsonArray.length()) {
                val jsonObject = jsonArray.getJSONObject(idx)
                val firstValue = jsonObject.getString("nameImage").replace(" ", "_").toLowerCase()
                val secondValue = firstValue.replace("-", "_").toLowerCase()
                val resources: Resources = resources
                val resourceId: Int = resources.getIdentifier(secondValue, "drawable", packageName)
                val mModel: FlagModel = FlagModel(
                    jsonObject.getString("name"),
                    jsonObject.getString("code"),
                    jsonObject.getString("dial_code"),
                    resourceId,
                    jsonObject.getString("nameImage")
                )
                mCountriesArrayList.add(mModel)
                if (mCountryName == jsonObject.getString("nameImage")) {
                    mCountryCode = jsonObject.getString("dial_code")
                    break
                }
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return mCountryCode
    }

    // - - Read Json from Assets Folder
    private fun readJSONFromAsset(): String? {
        val jsonString: String
        try {
            jsonString =
                mActivity.assets.open("countriesList.json").bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }




    /*
   *
   * Error Alert Dialog With Finish
   * */
    fun showFinishAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            finish()}
        alertDialog.show()
    }

    fun showDefaultDatePicker(mTextView: TextView){
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_date_picker, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        var mDatePickerDP: DatePicker = sheetView.findViewById(R.id.mDatePickerDP)
        mDatePickerDP.minDate = System.currentTimeMillis()



        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        txtSaveTV.setOnClickListener {
            var mMonthValue = mDatePickerDP.month + 1
            var mDayValue = mDatePickerDP.dayOfMonth
            mTextView?.text = ""+mMonthValue + "-" +mDayValue + "-"+mDatePickerDP.year
            mBottomSheetDialog.dismiss()
        }
    }

    fun showDefaultTimePicker(mTextView: TextView){
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View = mActivity.layoutInflater.inflate(R.layout.dialog_time_picker, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTimePickerTP: TimePicker = sheetView.findViewById(R.id.mTimePickerTP)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        txtSaveTV.setOnClickListener {
            mTextView.text = onTimeSet(mTimePickerTP.hour ,mTimePickerTP.minute)
            mBottomSheetDialog.dismiss()
        }
    }

    open fun onTimeSet(hourOfDay: Int, minute: Int) : String{
        var hourOfDay = hourOfDay
        var AM_PM = " AM"
        var mm_precede = ""
        if (hourOfDay >= 12) {
            AM_PM = " PM"
            if (hourOfDay >= 13 && hourOfDay < 24) {
                hourOfDay -= 12
            } else {
                hourOfDay = 12
            }
        } else if (hourOfDay == 0) {
            hourOfDay = 12
        }
        if (minute < 10) {
            mm_precede = "0"
        }
        return "$hourOfDay:$mm_precede$minute$AM_PM"
    }

    fun setLocale(lang: String?) {
        val myLocale = Locale(lang)
        val res: Resources = mActivity.resources
        val dm: DisplayMetrics = res.displayMetrics
        val conf: Configuration = res.configuration
        conf.locale = myLocale
        res.updateConfiguration(conf, dm)
    }
}




