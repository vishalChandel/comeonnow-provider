package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.interfaces.QuestionSelectInterface
import com.comeonnow.provider.app.model.GetAllQuestionsAnswersDataItem
import com.comeonnow.provider.app.model.QlistItem
import com.comeonnow.provider.app.ui.activity.SupportBotActivity.Companion.isTopicClickable
import com.comeonow.provider.app.R

class TopicsAdpater(
    var mActivity: Activity,
    private var getAllQuestionsAnswersArraylist: ArrayList<GetAllQuestionsAnswersDataItem>,
    private val questionSelectInterface: QuestionSelectInterface
) :
    RecyclerView.Adapter<TopicsAdpater.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_questions, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.answerTV.text = getAllQuestionsAnswersArraylist[position].topic


        holder.itemView.setOnClickListener {
            if (isTopicClickable) {
                questionSelectInterface.questionSelect(getAllQuestionsAnswersArraylist[position].topic.toString())
                isTopicClickable=false
            }
        }

    }

    override fun getItemCount(): Int {
        return getAllQuestionsAnswersArraylist!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val answerTV: TextView = itemView.findViewById(R.id.answerTV)
    }
}
