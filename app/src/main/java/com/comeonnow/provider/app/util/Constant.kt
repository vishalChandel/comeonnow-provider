package com.comeonnow.provider.app.util


// - - Base Server Url
const val BASE_URL = "https://comeonnow.io/come_on_now/"

//const val BASE_URL= "https://comeonnow.io/staging/"
const val SOCKET_URL="https://www.comeonnow.io:3010/"



// - - Constant Values
const val SPLASH_TIME_OUT = 1500
const val DEVICE_TYPE = "1"
const val USER_TYPE = 2
const val user_type="2"
const val NOTI_TYPE="noti_type"

// - - View Holder
const val LEFT_VIEW_HOLDER = 0
const val RIGHT_VIEW_HOLDER = 1

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = "https://comeonnow.io/" + "about-us.html"
const val PP_WEB_LINK = "https://comeonnow.io/" + "privacy-policy.html"
const val TERMS_WEB_LINK ="https://comeonnow.io/" +  "terms&services.html"
const val MODEL = "model"
const val LIST = "list"

// - - Fragment Tags
const val APPOINTMENT_TAG = "appointment_tag"
const val QUEUE_TAG = "queue_tag"
const val NOTIFICATION_TAG = "notification_tag"
const val PROFILE_TAG = "profile_tag"

// - - Shared Preference Keys
const val IS_LOGIN = "is_login"
const val USER_ID = "user_id"
const val ACCESS_TOKEN = "access_token"
const val EMAIL = "email"
const val RM_EMAIL = "re_email"
const val RM_PASSWORD = "re_password"
const val IS_REMEMBER_ME = "is_remember_me"
const val FIRST_NAME = "first_name"
const val LAST_NAME = "last_name"
const val CELL_NO = "cell_no"
const val COUNTRY_CODE = "country_code"
const val PROFILE_IMAGE = "image"
const val BASE_URL_PREF = "base_url"
const val IS_ENGLISH ="is_english"
const val BADGE_COUNT ="badge_count"

//Static arrays:
val GENDER_TYPES = arrayOf("Male", "Female", "Other")
val GENDER_TYPES_IDS = arrayOf("1", "2", "3")
fun getGender(mGender : String) : String{
    if (mGender.equals("1")){
        return GENDER_TYPES[0]
    }else if (mGender.equals("2")){
        return GENDER_TYPES[1]
    }else if (mGender.equals("3")){
        return GENDER_TYPES[1]
    }else{
        return ""
    }
}
