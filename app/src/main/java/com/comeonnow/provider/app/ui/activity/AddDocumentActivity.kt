package com.comeonnow.provider.app.ui.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Build.VERSION.SDK_INT
import android.os.Bundle
import android.provider.DocumentsContract
import android.provider.OpenableColumns
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.comeonnow.provider.app.model.PatientDataItem
import com.comeonnow.provider.app.model.PatientModel
import com.comeonnow.provider.app.model.UploadDocModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.activity_add_document.*
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.*


open class AddDocumentActivity : BaseActivity() {

    private val REQUEST_CODE_PICK_FILE = 201
    private val PERMISSION_REQUEST_CODE = 200
    private val readStorageStr: String = Manifest.permission.READ_EXTERNAL_STORAGE
    private val writeStorageStr: String = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var mPatientArrayList: ArrayList<PatientDataItem> = ArrayList()
    private var mPatientId = ""
    private var strDocumentPath = ""
    private var strDocumentName = ""
    private var selectedDocUri: Uri? = null
    private var strByteArray: ByteArray? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_document)
        ButterKnife.bind(this)
        getPatientNameListing()
    }

    @OnClick(
        R.id.backRL,
        R.id.patientTV,
        R.id.uploadDocRL,
        R.id.addSignatureLL,
        R.id.saveTV

    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.patientTV -> performPatientClick()
            R.id.uploadDocRL -> performUploadDocClick()
            R.id.addSignatureLL -> performAddSignClick()
            R.id.saveTV -> performSaveClick()
        }
    }

    private fun performAddSignClick() {
        if (selectedDocUri != null) {
            val i = Intent(mActivity, SignDocumentActivity::class.java)
            i.putExtra("selDocumentUri", selectedDocUri.toString())
            i.putExtra("selDocumentName", strDocumentName)
            startActivityForResult(i, 123)
        }
    }

    private fun performPatientClick() {
        createPatientListingDialog(mActivity)
    }

    private fun createPatientListingDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList<String>()
        for (i in 0 until mPatientArrayList.size) {
            mDataList.add(mPatientArrayList[i].lastName!! + ", " + mPatientArrayList[i].firstName!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_patient_listing, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mTypeface = Typeface.createFromAsset(assets, "font_poppins_regular.ttf")
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        mWheelPickerWP.typeface = mTypeface

        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)

        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mPatientId = "" + mPatientArrayList[mWheelPickerWP.currentItemPosition].id
            patientTV.text = mDataList[mWheelPickerWP.currentItemPosition]
            mBottomSheetDialog.dismiss()
        }
    }

    private fun performUploadDocClick() {
        if (checkPermission()) {
            openFiles()
        } else {
            requestPermission()
        }
    }

    /*
   * Patient Names Listing & Popup & Api
   * Executions
   * */
    private fun getPatientNameListing() {
        if (isNetworkAvailable(mActivity)) {
            executeGetPatientRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeGetPatientRequest() {
        patientPB.visibility = View.VISIBLE
        patientTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
        val mGetProfileData = ApiClient.apiInterface.hospitalPatientListRequest(getAccessToken())
        mGetProfileData.enqueue(object : Callback<PatientModel> {
            override fun onResponse(call: Call<PatientModel>, response: Response<PatientModel>) {
                patientPB.visibility = View.GONE
                patientTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
                if (response.code() == 200) {
                    val mModel: PatientModel = response.body()!!
                    mPatientArrayList.addAll(mModel.data)
                } else if (response.code() == 401) {
                    showAuthFailedDialog(mActivity)
                } else if (response.code() == 500) {
                    showToast(mActivity, getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<PatientModel>, t: Throwable) {
                patientPB.visibility = View.GONE
                patientTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_dropdown, 0)
            }
        })
    }

    private fun performSaveClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeUploadDocApi()
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }


    private fun executeUploadDocApi() {
        showProgressDialog(mActivity)
        val header: MutableMap<String, String> = HashMap()
        header["Authorization"] = getAccessToken()
        val title: RequestBody =
            titleET.text.toString().trim().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val description: RequestBody = descriptionET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val patientId: RequestBody =
            mPatientId.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val receiverId: RequestBody = "".toRequestBody("multipart/form-data".toMediaTypeOrNull())
        var mMultipartBody1: MultipartBody.Part? = null
        val doc: RequestBody = strByteArray.let { RequestBody.create("multipart/form-data".toMediaTypeOrNull(), it!!) }
        mMultipartBody1 = doc.let {
            MultipartBody.Part.createFormData(
                "docs",
                getAlphaNumericString()!!.toString() + ".pdf",
                it
            )
        }


        val call = ApiClient.apiInterface.uploadDocRequest(
            header,
            title,
            description,
            patientId,
            receiverId,
            mMultipartBody1
        )

        call.enqueue(object : Callback<UploadDocModel> {
            override fun onFailure(call: Call<UploadDocModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<UploadDocModel>,
                response: Response<UploadDocModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        val model = response.body()
                        if(!model?.message.isNullOrEmpty()) {
                            showToast(mActivity, model?.message)
                        }
                        onBackPressed()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    403 -> {

                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    private fun isValidate(): Boolean {
        var flag = true
        when {
            titleET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_title))
                flag = false
            }
            descriptionET!!.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_description))
                flag = false
            }
            patientTV.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_your_patient))
                flag = false
            }
        }
        return flag
    }

    private fun checkPermission(): Boolean {
        val readStorage = ContextCompat.checkSelfPermission(applicationContext, readStorageStr)
        val writeStorage = ContextCompat.checkSelfPermission(applicationContext, writeStorageStr)
        return readStorage == PackageManager.PERMISSION_GRANTED && writeStorage == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(readStorageStr, writeStorageStr),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.isNotEmpty()) {
                var i = 0
                while (i < grantResults.size) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        openFiles()
                    } else {
                        Log.e("TAG", "onRequestPermissionsResult: failed")
                    }
                    i++
                }
            }
        }
    }


    private fun openFiles() {
        val chooseFile = Intent(Intent.ACTION_GET_CONTENT)
        chooseFile.type = "*/*"
        val mimetypes = arrayOf("application/pdf")
        chooseFile.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)
        startActivityForResult(chooseFile, REQUEST_CODE_PICK_FILE)
    }

    @SuppressLint("Range")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_PICK_FILE && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                try {
                    val uri: Uri? = data.data
                    selectedDocUri = uri
                    val uriString = uri.toString()
                    val myFile = File(uriString)
                    strDocumentPath = myFile.absolutePath
                    if (uriString.startsWith("content://")) {
                        var cursor: Cursor? = null
                        try {
                            cursor = mActivity.contentResolver.query(uri!!, null, null, null, null)
                            if (cursor != null && cursor.moveToFirst()) {
                                strDocumentName =
                                    cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                                pdfTV.visibility = View.VISIBLE
                                pdfIV.visibility = View.GONE
                                pdfTV.text = strDocumentName
                            }
                        } finally {
                            cursor!!.close()
                        }
                    } else if (uriString.startsWith("file://")) {
                        strDocumentName = myFile.name.toString()
                        pdfTV.visibility = View.VISIBLE
                        pdfIV.visibility = View.GONE
                        pdfTV.text = strDocumentName
                    }

                    var iStream: InputStream? = null
                    if (isVirtualFile(uri!!)) {
                        try {
                            iStream = getInputStreamForVirtualFile(uri!!, "application/pdf")
                        } catch (e: IOException) {
                            Log.i(TAG, "onActivityResult: $e")
                            e.printStackTrace()
                        }
                    } else {
                        try {
                            iStream = contentResolver.openInputStream(uri!!)
                        } catch (e: FileNotFoundException) {
                            e.printStackTrace()
                        }
                    }
                    try {
                        strByteArray = getBytes(iStream!!)
                    } catch (e: Exception) {
                        Log.i(TAG, "onActivityResult: $e")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        } else if (requestCode == 123 && resultCode == Activity.RESULT_OK) {
            val tmpLocalFile = File("$cacheDir/sample.pdf")
            val fileInputStream = FileInputStream(tmpLocalFile)
            strByteArray = getBytes(fileInputStream)
        }
    }


    private fun isVirtualFile(uri: Uri): Boolean {
        return if (SDK_INT >= Build.VERSION_CODES.KITKAT) {
            if (!DocumentsContract.isDocumentUri(mActivity, uri)) {
                return false
            }
            val cursor = contentResolver.query(
                uri, arrayOf(DocumentsContract.Document.COLUMN_FLAGS),
                null, null, null
            )
            var flags = 0
            if (cursor!!.moveToFirst()) {
                flags = cursor.getInt(0)
            }
            cursor.close()
            flags and DocumentsContract.Document.FLAG_VIRTUAL_DOCUMENT != 0
        } else {
            false
        }
    }

    @Throws(IOException::class)
    private fun getInputStreamForVirtualFile(uri: Uri, mimeTypeFilter: String): InputStream? {
        val resolver = contentResolver
        val openableMimeTypes = resolver.getStreamTypes(uri, mimeTypeFilter)
        if (openableMimeTypes == null ||
            openableMimeTypes.isEmpty()
        ) {
            throw FileNotFoundException()
        }
        return resolver
            .openTypedAssetFileDescriptor(uri, openableMimeTypes[0], null)
            ?.createInputStream()
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray? {
        val byteBuffer = ByteArrayOutputStream()
        val bufferSize = 512
        val buffer = ByteArray(bufferSize)
        var len = 0
        while (inputStream.read(buffer).also { len = it } != -1) {
            byteBuffer.write(buffer, 0, len)
        }
        return byteBuffer.toByteArray()
    }

}