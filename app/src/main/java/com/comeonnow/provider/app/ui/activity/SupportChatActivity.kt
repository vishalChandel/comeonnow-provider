package com.comeonnow.provider.app.ui.activity

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import com.comeonnow.provider.app.ProviderApp
import com.comeonnow.provider.app.adapter.SupportChatAdapter
import com.comeonnow.provider.app.model.CreateRoomResponse
import com.comeonnow.provider.app.model.GetAllMessagesResponse
import com.comeonnow.provider.app.model.MessageData
import com.comeonnow.provider.app.model.SendMessageResponse
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.LEFT_VIEW_HOLDER
import com.comeonnow.provider.app.util.RIGHT_VIEW_HOLDER
import com.comeonow.provider.app.R
import com.google.gson.Gson
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_support_chat.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SupportChatActivity : BaseActivity() {
    lateinit var supportSupportChatAdapter: SupportChatAdapter
    var getAllMessagesArrayList: ArrayList<MessageData> = ArrayList()
    private var mOtherUserId: String = "1"
    private var mRoomId: String = ""
    private var mSocket: Socket? = null
    var mPageNo: Int = 1
    var mIsPagination = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support_chat)
        initView()
        onViewClicked()
    }

    private fun initView() {
        if (isNetworkAvailable(mActivity)) {
            mRoomId = getUserId()
            setUpSocketData()
        } else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun setTopSwipeRefresh() {
        mSwipeRefreshSR.setColorSchemeColors(
            resources.getColor(R.color.colorBlue),
            resources.getColor(R.color.colorBlack),
            resources.getColor(R.color.colorRed)
        )
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination) {
                ++mPageNo
                executeGetAllMessagesApi()
            } else {
                mSwipeRefreshSR.isRefreshing = false
            }
        }
    }

    private fun onViewClicked() {
        backRL.setOnClickListener {
            onBackPressed()
        }

        send_IV.setOnClickListener {
            if (editSendMsgET.text.toString().trim() != "") {
                if (isNetworkAvailable(mActivity)) {
                    executeSendMsgApi()
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }

        editSendMsgET.setOnClickListener {
            Handler(Looper.getMainLooper()).postDelayed({
                scrollToBottom()
            }, 100)
        }
    }

    private fun executeCreateRoomApi() {
        val call = ApiClient.apiInterface.createRoomRequest(getAccessToken(), mOtherUserId)
        call.enqueue(object : Callback<CreateRoomResponse> {
            override fun onFailure(call: Call<CreateRoomResponse>, t: Throwable) {
            }

            override fun onResponse(
                call: Call<CreateRoomResponse>,
                response: Response<CreateRoomResponse>
            ) {
                when (response.code()) {
                    200 -> {
                        mRoomId = getUserId()
                        Log.e("RoomId:", mRoomId)
                        setUpSocketData()
                    }
                    400 -> {
                        showAlertDialog(mActivity, "No data found.")
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

        })
    }

    private fun executeGetAllMessagesApi() {
        mSwipeRefreshSR.isRefreshing = true
        val call = ApiClient.apiInterface.getAllMessagesRequest(
            getAccessToken(),
            mRoomId,
            mPageNo.toString(),
            "2000"
        )
        call.enqueue(object : Callback<GetAllMessagesResponse> {
            override fun onFailure(call: Call<GetAllMessagesResponse>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<GetAllMessagesResponse>,
                response: Response<GetAllMessagesResponse>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        mSwipeRefreshSR.isRefreshing = false
                        if (getAllMessagesArrayList.isNotEmpty()) {
                            getAllMessagesArrayList.clear()
                        }
                        if (mPageNo == 1) {
                            Log.i(TAG, "onResponse: " + response.body()!!.data)
                            getAllMessagesArrayList.addAll(response.body()!!.data)
                            mIsPagination = getAllMessagesArrayList.size >= 2000
                            for (i in 0 until getAllMessagesArrayList.size) {
                                if (getAllMessagesArrayList[i].senderId!!.toString()
                                        .trim() == getUserId().trim()
                                ) {
                                    getAllMessagesArrayList[i].viewType = RIGHT_VIEW_HOLDER
                                } else {
                                    getAllMessagesArrayList!![i]!!.viewType = LEFT_VIEW_HOLDER
                                }

                                setAdapter()
                                scrollToBottom()
                            }
                        } else if (mPageNo > 1) {
                            getAllMessagesArrayList.addAll(0, response.body()?.data!!)
                            for (i in 0 until getAllMessagesArrayList!!.size) {
                                if (getAllMessagesArrayList!![i]!!.senderId!!.equals(getUserId())
                                ) {
                                    getAllMessagesArrayList!![i]!!.viewType = RIGHT_VIEW_HOLDER
                                } else {
                                    getAllMessagesArrayList!![i]!!.viewType = LEFT_VIEW_HOLDER
                                }
                                supportSupportChatAdapter?.notifyDataSetChanged()
                            }
                        }
                    }

                    400 -> {
                        mSwipeRefreshSR.isRefreshing = false
                        supportSupportChatAdapter?.notifyDataSetChanged()
                        txtNoDataFoundTV.visibility = View.VISIBLE
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                    else -> {
                        mSwipeRefreshSR.isRefreshing = false
                    }
                }
            }

        })

    }

    private fun setAdapter() {
        supportSupportChatAdapter = SupportChatAdapter(mActivity, getAllMessagesArrayList)
        chatRV.isNestedScrollingEnabled = false
        chatRV.adapter = supportSupportChatAdapter
    }

    private fun executeSendMsgApi() {
        mProgressPB.visibility = View.VISIBLE
        send_IV.visibility = View.GONE
        val call = ApiClient.apiInterface.sendMessageRequest(
            getAccessToken(),
            editSendMsgET.text.toString(),
            mRoomId
        )

        call.enqueue(object : Callback<SendMessageResponse> {
            override fun onFailure(call: Call<SendMessageResponse>, t: Throwable) {
                mProgressPB.visibility = View.GONE
                send_IV.visibility = View.VISIBLE
            }

            override fun onResponse(
                call: Call<SendMessageResponse>,
                response: Response<SendMessageResponse>
            ) {
                mProgressPB.visibility = View.GONE
                send_IV.visibility = View.VISIBLE
                when (response.code()) {
                    200 -> {
                        response.body()?.data!!.viewType = RIGHT_VIEW_HOLDER
                        if (getAllMessagesArrayList!!.size == 0) {
                            getAllMessagesArrayList!!.add(
                                getAllMessagesArrayList!!.size,
                                response.body()?.data!!
                            )
                            setAdapter()
                        } else {
                            getAllMessagesArrayList!!.add(
                                getAllMessagesArrayList!!.size,
                                response.body()?.data!!
                            )
                            supportSupportChatAdapter!!.notifyDataSetChanged()
                        }

                        scrollToBottom()
                        // perform the sending message attempt.
                        val gson = Gson()
                        val mOjectString = gson.toJson(response.body()!!.data)
                        var obj: JSONObject? = null
                        try {
                            obj = JSONObject(mOjectString)
                            Log.d("My App", obj.toString())
                        } catch (t: Throwable) {
                            Log.e(
                                "My App",
                                "Could not parse malformed JSON: \"$mOjectString\""
                            )
                        }
                        Log.i(TAG, "onResponse: $obj")
                        mSocket!!.emit("newMessage", mRoomId,
                        )
                        editSendMsgET.setText("")
                    }
                    400 -> {

                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

        })
    }


    private fun setUpSocketData() {
        val app: ProviderApp = application as ProviderApp
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", mRoomId)
        mSocket!!.emit("ConncetedChat", "1")
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        mSocket!!.on("leaveChat", onUserLeft)
        mSocket!!.connect()
        if(mSocket!!.connected()){
            executeGetAllMessagesApi()
        }
    }


    // - - Socket Chat Implementations:
    private val onConnect = Emitter.Listener {
        runOnUiThread {
        }
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread {
        }
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread {
        }
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                Log.e(TAG, "****MessageObject****" + args[1].toString())
                val mGson = Gson()
                val mChatMessageData: MessageData =
                    mGson.fromJson(args[1].toString(), MessageData::class.java)

                if (mChatMessageData.senderId!!.toString() == getUserId()) {
                    mChatMessageData.viewType = RIGHT_VIEW_HOLDER
                } else {
                    mChatMessageData.viewType = LEFT_VIEW_HOLDER
                }
                getAllMessagesArrayList.add(getAllMessagesArrayList.size, mChatMessageData)
                supportSupportChatAdapter.notifyDataSetChanged()
                scrollToBottom()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }

    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
                scrollToBottom()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }

    private fun scrollToBottom() {
        chatRV.scrollToPosition(getAllMessagesArrayList!!.size - 1)
    }

    override fun onResume() {
        super.onResume()
        executeGetAllMessagesApi()
    }

}