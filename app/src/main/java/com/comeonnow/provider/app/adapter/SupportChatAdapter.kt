package com.comeonnow.provider.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.model.ChattingModelClass
import com.comeonnow.provider.app.model.MessageData
import com.comeonnow.provider.app.util.LEFT_VIEW_HOLDER
import com.comeonow.provider.app.R


class SupportChatAdapter(private val context: Context, var list: ArrayList<MessageData>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private inner class MessageInViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var messageTV: TextView
        fun bind(position: Int) {
            val messageModel: MessageData = list[position]
            messageTV.text = messageModel.message

        }

        init {
            messageTV = itemView.findViewById(R.id.messageTV)

        }
    }

    private inner class MessageOutViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var messageTV: TextView
        fun bind(position: Int) {
            val messageModel: MessageData = list[position]
            messageTV.text = messageModel.message

        }

        init {
            messageTV = itemView.findViewById(R.id.messageTV)

        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == LEFT_VIEW_HOLDER) {
            MessageInViewHolder(
                LayoutInflater.from(context).inflate(R.layout.item_left_chat_view, parent, false)
            )
        } else MessageOutViewHolder(
            LayoutInflater.from(context).inflate(R.layout.item_right_chat_view, parent, false)
        )
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (list[position].viewType == LEFT_VIEW_HOLDER) {
            (holder as MessageInViewHolder).bind(position)
        } else {
            (holder as MessageOutViewHolder).bind(position)
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].viewType
    }


}