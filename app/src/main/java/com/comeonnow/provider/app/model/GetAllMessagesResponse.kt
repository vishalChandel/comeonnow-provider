package com.comeonnow.provider.app.model

import com.google.gson.annotations.SerializedName

data class GetAllMessagesResponse(

	@field:SerializedName("data")
	val data: ArrayList<MessageData> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
