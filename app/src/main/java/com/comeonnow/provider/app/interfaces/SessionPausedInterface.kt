package com.comeonnow.provider.app.interfaces

interface SessionPausedInterface {
    fun sessionPaused() {}
}