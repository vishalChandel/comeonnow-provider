package com.comeonnow.provider.app.model

import com.google.gson.annotations.SerializedName

data class AppointmentHistoryModel(

	@field:SerializedName("data")
	val data: List<AppointmentHistoryDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class AppointmentHistoryDataItem(

	@field:SerializedName("appoint_start_time")
	val appointStartTime: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("gender")
	val gender: Int? = null,

	@field:SerializedName("loginuser_name")
	val loginuserName: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("type")
	val type: Int? = null,

	@field:SerializedName("appoint_end_time")
	val appointEndTime: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("relationship")
	val relationship: String? = null,

	@field:SerializedName("appoint_date")
	val appointDate: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("age")
	val age: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
