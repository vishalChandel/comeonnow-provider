package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SelectUserModel(

	@field:SerializedName("data")
	val data: ArrayList<UserDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class UserDataItem(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("user_id")
	val userId: Int? = null,

	@field:SerializedName("patient_id")
	val patientId: Int? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("isSelected")
	var isSelected: Boolean? = false
) : Parcelable
