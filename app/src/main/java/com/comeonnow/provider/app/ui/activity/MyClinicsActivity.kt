package com.comeonnow.provider.app.ui.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import butterknife.ButterKnife
import butterknife.OnClick
import com.comeonnow.provider.app.interfaces.DeleteMyClinicInterface
import com.comeonnow.provider.app.adapter.MyClinicsAdapter
import com.comeonnow.provider.app.model.MyClinicsDataItem
import com.comeonnow.provider.app.model.MyClinicsModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.activity_my_clinics.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.collections.ArrayList

class MyClinicsActivity : BaseActivity() {

    // - - Initialize Objects
    var mMyClinicsArrayList : ArrayList<MyClinicsDataItem> = ArrayList()
    private lateinit var mMyClinicsAdapter: MyClinicsAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_clinics)
        ButterKnife.bind(this)
    }

    private fun initView() {
        getMyClinicsData()
    }

    private fun getMyClinicsData() {
        if (isNetworkAvailable(mActivity)) {
            executeMyClinicsRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun executeMyClinicsRequest() {
        showProgressDialog(mActivity)
        val mMyClinicsData = ApiClient.apiInterface.myClinicsListRequest(getAccessToken())
        mMyClinicsData.enqueue(object : Callback<MyClinicsModel> {
            override fun onResponse(call: Call<MyClinicsModel>, response: Response<MyClinicsModel>) {
                dismissProgressDialog()
                mMyClinicsArrayList.clear()
                if (response.code() == 200) {
                    val mModel: MyClinicsModel = response.body()!!
                    if (mModel.data.size > 0) {
                        txtNoDataFoundTV.visibility = View.GONE
                        mMyClinicsArrayList.addAll(mModel.data)
                        setAdapter()
                    } else {
                        setAdapter()
                        txtNoDataFoundTV.visibility = View.VISIBLE
                    }
                }else if (response.code() == 404){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                }else if (response.code() == 401){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                }
            }

            override fun onFailure(call: Call<MyClinicsModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    private fun setAdapter() {
        mMyClinicsAdapter = MyClinicsAdapter(this,mMyClinicsArrayList,mDeleteMyClinicInterface)
        myClinicsRV.adapter = mMyClinicsAdapter
    }

    @OnClick(
        R.id.backRL,
        R.id.editRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.editRL -> performEditClick()
        }
    }

    private fun performEditClick() {
        val i = Intent(this, AddClinicActivity::class.java)
        startActivity(i)
    }

    private var mDeleteMyClinicInterface : DeleteMyClinicInterface = object : DeleteMyClinicInterface {
        override fun onItemClickListener(mModel: MyClinicsDataItem) {
            executeDeleteMyClinicRequest(mModel.id)
        }
    }

    private fun executeDeleteMyClinicRequest(id: String?) {
        showProgressDialog(mActivity)
        val mDeleteMyClinicData = ApiClient.apiInterface.deleteMyClinicRequest(getAccessToken(),id.toString())
        mDeleteMyClinicData.enqueue(object : Callback<MyClinicsModel> {
            override fun onResponse(call: Call<MyClinicsModel>, response: Response<MyClinicsModel>) {
                dismissProgressDialog()
                mMyClinicsArrayList.clear()
                if (response.code() == 200) {
                    showToast(mActivity,getString(R.string.clinic_deleted_successfully))
                    val mModel: MyClinicsModel = response.body()!!
                    if (mModel.data.size > 0) {
                        txtNoDataFoundTV.visibility = View.GONE
                        mMyClinicsArrayList.addAll(mModel.data)
                        setAdapter()
                    } else {
                        setAdapter()
                        txtNoDataFoundTV.visibility = View.VISIBLE
                    }
                }else if (response.code() == 404){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                }else if (response.code() == 401){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                    showAuthFailedDialog(mActivity)
                }else if (response.code() == 500){
                    txtNoDataFoundTV.visibility = View.VISIBLE
                    myClinicsRV.visibility=View.GONE
                }
            }

            override fun onFailure(call: Call<MyClinicsModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }


    override fun onResume() {
        super.onResume()
        initView()
    }
}