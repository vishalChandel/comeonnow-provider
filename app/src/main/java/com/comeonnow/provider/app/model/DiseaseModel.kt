package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class DiseaseModel(

	@field:SerializedName("data")
	val data: List<DiseaseDataItem>,

	@field:SerializedName("message")
	val message: String,

	@field:SerializedName("status")
	val status: Int
) : Parcelable

@Parcelize
data class DiseaseDataItem(

	@field:SerializedName("id")
	val id: Int,

	@field:SerializedName("disease_name")
	val diseaseName: String,

	@field:SerializedName("creation_date")
	val creationDate: String,

	@field:SerializedName("status")
	val status: Int
) : Parcelable
