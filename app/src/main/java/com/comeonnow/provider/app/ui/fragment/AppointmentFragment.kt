package com.comeonnow.provider.app.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.comeonnow.provider.app.adapter.AppointmentAdapter
import com.comeonnow.provider.app.model.AppointmentListingModel
import com.comeonnow.provider.app.model.AptDataItem
import com.comeonnow.provider.app.model.SignUpErrorModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.ui.activity.AddAppointmentActivity
import com.comeonow.provider.app.R
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.fragment_appointment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.*

class AppointmentFragment : BaseFragment() {

    // - - Initialize Widgets
    @BindView(R.id.appointmentRV)
    lateinit var appointmentRV: RecyclerView

    // - - Initialize Objects
    lateinit var mUnbinder: Unbinder
    var mAppointmentAdapter: AppointmentAdapter? = null
    var mAppointmentList: ArrayList<AptDataItem?>? = ArrayList<AptDataItem?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_appointment, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        executeAppointmentListingApi()
        return view
    }

    @OnClick(
        R.id.addAppointmentLL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.addAppointmentLL -> performAddAppointmentClick()
        }
    }

    private fun performAddAppointmentClick() {
        val intent = Intent(activity, AddAppointmentActivity::class.java)
        startActivity(intent)
    }

    // - - Execute Appointment Listing Api
    private fun executeAppointmentListingApi() {
        showProgressDialog(activity)
        val call = ApiClient.apiInterface.appointmentListingRequest(getAccessToken(), "500", "1", "2")
        call.enqueue(object : Callback<AppointmentListingModel> {
            override fun onResponse(
                call: Call<AppointmentListingModel>,
                response: Response<AppointmentListingModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        if (response.body()?.data != null) {
                            mAppointmentList = response.body()?.data as ArrayList<AptDataItem?>?
                            setAdapter(activity, mAppointmentList)
                            Log.e("AppointmentList:", "$mAppointmentList")
                        }else{
                            val gson = GsonBuilder().create()
                            val mError: SignUpErrorModel
                            try {
                                mError = gson.fromJson(
                                    response.errorBody()!!.string(),
                                    SignUpErrorModel::class.java
                                )
                                txtNoDataFoundTV.visibility = View.VISIBLE
                                txtNoDataFoundTV.text =mError.message
                                setAdapter(activity, mAppointmentList)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }

                        }
                    }
                    401 -> {
                        val gson = GsonBuilder().create()
                        val mError: SignUpErrorModel
                        try {
                            mError = gson.fromJson(
                                response.errorBody()!!.string(),
                                SignUpErrorModel::class.java
                            )
                            txtNoDataFoundTV.visibility = View.VISIBLE
                            txtNoDataFoundTV.text =mError.message
                            showAuthFailedDialog(activity)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                    404 -> {
                        val gson = GsonBuilder().create()
                        val mError: SignUpErrorModel
                        try {
                            mError = gson.fromJson(
                                response.errorBody()!!.string(),
                                SignUpErrorModel::class.java
                            )
                            txtNoDataFoundTV.visibility = View.VISIBLE
                            txtNoDataFoundTV.text =mError.message
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                    500 -> {
                        val gson = GsonBuilder().create()
                        val mError: SignUpErrorModel
                        try {
                            mError = gson.fromJson(
                                response.errorBody()!!.string(),
                                SignUpErrorModel::class.java
                            )
                            txtNoDataFoundTV.visibility = View.VISIBLE
                            txtNoDataFoundTV.text =mError.message
                            showToast(activity, getString(R.string.internal_server_error))
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }

                    }
                }
            }

            override fun onFailure(call: Call<AppointmentListingModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }

    private fun setAdapter(
        activity: FragmentActivity?,
        mAppointmentList: ArrayList<AptDataItem?>?
    ) {
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        appointmentRV.layoutManager = layoutManager
        mAppointmentAdapter = AppointmentAdapter(activity, mAppointmentList)
        appointmentRV.adapter = mAppointmentAdapter
    }
}