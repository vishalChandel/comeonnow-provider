package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class AppointmentTypeModel(

	@field:SerializedName("data")
	val data: ArrayList<AppointmentTypeDataItem> = ArrayList(),

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class AppointmentTypeDataItem(

	@field:SerializedName("updated_at")
	val updatedAt: String? = "",

	@field:SerializedName("name")
	val name: String? = "",

	@field:SerializedName("created_at")
	val createdAt: String? = null,

	@field:SerializedName("id")
	val id: Int? = -1
) : Parcelable
