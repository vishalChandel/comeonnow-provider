package com.comeonnow.provider.app.interfaces

interface QuestionSelectInterface {
    fun questionSelect(selectionString: String) {}
}