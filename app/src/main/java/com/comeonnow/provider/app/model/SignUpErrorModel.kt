package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SignUpErrorModel(
	@field:SerializedName("hasErrors")
	val hasErrors: Boolean? = null,

	@field:SerializedName("status")
	val status: String? = "",

	@field:SerializedName("message")
	val message: String? = "",

	@field:SerializedName("errors")
	val errors: Errors? = null
) : Parcelable

@Parcelize
data class Errors(
	@field:SerializedName("username")
	val username: List<String?>? = ArrayList<String>(),

	@field:SerializedName("cellno")
	val cellno: List<String?>? = null
) : Parcelable
