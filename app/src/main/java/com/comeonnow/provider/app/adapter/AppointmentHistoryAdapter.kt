package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.model.AppointmentHistoryDataItem
import com.comeonow.provider.app.R

class AppointmentHistoryAdapter(var mActivity: Activity?, var mAppointmentHistoryList: ArrayList<AppointmentHistoryDataItem?>?) :
    RecyclerView.Adapter<AppointmentHistoryAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.item_appointment, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel = mAppointmentHistoryList!![position]
        holder.allListIV1.setImageResource(R.drawable.ic_completed)
        holder.txtAppointmentStatusTV.text=mActivity!!.getString(R.string.completed)

        mActivity?.let {
            Glide.with(it).load(mModel?.image)
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(holder.allListImage)
        }
        if (mModel != null) {
            holder.allListNameTV.text = mModel.lastName + ","+ " "+mModel.firstName
            holder.allListAgeTV.text = mModel.age.toString() + " "+mActivity!!.resources?.getString(R.string.years_old)
            holder.allListDateTV.text = mModel.appointDate.toString()
            holder.allListTimeTV.text = mModel.appointStartTime.toString() + " - " + mModel.appointEndTime
            when (mModel.gender) {
                1 -> {
                    holder.allListGenderTV.text = mActivity!!.resources?.getString(R.string.male)
                }
                2 -> {
                    holder.allListGenderTV.text = mActivity!!.resources?.getString(R.string.female)
                }
                3 -> {
                    holder.allListGenderTV.text = mActivity!!.resources?.getString(R.string.others)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return mAppointmentHistoryList!!.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var allListImage: ImageView = itemView.findViewById(R.id.allListIV)
        var allListNameTV: TextView = itemView.findViewById(R.id.allListNameTV)
        var allListAgeTV: TextView = itemView.findViewById(R.id.allListAgeTV)
        var allListGenderTV: TextView = itemView.findViewById(R.id.allListGenderTV)
        var allListDateTV: TextView = itemView.findViewById(R.id.allListDateTV)
        var allListTimeTV: TextView = itemView.findViewById(R.id.allListTimeTV)
        var allListIV1: ImageView = itemView.findViewById(R.id.allListIV1)
        var txtAppointmentStatusTV: TextView = itemView.findViewById(R.id.txtAppointmentStatusTV)
    }
}