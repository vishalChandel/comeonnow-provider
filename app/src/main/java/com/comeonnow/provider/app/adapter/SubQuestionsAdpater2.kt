package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.interfaces.QuestionSelectInterface
import com.comeonnow.provider.app.model.SubquestionItem
import com.comeonnow.provider.app.ui.activity.SupportBotActivity.Companion.isSubQuestion2Clickable
import com.comeonnow.provider.app.ui.activity.SupportBotActivity.Companion.isSubQuestionClickable
import com.comeonnow.provider.app.ui.activity.SupportBotActivity.Companion.isTopicClickable
import com.comeonow.provider.app.R

class SubQuestionsAdpater2(
    var mActivity: Activity,
    private var getAllSubquestionArraylist: ArrayList<SubquestionItem>,
    private val questionSelectInterface: QuestionSelectInterface
) :
    RecyclerView.Adapter<SubQuestionsAdpater2.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_questions, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        holder.answerTV.text = getAllSubquestionArraylist[position].questions


        holder.itemView.setOnClickListener {
            if (isSubQuestion2Clickable) {
                questionSelectInterface.questionSelect(getAllSubquestionArraylist[position].questions.toString())
                isSubQuestion2Clickable=false
            }
        }

    }

    override fun getItemCount(): Int {
        return getAllSubquestionArraylist.size
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        val answerTV: TextView = itemView.findViewById(R.id.answerTV)
    }
}
