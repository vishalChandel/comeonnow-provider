package com.comeonnow.provider.app.ui.activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.model.FlagModel
import com.comeonnow.provider.app.model.GetProfileModel
import com.comeonnow.provider.app.model.ProfileErrorModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.*
import com.comeonow.provider.app.R
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_edit_profile.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

class EditProfileActivity : BaseActivity() {

    // - - Initialize objects
    var mBitmap: Bitmap? = null
    var mCountryImage: String? = null

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = Manifest.permission.CAMERA

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(this)
        setEditTextFocused(editPhoneET)
    }

    // - - Set Data on Widgets
    private fun setDataOnWidgets() {
        editFirstNameET.setText(firstName())
        editFirstNameET.setSelection(firstName().length)
        editLastNameET.setText(lastName())
        editLastNameET.setSelection(lastName().length)
        editEmailET.text = userEmail()
        editPhoneET.setText(cellNo())
        editPhoneET.setSelection(cellNo().length)
        txtCountryCodeTV.text = "+"+countryCode()
        if (mCountryImage == null) {
            mCountryImage = countryCode()
        }
        Glide.with(mActivity).load(profilePicture())
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(profilePicIV)
    }

    @OnClick(
        R.id.backRL,
        R.id.profilePicIV,
        R.id.btnSaveTV,
        R.id.txtCountryCodeTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.profilePicIV -> performProfilePicClick()
            R.id.btnSaveTV -> performSaveClick()
            R.id.txtCountryCodeTV -> performCountryCodeClick()
        }
    }

// - - Open Country List Activity
    private fun performCountryCodeClick() {
        val intent = Intent(mActivity, CountriesActivity::class.java)
        startActivityForResult(intent, 555)
    }

    private fun performProfilePicClick() {
        if (checkPermission()) {
            onSelectImageClick()
        } else {
            requestPermission()
        }
    }

    private fun performSaveClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                Log.i(TAG, "performSaveClick: "+countryCode().trim()+"::"+txtCountryCodeTV.text.trim().substring(1)+"::"+ cellNo().trim()+"::"+editPhoneET.text.trim())
                if(countryCode().trim()==txtCountryCodeTV.text.trim().substring(1).trim() && cellNo().toString().trim()==editPhoneET.text.toString().trim()){
                    executeUpdateProfileApi()
                }else{
                    val i = Intent(this, OtpVerificationActivity::class.java)
                    i.putExtra("tag","editProfileActivity")
                    i.putExtra("phoneNumber", editPhoneET.text.toString().trim())
                    i.putExtra("countryCode", txtCountryCodeTV.text.toString().trim())
                    startActivity(i)
                }


            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            //Crop image(Optional), Check Customization for more option
            .crop()
            //Final image size will be less than 1 MB(Optional)
            .compress(480)
            //Final image resolution will be less than 1080 x 1080(Optional)
            .maxResultSize(512, 512)
            .cropSquare()
            .start()
    }


    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!
                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_profile)
                        .into(profilePicIV)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    val out = ByteArrayOutputStream()
                    selectedImage.compress(Bitmap.CompressFormat.PNG, 60, out)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }
            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
        if (requestCode == 555) {
            if (data != null) {
                var mModel: FlagModel? = data.getParcelableExtra(MODEL)
                txtCountryCodeTV.setText(mModel!!.countryPhoneCode)
                mCountryImage = mModel.countryImageName
            }
        }
    }

    // - - Execute Edit Profile Api
    private fun executeUpdateProfileApi() {
        var  mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "image",
                    getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )
            }
        }
        val first_name: RequestBody = editFirstNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val last_name: RequestBody = editLastNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val email: RequestBody = editEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val cellno: RequestBody = editPhoneET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val countrycode: RequestBody =
            mCountryImage.toString().trim().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val usertype: RequestBody =
            user_type.toRequestBody("multipart/form-data".toMediaTypeOrNull())
        showProgressDialog(mActivity)
        val headers: MutableMap<String, String> = HashMap()
        headers["Authorization"] = getAccessToken()
        val call = ApiClient.apiInterface.editProfileRequest(
            headers,
            mMultipartBody,
            first_name,
            last_name,
            email,
            cellno,
            countrycode,
            usertype
        )
        Log.e("EditProfileParams:", "AccessToken:${headers},Image:$mMultipartBody,FirstName:$first_name,LastName:$last_name,Email:$email:,PhoneNumber$cellno,CountryCode:$countrycode,UserType:$usertype")
        call.enqueue(object : Callback<GetProfileModel> {
            override fun onResponse(
                call: Call<GetProfileModel>,
                response: Response<GetProfileModel>
            ) {
                dismissProgressDialog()
                val mModel = response.body()
                when {
                    response.code() == 200 -> {
                        showToast(mActivity, getString(R.string.profile_updated_sucessfully))
                        AppPreference().writeString(mActivity, FIRST_NAME, mModel?.data?.firstName)
                        AppPreference().writeString(mActivity, LAST_NAME, mModel?.data?.lastName)
                        AppPreference().writeString(mActivity, CELL_NO, mModel?.data?.cellno)
                        AppPreference().writeString(mActivity, EMAIL, mModel?.data?.email)
                        AppPreference().writeString(mActivity, COUNTRY_CODE, mModel?.data?.countrycode)
                        AppPreference().writeString(mActivity, PROFILE_IMAGE, mModel?.data?.image)
                        finish()
                    }
                    response.code() == 401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    response.code() == 409 -> {
                        val gson = GsonBuilder().create()
                        var mError = ProfileErrorModel()
                        try {
                            mError = gson.fromJson(response.errorBody()!!.string(), ProfileErrorModel::class.java)
                            if (mError.errors?.cellno?.get(0).toString()!!.length > 0) {
                                showAlertDialog(
                                    mActivity,
                                    mError.errors?.cellno?.get(0).toString()
                                )
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    response.code() == 500 -> {
                        showAlertDialog(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<GetProfileModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

        })
    }

    // - - Validations for Edit Profile fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editFirstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_first_name))
                flag = false
            }
            editLastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_last_name))
                flag = false
            }
            txtCountryCodeTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_country_code))
                flag = false
            }
            editPhoneET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_phone_number))
                flag = false
            }
            editPhoneET.text.toString().length < 10 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_phone_number))
                flag = false
            }
        }
        return flag
    }

    override fun onResume() {
        super.onResume()
        setDataOnWidgets()
    }
}