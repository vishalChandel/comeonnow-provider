package com.comeonnow.provider.app.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SendMessageResponse(

	@field:SerializedName("data")
	val data: MessageData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class MessageData(

	@field:SerializedName("created_on")
	val createdOn: String? = null,

	@field:SerializedName("receiver_id")
	val receiverId: Int? = null,

	@field:SerializedName("disable")
	val disable: Int? = null,

	@field:SerializedName("room_no")
	val roomNo: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("sender_id")
	val senderId: Int? = null,

	@field:SerializedName("chat_id")
	val chatId: Int? = null,

	var viewType: Int = -1
) : Parcelable
