package com.comeonnow.provider.app.fcm

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.comeonnow.provider.app.ui.activity.HomeActivity
import com.comeonnow.provider.app.ui.activity.SplashActivity
import com.comeonnow.provider.app.util.AppPreference
import com.comeonnow.provider.app.util.BADGE_COUNT
import com.comeonnow.provider.app.util.IS_LOGIN
import com.comeonnow.provider.app.util.NOTI_TYPE
import com.comeonow.provider.app.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import org.json.JSONObject
import kotlin.random.Random


// - - Initialize Objects
private const val CHANNEL_ID = "my_channel"
class NotificationReciever : FirebaseMessagingService() {

    var TAG : String = "FirebaseMessagingService"
    var mNotificationModel: CustomNotificationModel? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onMessageReceived(message: RemoteMessage) {
        super.onMessageReceived(message)
        Log.e("PushNotificationData", message.data.toString())
            if (message.data.isNotEmpty()) {
                try {
                    Log.e(TAG,"Notifications:::::"+message.data["data"])
                    mNotificationModel = Gson().fromJson(message.data["data"], CustomNotificationModel::class.java)
                   Log.e(TAG,"COUNT::"+mNotificationModel?.badgeCount)
                    Log.e(TAG,"COUNTvalue::"+mNotificationModel?.badgeCount)
                    mNotificationModel?.badgeCount?.let {
                        AppPreference().writeInteger(applicationContext, BADGE_COUNT,
                            it
                        )
                    }
                    sendPushNotification(mNotificationModel!!)
                } catch (e: Exception) {
                    Log.e("FCM", "onMessageReceived: $e")
                }
            }
    }


    private fun sendPushNotification(mNotificationModel : CustomNotificationModel) {
        val intent = Intent(this, HomeActivity::class.java)
        intent.putExtra(NOTI_TYPE, mNotificationModel.type)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT )

        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(mNotificationModel.title)
            .setContentText(mNotificationModel.message)
            .setSmallIcon(R.drawable.ic_notification_icon)
            .setSound(defaultSoundUri)
            .setAutoCancel(true)
            .setContentIntent(pendingIntent)
            .setVibrate(longArrayOf(1000, 1000, 1000, 1000, 1000))
            .build()
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationID = Random.nextInt()


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelName = "channelName"
            val channel = NotificationChannel(
                CHANNEL_ID,
                channelName,
                NotificationManager.IMPORTANCE_HIGH
            ).apply {
                enableLights(true)
                lightColor = Color.GREEN
            }
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(notificationID, notification)
    }


}