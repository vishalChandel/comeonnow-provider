package com.comeonnow.provider.app.ui.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.comeonnow.provider.app.adapter.NotificationAdapter
import com.comeonnow.provider.app.interfaces.ClinicRequestAcceptRejectInterface
import com.comeonnow.provider.app.model.DataItem
import com.comeonnow.provider.app.model.NotificationModel
import com.comeonnow.provider.app.model.StatusMessageModel
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonow.provider.app.R
import kotlinx.android.synthetic.main.fragment_appointment.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class NotificationFragment : BaseFragment() {

    // - - Initialize Widgets
    @BindView(R.id.notificationListRV)
    lateinit var notificationListRV: RecyclerView

    // - - Initialize Objects
    lateinit var mUnbinder: Unbinder
    var mNotificationAdapter: NotificationAdapter? = null
    var mNotificationList: ArrayList<DataItem?>? = ArrayList<DataItem?>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_notification, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        getNotificationData()
        return view
    }

    private fun getNotificationData() {
        if (activity?.let { isNetworkAvailable(it) }!!){
            executeNotificationListingApi()
        }else{
            showToast(activity,getString(R.string.internet_connection_error))
        }
    }

    private fun setAdapter(activity: FragmentActivity?, mNotificationList: ArrayList<DataItem?>?) {
        var langStatus=""
        langStatus = if(isEnglish()){
            "en"
        }else{
            "es"
        }
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        notificationListRV.layoutManager = layoutManager
        mNotificationAdapter = NotificationAdapter(activity,mNotificationList,langStatus,clinicRequestAcceptRejectInterface)
        notificationListRV.adapter = mNotificationAdapter
    }

    // - - Execute Notification Listing Api
    private fun executeNotificationListingApi() {
        showProgressDialog(activity)
        val call = ApiClient.apiInterface.notificationListingRequest(getAccessToken(),"500","1")
        Log.e("ProfileParams:","AccessToken:${getAccessToken()}")
        call.enqueue(object : Callback<NotificationModel> {
            override fun onResponse(
                call: Call<NotificationModel>,
                response: Response<NotificationModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {

                        mNotificationList = response.body()?.data as ArrayList<DataItem?>?
                        Log.i(TAG, "onResponse: "+mNotificationList)
                        setAdapter(activity,mNotificationList)
                        if (mNotificationList?.size!! > 0){
                            txtNoDataFoundTV.visibility = View.GONE
                        }else{
                            txtNoDataFoundTV.visibility = View.VISIBLE
                        }
                    }
                    401 -> {
                        showAuthFailedDialog(activity)
                    }
                    404 -> {
                        txtNoDataFoundTV.visibility=View.VISIBLE
                        txtNoDataFoundTV.text=getString(R.string.no_notifications)
                    }
                    500 -> {
                        showToast(activity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }


    // - - Implements Interface on cancel Appointment Click
    var clinicRequestAcceptRejectInterface: ClinicRequestAcceptRejectInterface = object :
        ClinicRequestAcceptRejectInterface {
        override fun clinicRequestAcceptReject(tag: String, id: String, notificationId: String) {
            super.clinicRequestAcceptReject(tag, id,notificationId)
            executeAcceptRejectClinicRequestApi(tag,id,notificationId)
        }
    }

    private fun executeAcceptRejectClinicRequestApi(tag: String, id: String, notificationId: String) {
        showProgressDialog(activity)
        val call = ApiClient.apiInterface.acceptRejectClinicRequest(getAccessToken(),id,tag,notificationId)
        call.enqueue(object : Callback<StatusMessageModel> {
            override fun onResponse(
                call: Call<StatusMessageModel>,
                response: Response<StatusMessageModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        showToast(requireActivity(),response.body()!!.message)
                        executeNotificationListingApi()
                    }
                    401 -> {
                        showAuthFailedDialog(activity)
                    }
                    404 -> {

                    }
                    500 -> {
                        showToast(activity, getString(R.string.internal_server_error))
                    }
                }
            }

            override fun onFailure(call: Call<StatusMessageModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }
}