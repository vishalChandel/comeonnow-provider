package com.comeonnow.provider.app.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.comeonnow.provider.app.interfaces.DeleteMyClinicInterface
import com.comeonnow.provider.app.model.MyClinicsDataItem
import com.comeonow.provider.app.R

class MyClinicsAdapter(
    var mActivity: Activity?,
    private var mMyClinicsArrayList: ArrayList<MyClinicsDataItem>,
    var mDeleteMyClinicInterface: DeleteMyClinicInterface
) :
    RecyclerView.Adapter<MyClinicsAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_my_clinics, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.clinicTV.text = mMyClinicsArrayList[position].branchName
        if(mMyClinicsArrayList[position].isApproved=="Approved"){
            holder.statusTV.text=""
        }else{
            holder.statusTV.text=mMyClinicsArrayList[position].isApproved
        }

        if (mMyClinicsArrayList[position].clinicName!!.isNotEmpty()) {
            holder.stateTV.visibility = View.VISIBLE
            holder.stateTV.text = mMyClinicsArrayList[position].clinicName
        } else {
            holder.stateTV.visibility = View.GONE
        }


        holder.deleteIV.setOnClickListener {
                if(mMyClinicsArrayList.size>1) {
                    mDeleteMyClinicInterface.onItemClickListener(mMyClinicsArrayList[position])
                }else{
                    Toast.makeText(mActivity, "You have deleted max number of clinics.", Toast.LENGTH_SHORT).show()
                    notifyDataSetChanged()
                }
        }
    }

    override fun getItemCount(): Int {
        return if (mMyClinicsArrayList.isEmpty()) {
            0
        } else {
            mMyClinicsArrayList.size
        }
    }

    class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        var editIV: ImageView = itemView.findViewById(R.id.editIV)
        var deleteIV: ImageView = itemView.findViewById(R.id.deleteIV)
        var clinicTV: TextView = itemView.findViewById(R.id.clinicTV)
        var stateTV: TextView = itemView.findViewById(R.id.stateTV)
        var statusTV: TextView = itemView.findViewById(R.id.statusTV)

    }
}