package com.comeonnow.provider.app.ui.activity


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import android.widget.Toast
import butterknife.ButterKnife
import butterknife.OnClick
import com.aigestudio.wheelpicker.WheelPicker
import com.bumptech.glide.Glide
import com.comeonnow.provider.app.model.*
import com.comeonnow.provider.app.retrofit.ApiClient
import com.comeonnow.provider.app.util.LIST
import com.comeonnow.provider.app.util.MODEL
import com.comeonow.provider.app.R
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_add_appointment.*
import kotlinx.android.synthetic.main.activity_edit_profile.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*


class AddAppointmentActivity : BaseActivity() {

    // - - Initialize Objects
    var mActualDate: String = ""
    var mDiseaseName: String = ""
    var mDiseaseId: String = ""
    var mAppointmentTypeId: String = ""
    var mAppointmentTypeName: String = ""
    var mPatientType: Int =  2
    var mDiseaseList: ArrayList<DiseaseDataItem?>? = ArrayList<DiseaseDataItem?>()
    var mAppointmentList: ArrayList<AppointmentTypeDataItem> = ArrayList<AppointmentTypeDataItem>()
    var mSelectedArrayList: ArrayList<UserDataItem> = ArrayList()
    var mSelectUser = "all"
    var mPatientId = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_appointment)
        ButterKnife.bind(this)
        setEditTextFocused(editDescriptionET)
        getDiseaseData()
    }

    private fun getDiseaseData() {
        if (isNetworkAvailable(mActivity)){
            executeDiseaseRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    @OnClick(
        R.id.backRL,
        R.id.aptDateTV,
        R.id.btnSaveTV,
        R.id.txtDiseaseTV,
        R.id.aptStartTimeTV,
        R.id.aptEndTimeTV,
        R.id.radioButtonAdult,
        R.id.radioButtonChild,
        R.id.radioButtonAllRB,
        R.id.radioButtonSelectedRB,
        R.id.txtAppointmentTypeTV)
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.aptDateTV -> showDefaultDatePicker(aptDateTV)
            R.id.btnSaveTV -> performSaveClick()
            R.id.txtDiseaseTV ->  mDiseaseListingDialog(mActivity)
            R.id.aptStartTimeTV -> showDefaultTimePicker(aptStartTimeTV)
            R.id.aptEndTimeTV -> showDefaultTimePicker(aptEndTimeTV)
            R.id.radioButtonAdult -> selectPatientTypeAdult()
            R.id.radioButtonChild -> selectPatientTypeChild()
            R.id.radioButtonAllRB -> selectAllChild()
            R.id.radioButtonSelectedRB -> selectSelectedChild()
            R.id.txtAppointmentTypeTV -> performAppointmentTypeClick()
        }
    }

    private fun selectAllChild() {
        mSelectUser = "all"
    }

    private fun selectSelectedChild() {
        mSelectUser = "select"
        val intent = Intent(mActivity, SelectUsersActivity::class.java)
        intent.putExtra(LIST,mSelectedArrayList)
        startActivityForResult(intent, 555)
    }

    private fun performAppointmentTypeClick() {
        mAppointmentTypeListingDialog(mActivity)
    }

    private fun selectPatientTypeAdult() {
        mPatientType = 1
    }

    private fun selectPatientTypeChild() {
        mPatientType = 2
    }

    private fun performSaveClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                if (mSelectUser == "select"){
                    executeSelectAddAppointmentApi()
                }else{
                    executeAllAddAppointmentApi()
                }
            } else
                showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    // - - Execute Add Appointment Api
    private fun executeAllAddAppointmentApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.addAppointmentRequest(getAccessToken(),
            editAptTitleET.text.toString(),
            mPatientType,
            mDiseaseId,
            aptDateTV.text.toString(),
            aptStartTimeTV.text.toString(),
            aptEndTimeTV.text.toString(),
            mAppointmentTypeId,
            editDescriptionET.text.toString())
        call.enqueue(object : Callback<AddAppointmentModel> {
            override fun onFailure(call: Call<AddAppointmentModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<AddAppointmentModel>, response: Response<AddAppointmentModel>) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity, getString(R.string.appointment_added_successfully))
                        finish()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    409 -> {
                        dismissProgressDialog()
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        val mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    405 -> {
                        dismissProgressDialog()
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        val mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - Execute Add Appointment Api
    private fun executeSelectAddAppointmentApi() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.addAppointmentSelectUserRequest(getAccessToken(),
            editAptTitleET.text.toString(),
            mPatientType,
            mDiseaseId,
            aptDateTV.text.toString(),
            aptStartTimeTV.text.toString(),
            aptEndTimeTV.text.toString(),
            mAppointmentTypeId,
            editDescriptionET.text.toString(),
            mPatientId
        )
        call.enqueue(object : Callback<AddAppointmentModel> {
            override fun onFailure(call: Call<AddAppointmentModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<AddAppointmentModel>, response: Response<AddAppointmentModel>) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity, getString(R.string.appointment_added_successfully))
                        finish()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    405 -> {
                        dismissProgressDialog()
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        val mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    409 -> {
                        dismissProgressDialog()
                        val gson = Gson()
                        val type = object : TypeToken<ChangePwdModel>() {}.type
                        var mModel: ChangePwdModel? = gson.fromJson(response.errorBody()!!.charStream(), type)
                        showAlertDialog(mActivity, mModel?.message)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - Validations for Add Patient fields
    private fun isValidate(): Boolean {
        var flag = true
        when {

            mSelectUser == "select" &&  mSelectedArrayList.isEmpty() -> {
                showAlertDialog(mActivity, getString(R.string.please_select_users))
                flag = false
            }
            editAptTitleET.text.toString().trim { it <= ' ' } == "" -> {

            }
            !radioButtonAdult.isChecked && !radioButtonChild.isChecked -> {
                showAlertDialog(mActivity, getString(R.string.please_select_patient_type))
                flag = false
            }
            txtDiseaseTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_disease))
                flag = false
            }
            aptDateTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_date))
                flag = false
            }
            aptStartTimeTV.text.toString().trim() { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_apt_time))
                flag = false
            }
            aptEndTimeTV.text.toString().trim() { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_apt_end_time))
                flag = false
            }
            txtAppointmentTypeTV.text.toString().trim() { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_select_the_appointment_type))
                flag = false
            }
            editDescriptionET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_description))
                flag = false
            }
            editDescriptionET.text.toString().trim { it <= ' ' } == "" && editDescriptionET.text.toString().trim().length!! < 10 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_description_in_more_details))
                flag = false
            }
        }
        return flag
    }


    // - - Execute Disease Api
    private fun executeDiseaseRequest() {
        showProgressDialog(mActivity)
        val call = ApiClient.apiInterface.diseaseListRequest(getAccessToken())
        Log.e("DiseaseParams:", "AccessToken:${getAccessToken()}")
        call.enqueue(object : Callback<DiseaseModel> {
            override fun onResponse(
                call: Call<DiseaseModel>,
                response: Response<DiseaseModel>) {
                when (response.code()) {
                    200 -> {
                        mDiseaseList = response.body()?.data as ArrayList<DiseaseDataItem?>?
                        executeAppointmentsTypeRequest()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
            override fun onFailure(call: Call<DiseaseModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }



    fun mDiseaseListingDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList<String>()
        for (i in 0 until mDiseaseList?.size!!) {
            mDataList.add(mDiseaseList?.get(i)?.diseaseName!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_disease, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val mWheelPickerWP: WheelPicker = sheetView.findViewById(R.id.mWheelPickerWP)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        mWheelPickerWP.setAtmospheric(true)
        mWheelPickerWP.isCyclic = false
        mWheelPickerWP.isCurved = true
        //Set Data
        mWheelPickerWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mDiseaseId = "" + mDiseaseList!![mWheelPickerWP.currentItemPosition]?.id
            mDiseaseName = mDataList.get(mWheelPickerWP.currentItemPosition)
            txtDiseaseTV.text = mDiseaseName
            mBottomSheetDialog.dismiss()
        }
    }




    // - - Execute Disease Api
    private fun executeAppointmentsTypeRequest() {
        val call = ApiClient.apiInterface.appointmentsTypeListRequest(getAccessToken())
        call.enqueue(object : Callback<AppointmentTypeModel> {
            override fun onResponse(
                call: Call<AppointmentTypeModel>,
                response: Response<AppointmentTypeModel>) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        mAppointmentList = response.body()?.data as ArrayList<AppointmentTypeDataItem>
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
            override fun onFailure(call: Call<AppointmentTypeModel>, t: Throwable) {
                dismissProgressDialog()
            }
        })
    }


    fun mAppointmentTypeListingDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList<String>()
        for (i in 0 until mAppointmentList?.size!!) {
            mDataList.add(mAppointmentList?.get(i)?.name!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_appointment_types, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val relationshipWP: WheelPicker = sheetView.findViewById(R.id.relationshipWP)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        relationshipWP.setAtmospheric(true)
        relationshipWP.isCyclic = false
        relationshipWP.isCurved = true
        //Set Data
        relationshipWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mAppointmentTypeId = "" + mAppointmentList!![relationshipWP.currentItemPosition]?.id
            mAppointmentTypeName = mDataList.get(relationshipWP.currentItemPosition)
            txtAppointmentTypeTV.text = mAppointmentTypeName
            mBottomSheetDialog.dismiss()
        }
    }





    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 555) {
            if (data != null) {
                mSelectedArrayList = data.getParcelableArrayListExtra<UserDataItem>(LIST)!!
                Log.e(TAG,"**SelectedUserSize**"+mSelectedArrayList.size)
                for (i in 0 until mSelectedArrayList.size){
                    if (i == 0){
                        mPatientId = ""+mSelectedArrayList.get(i).patientId
                    }else{
                        mPatientId =mPatientId +","+""+mSelectedArrayList.get(i).patientId
                    }
                }
            }
        }
    }

}




